LastSearches = new Mongo.Collection("lastSearches");

LastSearches.attachSchema(new SimpleSchema({
    user_id: {
        type: String
    },

    city: {
        type: String
    },

    degree_type: {
        type: Number,
        optional: true
    },

    languages: {
        type: [Object],
        min: 1
    },

    'languages.$.language': {
        type: Number
    },

    'languages.$.speaking': {
        type: Number,
        optional: true
    },

    'languages.$.reading': {
        type: Number,
        optional: true
    },

    'languages.$.writing': {
        type: Number,
        optional: true
    },

    skills: {
        type: [Object],
        optional: true
    },

    'skills.$.name': {
        type: String
    },

    'skills.$.experience': {
        type: Number
    },

    salary_min: {
        type: Number,
        optional: true
    },

    salary_max: {
        type: Number,
        optional: true
    },

        work_areas: {
            type: [Object],
            min: 1
        },
        "work_areas.$.area": {
            label: "Darbo Sritis",
            type: Number,
            allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
            defaultValue: 1,
            autoform: {
                class: "browser-default work_area",
                afFormGroup : {
                    'formgroup-class': 'col s12 m12'
                },
                afFieldInput: {
                    class: "work-area"
                },
                options: [
                    {value: 1, label: "Administravimas/sekretoriavimas"},
                    {value: 2, label:  "Apskaita/finansai/auditas"},
                    {value: 3, label: "Dizainas/architektūra"},
                    {value: 4, label: "Draudimas"},
                    {value: 5, label: "Eksportas"},
                    {value: 6, label: "Elektronika/telekomunikacijos"},
                    {value: 7, label: "Energetika"},
                    {value: 8, label: "Inžinerija/mechanika"},
                    {value: 9, label: "Klientų aptarnavimas/paslaugos"},
                    {value: 10, label: "Kompiuteriai/IT/internetas"},
                    {value: 11, label: "Kultūra/kūryba"},
                    {value: 12, label: "Logistika/transportas"},
                    {value: 13, label: "Maisto gamyba"},
                    {value: 14, label: "Marketingas/reklama"},
                    {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
                    {value: 16, label: "Nekilnojamasis turtas"},
                    {value: 17, label: "Pardavimų vadyba"},
                    {value: 18, label: "Personalo valdymas"},
                    {value: 19, label: "Pirkimai/tiekimas"},
                    {value: 20, label: "Pramonė/gamyba"},
                    {value: 21, label: "Prekyba - konsultavimas"},
                    {value: 22, label: "Sandėliavimas"},
                    {value: 23, label: "Statyba"},
                    {value: 24, label: "Švietimas/mokymai"},
                    {value: 25, label: "Teisė"},
                    {value: 26, label: "Turizmas/viešbučiai"},
                    {value: 27, label: "Vadovavimas/valdymas"},
                    {value: 28, label: "Valstybės tarnyba"},
                    {value: 29, label: "Žemės ūkis/žuvininkystė"},
                    {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
                ]
            }
        },

    past_work: {
        type: Boolean
    }

})
);

LastSearches.attachBehaviour('timestampable');

if(Meteor.isServer) {
    Meteor.publish('lastSearches', function() {
        return LastSearches.find();
    });
}