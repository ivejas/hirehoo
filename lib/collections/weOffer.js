WeOffer = new Mongo.Collection("weOffer");

if(Meteor.isServer) {
    Meteor.publish('weOffer', function() {
        return WeOffer.find();
    });
}