    AccountsTemplates.configure({
        // Behavior
        confirmPassword: false,
        enablePasswordChange: true,
        enforceEmailVerification: true,
        forbidClientAccountCreation: false,
        overrideLoginErrors: true,
        sendVerificationEmail: true,
        lowercaseUsername: false,

        // Appearance
        showAddRemoveServices: false,
        showForgotPasswordLink: true,
        showLabels: true,
        showPlaceholders: true,
        showPlaceholders: true,
        showResendVerificationEmailLink: false,

        // Client-side Validation
        continuousValidation: false,
        negativeFeedback: false,
        negativeValidation: true,
        positiveValidation: true,
        positiveFeedback: true,
        showValidating: true,

        // Privacy Policy and Terms of Use
        privacyUrl: 'privacy',
        termsUrl: 'terms-of-use',

        // Redirects
        //homeRoutePath: '/',

        //// Hooks
        onLogoutHook: function () {
            FlowRouter.go('/');
        },
        onSubmitHook: function () {

            //FlowRouter.go('/cv/add');
        },
        //preSignUpHook: myPreSubmitFunc,

        // Texts
        texts: {
            socialIcons: {
                "meteor-developer": "fa fa-rocket"
            },
            title: {
                forgotPwd: "Recover Your Password",
                signIn: "Prisijugti",
                signUp: "Registracija"
            },
            button: {
                changePwd: "Pakeisti slaptažodį",
                signUp: "Registruotis",
                signIn: "Prisijungti"
            },
            info: {
                signUpVerifyEmail: "Registracija sėkminga. Patikrinkite savo el.paštą"
            }
        }
    });
    AccountsTemplates.removeField('password');
    AccountsTemplates.addField({
        _id: 'password',
        type: 'password',
        displayName: "Slaptažodis",
        required: true,
        minLength: 6,
        errStr: 'Slaptažodis turi turėti 6 ženklus'
    });

    AccountsTemplates.addField({
        _id: "type",
        type: "select",
        displayName: "Vartotojo tipas",
        select: [
            {
                text: "Ieškantis darbo",
                value: "1"
            },
            {
                text: "Įmonė",
                value: "2"
            },
            //{
            //    text: "Admin",
            //    value: "0"
            //}
        ]
    });


//AccountsTemplates.configureRoute('signIn');
//AccountsTemplates.configureRoute('signUp');
    AccountsTemplates.configureRoute('forgotPwd');
    AccountsTemplates.configureRoute('changePwd');
    AccountsTemplates.configureRoute('resetPwd');
    AccountsTemplates.configureRoute('verifyEmail');

    Accounts.onLogin(function (user) {
        if (user.user.profile.type == 1) {
            var profile = Profiles.findOne({"_id": user.user._id()});
            if (profile == null) {
                FlowRouter.go('/cv/add')
            } else {
                FlowRouter.go('/');
            }
        } else if (user.user.profile.type == 2) {
            var profile = CompanyProfiles.findOne({"_id": user.user._id});
            if (profile == null) {
                FlowRouter.go('/company/add')
            } else {
                FlowRouter.go('/');
            }
        }
    });

if(Meteor.isServer) {
    Meteor.publish("users", function () {
        return Meteor.users.find();
    });


}



