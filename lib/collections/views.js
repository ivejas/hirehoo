Views = new Mongo.Collection("views");

Views.attachSchema(new SimpleSchema({
    company_id: {
        type: String
    },
    user_id: {
        type: String
    }
}));

Views.attachBehaviour('timestampable');

if(Meteor.isServer) {
    Meteor.publish('views', function() {
        return Views.find();
    });
    Meteor.publish('myPersonalViews', function() {
        return Views.find({user_id: this.userId}, {"_id": 1, 'createdAt': 1});
    });
}