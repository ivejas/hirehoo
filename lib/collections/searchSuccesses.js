SearchSuccesses = new Mongo.Collection("searchSuccesses");

SearchSuccesses.attachSchema(new SimpleSchema({
    user_id: {
        type: String
    },
    count: {
        type: Number
    }
})
);
SearchSuccesses.attachBehaviour('timestampable');

if(Meteor.isServer) {
    Meteor.publish('searchSuccesses', function() {
        return SearchSuccesses.find();
    });
    Meteor.publish('mySearchSuccesses', function() {
        return SearchSuccesses.find({user_id: this.userId});
    });
}