Tickets = new Mongo.Collection("tickets");

Tickets.attachSchema(new SimpleSchema({
    owner: {
        type: String
    },
    token: {
       type: String
    },
    datetime_created: {
        type: Date
    },
    datetime_used: {
        type: Date,
        optional: true
    },
    used_on: {
        type: String,
        optional: true
    },
    used: {
        type: Number
    }
}));

if(Meteor.isServer) {
    Meteor.publish('myTickets', function() {
        return Tickets.find({owner: this.userId}, {fields: {'token': 0, 'owner': 0}});
    });
}