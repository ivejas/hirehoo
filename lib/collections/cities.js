Cities = new Mongo.Collection("cities");

Cities.attachSchema(new SimpleSchema({
    city: {
        type: String
    },
    country: {
        type: Number
    }

})
);

if(Meteor.isServer) {
    Meteor.publish('cities', function() {
        return Cities.find();
    });
}