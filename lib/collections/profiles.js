SimpleSchema.messages({
    "datesMismatch": "Baigimo data negali būti ankčiau nei pradžios data"
});

Profiles = new Mongo.Collection("profiles");

Profiles.attachSchema(new SimpleSchema({
    user_id: {
        type: String
    },
    approved: {
        type: Number,
        defaultValue: 0
    },
    email: {
        type: String
    },

    picture: {
        optional: true,
        type: String,
        label: 'Nuotrauka',
        autoform: {
            class: "profile-picture",
            afFieldInput: {
                type: 'fileUpload',
                collection: 'Images',
                accept: 'image/*'
            }
        }
    },
    first_name: {
        label: 'Vardas',
        type: String,
        regEx: /^([^0-9]*)$/,
        autoform: {
            afFormGroup : {
                'formgroup-class': 'col s6 m6'
            }
        }
    },
    last_name: {
        label: "Pavardė",
        type: String,
        regEx: /^([^0-9]*)$/,
        autoform: {
            afFormGroup : {
                'formgroup-class': 'col s6 m6'
            }
        }
    },
    age: {
        label: "Amžius",
        type: Number,
        min: 18,
        autoform: {
            afFormGroup : {
                'formgroup-class': 'col s6 m6'
            }
        }
    },
    phone: {
        label: "Telefono numeris",
        type: String,
        autoform: {
            afFormGroup : {
                'formgroup-class': 'col s6 m6'
            }
        }
    },
    //work_area: {
    //    label: "Darbo Sritis",
    //    type: Number,
    //    allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
    //    defaultValue: 1,
    //    autoform: {
    //        class: "browser-default work_area",
    //        afFormGroup : {
    //            'formgroup-class': 'col s12 m12'
    //        },
    //        afFieldInput: {
    //            class: "work-area"
    //        },
    //        options: [
    //            {value: 1, label: "Administravimas/sekretoriavimas"},
    //            {value: 2, label:  "Apskaita/finansai/auditas"},
    //            {value: 3, label: "Dizainas/architektūra"},
    //            {value: 4, label: "Draudimas"},
    //            {value: 5, label: "Eksportas"},
    //            {value: 6, label: "Elektronika/telekomunikacijos"},
    //            {value: 7, label: "Energetika"},
    //            {value: 8, label: "Inžinerija/mechanika"},
    //            {value: 9, label: "Klientų aptarnavimas/paslaugos"},
    //            {value: 10, label: "Kompiuteriai/IT/internetas"},
    //            {value: 11, label: "Kultūra/kūryba"},
    //            {value: 12, label: "Logistika/transportas"},
    //            {value: 13, label: "Maisto gamyba"},
    //            {value: 14, label: "Marketingas/reklama"},
    //            {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
    //            {value: 16, label: "Nekilnojamasis turtas"},
    //            {value: 17, label: "Pardavimų vadyba"},
    //            {value: 18, label: "Personalo valdymas"},
    //            {value: 19, label: "Pirkimai/tiekimas"},
    //            {value: 20, label: "Pramonė/gamyba"},
    //            {value: 21, label: "Prekyba - konsultavimas"},
    //            {value: 22, label: "Sandėliavimas"},
    //            {value: 23, label: "Statyba"},
    //            {value: 24, label: "Švietimas/mokymai"},
    //            {value: 25, label: "Teisė"},
    //            {value: 26, label: "Turizmas/viešbučiai"},
    //            {value: 27, label: "Vadovavimas/valdymas"},
    //            {value: 28, label: "Valstybės tarnyba"},
    //            {value: 29, label: "Žemės ūkis/žuvininkystė"},
    //            {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
    //        ]
    //    }
    //},
    short_description: {
        label: "Aprašymas",
        type: String,
        autoform: {
            afFormGroup : {
                'formgroup-class': 'col s12 m12'
            },
            afFieldInput: {
                type: "textarea",
                class: "materialize-textarea"
            }
        }
    },
    requirements: {
        type: [Object],
        //min: 1,
        label: " ",
        optional: true
    },
    //"requirements.$.country": {
    //    label: ' ',
    //    type: Number,
    //    allowedValues: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28],
    //    defaultValue: 13,
    //    autoform: {
    //        class: "browser-default country hidden",
    //        options: [
    //            {label: "Anglija", value: 0},
    //            {label: "Baltarusija", value: 1},
    //            {label: "Bulgarija", value: 2},
    //            {label: "Čekija", value: 3},
    //            {label: "Danija", value: 4},
    //            {label: "Estija", value: 5},
    //            {label: "Graikija", value: 6},
    //            {label: "Gruzija", value: 7},
    //            {label: "Ispanija", value: 8},
    //            {label: "Italija", value: 9},
    //            {label: "Kroatija", value: 10},
    //            {label: "Latvija", value: 11},
    //            {label: "Lenkija", value: 12},
    //            {label: "Lietuva", value: 13},
    //            {label: "Norvegija", value: 14},
    //            {label: "Olandija", value: 15},
    //            {label: "Portugalija", value: 16},
    //            {label: "Prancūzija", value: 17},
    //            {label: "Rumunija", value: 18},
    //            {label: "Rusija", value: 19},
    //            {label: "Serbija", value: 20},
    //            {label: "Slovakija", value: 21},
    //            {label: "Slovėnija", value: 22},
    //            {label: "Suomija", value: 23},
    //            {label: "Švedija", value: 24},
    //            {label: "Turkija", value: 25},
    //            {label: "Ukraina", value: 26},
    //            {label: "Vengrija", value: 27},
    //            {label: "Vokietija", value: 28}
    //        ]
    //    }
    //},
    work_areas: {
        type: [Object],
        min: 1
    },
    "work_areas.$.area": {
        label: "Darbo Sritis",
        type: Number,
        allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        defaultValue: 1,
        autoform: {
            class: "browser-default work_area",
            afFormGroup : {
                'formgroup-class': 'col s12 m12'
            },
            afFieldInput: {
                class: "work-area"
            },
            options: [
                {value: 1, label: "Administravimas/sekretoriavimas"},
                {value: 2, label:  "Apskaita/finansai/auditas"},
                {value: 3, label: "Dizainas/architektūra"},
                {value: 4, label: "Draudimas"},
                {value: 5, label: "Eksportas"},
                {value: 6, label: "Elektronika/telekomunikacijos"},
                {value: 7, label: "Energetika"},
                {value: 8, label: "Inžinerija/mechanika"},
                {value: 9, label: "Klientų aptarnavimas/paslaugos"},
                {value: 10, label: "Kompiuteriai/IT/internetas"},
                {value: 11, label: "Kultūra/kūryba"},
                {value: 12, label: "Logistika/transportas"},
                {value: 13, label: "Maisto gamyba"},
                {value: 14, label: "Marketingas/reklama"},
                {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
                {value: 16, label: "Nekilnojamasis turtas"},
                {value: 17, label: "Pardavimų vadyba"},
                {value: 18, label: "Personalo valdymas"},
                {value: 19, label: "Pirkimai/tiekimas"},
                {value: 20, label: "Pramonė/gamyba"},
                {value: 21, label: "Prekyba - konsultavimas"},
                {value: 22, label: "Sandėliavimas"},
                {value: 23, label: "Statyba"},
                {value: 24, label: "Švietimas/mokymai"},
                {value: 25, label: "Teisė"},
                {value: 26, label: "Turizmas/viešbučiai"},
                {value: 27, label: "Vadovavimas/valdymas"},
                {value: 28, label: "Valstybės tarnyba"},
                {value: 29, label: "Žemės ūkis/žuvininkystė"},
                {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
            ]
        }
    },
    "requirements.$.city": {
        label: "Miestas",
        regEx: /^([^0-9]*)$/,
        type: String
    },
    "requirements.$.salary_from": {
        label: "Alga nuo",
        type: Number,
        min: 1
    },
    past_work: {
        label: " ",
        type: [Object],
        optional: true
    },
    "past_work.$.work_area": {
        label: "Darbo sritis",
        type: Number,
        allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        defaultValue: 1,
        autoform: {
            class: "browser-default",
            afFieldInput: {
                //class: "col m6"
            },
            options: [
                {value: 1, label: "Administravimas/sekretoriavimas"},
                {value: 2, label:  "Apskaita/finansai/auditas"},
                {value: 3, label: "Dizainas/architektūra"},
                {value: 4, label: "Draudimas"},
                {value: 5, label: "Eksportas"},
                {value: 6, label: "Elektronika/telekomunikacijos"},
                {value: 7, label: "Energetika"},
                {value: 8, label: "Inžinerija/mechanika"},
                {value: 9, label: "Klientų aptarnavimas/paslaugos"},
                {value: 10, label: "Kompiuteriai/IT/internetas"},
                {value: 11, label: "Kultūra/kūryba"},
                {value: 12, label: "Logistika/transportas"},
                {value: 13, label: "Maisto gamyba"},
                {value: 14, label: "Marketingas/reklama"},
                {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
                {value: 16, label: "Nekilnojamasis turtas"},
                {value: 17, label: "Pardavimų vadyba"},
                {value: 18, label: "Personalo valdymas"},
                {value: 19, label: "Pirkimai/tiekimas"},
                {value: 20, label: "Pramonė/gamyba"},
                {value: 21, label: "Prekyba - konsultavimas"},
                {value: 22, label: "Sandėliavimas"},
                {value: 23, label: "Statyba"},
                {value: 24, label: "Švietimas/mokymai"},
                {value: 25, label: "Teisė"},
                {value: 26, label: "Turizmas/viešbučiai"},
                {value: 27, label: "Vadovavimas/valdymas"},
                {value: 28, label: "Valstybės tarnyba"},
                {value: 29, label: "Žemės ūkis/žuvininkystė"},
                {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
            ]
        }
    },
    "past_work.$.workplace_name": {
        label: "Įmonės pavadinimas",
        type: String
    },
    "past_work.$.workplace_website": {
        label: "Įmonės svetainė",
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: 'url'
            }
        }
    },
    "past_work.$.work_title": {
        label: "Pareigos",
        type: String
    },
    "past_work.$.start_time_year": {
        label: "Darbo pradžia",
        type: Number,
        allowedValues: [2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,
            1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960],
        defaultValue: 2015,
        autoform: {
            options: [
                {value: 2015, label: 2015},
                {value: 2014, label: 2014},
                {value: 2013, label: 2013},
                {value: 2012, label: 2012},
                {value: 2011, label: 2011},
                {value: 2010, label: 2010},
                {value: 2009, label: 2009},
                {value: 2008, label: 2008},
                {value: 2007, label: 2007},
                {value: 2006, label: 2006},
                {value: 2005, label: 2005},
                {value: 2004, label: 2004},
                {value: 2003, label: 2003},
                {value: 2002, label: 2002},
                {value: 2001, label: 2001},
                {value: 2000, label: 2000},
                {value: 1999, label: 1999},
                {value: 1998, label: 1998},
                {value: 1997, label: 1997},
                {value: 1996, label: 1996},
                {value: 1995, label: 1995},
                {value: 1994, label: 1994},
                {value: 1993, label: 1993},
                {value: 1992, label: 1992},
                {value: 1991, label: 1991},
                {value: 1990, label: 1990},
                {value: 1989, label: 1989},
                {value: 1988, label: 1988},
                {value: 1987, label: 1987},
                {value: 1986, label: 1986},
                {value: 1985, label: 1985},
                {value: 1984, label: 1984},
                {value: 1983, label: 1983},
                {value: 1982, label: 1982},
                {value: 1981, label: 1981},
                {value: 1980, label: 1980},
                {value: 1979, label: 1979},
                {value: 1978, label: 1978},
                {value: 1977, label: 1977},
                {value: 1976, label: 1976},
                {value: 1975, label: 1975},
                {value: 1974, label: 1974},
                {value: 1973, label: 1973},
                {value: 1972, label: 1972},
                {value: 1971, label: 1971},
                {value: 1970, label: 1970},
                {value: 1969, label: 1969},
                {value: 1968, label: 1968},
                {value: 1967, label: 1967},
                {value: 1966, label: 1966},
                {value: 1965, label: 1965},
                {value: 1964, label: 1964},
                {value: 1963, label: 1963},
                {value: 1962, label: 1962},
                {value: 1961, label: 1961},
                {value: 1960, label: 1960}
            ]
        }
    },
    "past_work.$.start_time_month": {
        label: '',
        defaultValue: 1,
        autoform: {
            options: [
                {value: 12, label: 12},
                {value: 11, label: 11},
                {value: 10, label: 10},
                {value: 9, label: 9},
                {value: 8, label: 8},
                {value: 7, label: 7},
                {value: 6, label: 6},
                {value: 5, label: 5},
                {value: 4, label: 4},
                {value: 3, label: 3},
                {value: 2, label: 2},
                {value: 1, label: 1}
            ]
        },
        type: Number,
        custom: function() {
            if(this.value != 0) {
                var split = this.key.split('.');
                if((this.field(split[0] + '.' + split[1] + '.end_time_year').value) != 0) {
                    var start = this.field(split[0] + '.' + split[1] + '.start_time_year').value * 12 + this.field(split[0] + '.' + split[1] + '.start_time_month').value;
                    var end = this.field(split[0] + '.' + split[1] + '.end_time_year').value * 12 + this.field(split[0] + '.' + split[1] + '.end_time_month').value;
                    if ((end - start) <= 0) {
                        return "datesMismatch";
                    }
                }
            }
        },
        allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12]
    },
    "past_work.$.end_time_year": {
        label: "Darbo pabaiga",
        defaultValue: 2015,
        autoform: {
            options: [
                {value: 0, label: "Vis dar dirbu"},
                {value: 2015, label: 2015},
                {value: 2014, label: 2014},
                {value: 2013, label: 2013},
                {value: 2012, label: 2012},
                {value: 2011, label: 2011},
                {value: 2010, label: 2010},
                {value: 2009, label: 2009},
                {value: 2008, label: 2008},
                {value: 2007, label: 2007},
                {value: 2006, label: 2006},
                {value: 2005, label: 2005},
                {value: 2004, label: 2004},
                {value: 2003, label: 2003},
                {value: 2002, label: 2002},
                {value: 2001, label: 2001},
                {value: 2000, label: 2000},
                {value: 1999, label: 1999},
                {value: 1998, label: 1998},
                {value: 1997, label: 1997},
                {value: 1996, label: 1996},
                {value: 1995, label: 1995},
                {value: 1994, label: 1994},
                {value: 1993, label: 1993},
                {value: 1992, label: 1992},
                {value: 1991, label: 1991},
                {value: 1990, label: 1990},
                {value: 1989, label: 1989},
                {value: 1988, label: 1988},
                {value: 1987, label: 1987},
                {value: 1986, label: 1986},
                {value: 1985, label: 1985},
                {value: 1984, label: 1984},
                {value: 1983, label: 1983},
                {value: 1982, label: 1982},
                {value: 1981, label: 1981},
                {value: 1980, label: 1980},
                {value: 1979, label: 1979},
                {value: 1978, label: 1978},
                {value: 1977, label: 1977},
                {value: 1976, label: 1976},
                {value: 1975, label: 1975},
                {value: 1974, label: 1974},
                {value: 1973, label: 1973},
                {value: 1972, label: 1972},
                {value: 1971, label: 1971},
                {value: 1970, label: 1970},
                {value: 1969, label: 1969},
                {value: 1968, label: 1968},
                {value: 1967, label: 1967},
                {value: 1966, label: 1966},
                {value: 1965, label: 1965},
                {value: 1964, label: 1964},
                {value: 1963, label: 1963},
                {value: 1962, label: 1962},
                {value: 1961, label: 1961},
                {value: 1960, label: 1960}
            ]
        },
        type: Number,
        custom: function() {
            if(this.value != 0) {
                var split = this.key.split('.');
                if((this.field(split[0] + '.' + split[1] + '.end_time_year').value) != 0) {
                    var start = this.field(split[0] + '.' + split[1] + '.start_time_year').value * 12 + this.field(split[0] + '.' + split[1] + '.start_time_month').value;
                    var end = this.field(split[0] + '.' + split[1] + '.end_time_year').value * 12 + this.field(split[0] + '.' + split[1] + '.end_time_month').value;
                    if ((end - start) <= 0) {
                        return "datesMismatch";
                    }
                }
            }
        },
        allowedValues: [0,2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,
            1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960]
    },
    "past_work.$.end_time_month": {
        optional: true,
        label: '',
        defaultValue: 1,
        autoform: {
            options: [
                {value: 12, label: 12},
                {value: 11, label: 11},
                {value: 10, label: 10},
                {value: 9, label: 9},
                {value: 8, label: 8},
                {value: 7, label: 7},
                {value: 6, label: 6},
                {value: 5, label: 5},
                {value: 4, label: 4},
                {value: 3, label: 3},
                {value: 2, label: 2},
                {value: 1, label: 1}
            ]
        },
        type: Number,
        allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12],
        custom: function() {
            if(this.value != 0) {
                var split = this.key.split('.');
                if((this.field(split[0] + '.' + split[1] + '.end_time_year').value) != 0) {
                    var start = this.field(split[0] + '.' + split[1] + '.start_time_year').value * 12 + this.field(split[0] + '.' + split[1] + '.start_time_month').value;
                    var end = this.field(split[0] + '.' + split[1] + '.end_time_year').value * 12 + this.field(split[0] + '.' + split[1] + '.end_time_month').value;
                    if ((end - start) <= 0) {
                        return "datesMismatch";
                    }
                }
            }
        }
    },
    "past_work.$.work_description": {
        label: "Darbo aprašymas",
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: "textarea",
                class: "materialize-textarea"
            }
        }
    },
    education: {
        label: " ",
        type: [Object],
        optional: true
    },
    "education.$.degree_type": {
        label: '',
        type: Number,
        allowedValues: [0,1,2,3],
        defaultValue: 0,
        autoform: {
            options: [
                {label: "Vidurinysis", value: 0},
                {label: "Bakalauras", value: 1},
                {label: "Magistras", value: 2},
                {label: "Daktaras", value: 3}
            ]
        }
    },
    "education.$.degree_name": {
        label: "Mokslų pavadinimas",
        type: String,
        max: 30
    },
    "education.$.institution_name": {
        label: "Mokymo įstaigos pavadinimas",
        max: 50,
        type: String
    },
    "education.$.start_time_year": {
        label: "Mokslų pradžia",
        defaultValue: 2015,
        autoform: {
            options: [
                {value: 2015, label: 2015},
                {value: 2014, label: 2014},
                {value: 2013, label: 2013},
                {value: 2012, label: 2012},
                {value: 2011, label: 2011},
                {value: 2010, label: 2010},
                {value: 2009, label: 2009},
                {value: 2008, label: 2008},
                {value: 2007, label: 2007},
                {value: 2006, label: 2006},
                {value: 2005, label: 2005},
                {value: 2004, label: 2004},
                {value: 2003, label: 2003},
                {value: 2002, label: 2002},
                {value: 2001, label: 2001},
                {value: 2000, label: 2000},
                {value: 1999, label: 1999},
                {value: 1998, label: 1998},
                {value: 1997, label: 1997},
                {value: 1996, label: 1996},
                {value: 1995, label: 1995},
                {value: 1994, label: 1994},
                {value: 1993, label: 1993},
                {value: 1992, label: 1992},
                {value: 1991, label: 1991},
                {value: 1990, label: 1990},
                {value: 1989, label: 1989},
                {value: 1988, label: 1988},
                {value: 1987, label: 1987},
                {value: 1986, label: 1986},
                {value: 1985, label: 1985},
                {value: 1984, label: 1984},
                {value: 1983, label: 1983},
                {value: 1982, label: 1982},
                {value: 1981, label: 1981},
                {value: 1980, label: 1980},
                {value: 1979, label: 1979},
                {value: 1978, label: 1978},
                {value: 1977, label: 1977},
                {value: 1976, label: 1976},
                {value: 1975, label: 1975},
                {value: 1974, label: 1974},
                {value: 1973, label: 1973},
                {value: 1972, label: 1972},
                {value: 1971, label: 1971},
                {value: 1970, label: 1970},
                {value: 1969, label: 1969},
                {value: 1968, label: 1968},
                {value: 1967, label: 1967},
                {value: 1966, label: 1966},
                {value: 1965, label: 1965},
                {value: 1964, label: 1964},
                {value: 1963, label: 1963},
                {value: 1962, label: 1962},
                {value: 1961, label: 1961},
                {value: 1960, label: 1960}
            ]
        },
        type: Number,
        custom: function() {
            if(this.value != 0) {
                var split = this.key.split('.');
                var start = this.field(split[0] + '.' + split[1] + '.start_time_year').value * 12;
                var end = this.field(split[0] + '.' + split[1] + '.end_time_year').value * 12;
                if ((end - start) <= 0) {
                    return "datesMismatch";
                }
            }
        },
        allowedValues: [2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,
            1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960]
    },
    "education.$.end_time_year": {
        label: "Mokslų pabaiga",
        defaultValue: 2015,
        autoform: {
            options: [
                {value: 2015, label: 2015},
                {value: 2014, label: 2014},
                {value: 2013, label: 2013},
                {value: 2012, label: 2012},
                {value: 2011, label: 2011},
                {value: 2010, label: 2010},
                {value: 2009, label: 2009},
                {value: 2008, label: 2008},
                {value: 2007, label: 2007},
                {value: 2006, label: 2006},
                {value: 2005, label: 2005},
                {value: 2004, label: 2004},
                {value: 2003, label: 2003},
                {value: 2002, label: 2002},
                {value: 2001, label: 2001},
                {value: 2000, label: 2000},
                {value: 1999, label: 1999},
                {value: 1998, label: 1998},
                {value: 1997, label: 1997},
                {value: 1996, label: 1996},
                {value: 1995, label: 1995},
                {value: 1994, label: 1994},
                {value: 1993, label: 1993},
                {value: 1992, label: 1992},
                {value: 1991, label: 1991},
                {value: 1990, label: 1990},
                {value: 1989, label: 1989},
                {value: 1988, label: 1988},
                {value: 1987, label: 1987},
                {value: 1986, label: 1986},
                {value: 1985, label: 1985},
                {value: 1984, label: 1984},
                {value: 1983, label: 1983},
                {value: 1982, label: 1982},
                {value: 1981, label: 1981},
                {value: 1980, label: 1980},
                {value: 1979, label: 1979},
                {value: 1978, label: 1978},
                {value: 1977, label: 1977},
                {value: 1976, label: 1976},
                {value: 1975, label: 1975},
                {value: 1974, label: 1974},
                {value: 1973, label: 1973},
                {value: 1972, label: 1972},
                {value: 1971, label: 1971},
                {value: 1970, label: 1970},
                {value: 1969, label: 1969},
                {value: 1968, label: 1968},
                {value: 1967, label: 1967},
                {value: 1966, label: 1966},
                {value: 1965, label: 1965},
                {value: 1964, label: 1964},
                {value: 1963, label: 1963},
                {value: 1962, label: 1962},
                {value: 1961, label: 1961},
                {value: 1960, label: 1960}
            ]
        },
        custom: function() {
            if(this.value != 0) {
                var split = this.key.split('.');
                var start = this.field(split[0] + '.' + split[1] + '.start_time_year').value * 12;
                var end = this.field(split[0] + '.' + split[1] + '.end_time_year').value * 12;
                if ((end - start) <= 0) {
                    return "datesMismatch";
                }
            }
        },
        type: Number,
        allowedValues: [2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,
            1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960]
    },
    languages: {
        label: " ",
        type: [Object],
        //min: 1
        optional: true
    },
    "languages.$.language": {
        label: '',
        type: Number,
        allowedValues: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40],
        defaultValue: 23,
        autoform: {
            class: "browser-default",
            options: [
            {label: "Anglų", value: 0},
            {label: "Arabų", value: 1},
            {label: "Armėnų", value: 2},
            {label: "Azerbaidžaniečių", value: 3},
            {label: "Baltarusių", value: 4},
            {label: "Bulgarų", value: 5},
            {label: "Čekų", value: 6},
            {label: "Danų", value: 7},
            {label: "Esperanto", value: 8},
            {label: "Estų", value: 9},
            {label: "Filipinų", value: 10},
            {label: "Graikų", value: 11},
            {label: "Gruzinų", value: 12},
            {label: "Hebrajų", value: 13},
            {label: "Hindi", value: 14},
            {label: "Ispanų", value: 15},
            {label: "Italų", value: 16},
            {label: "Japonų", value: 17},
            {label: "Kinų", value: 18},
            {label: "Korėjiečių", value: 19},
            {label: "Kroatų", value: 20},
            {label: "Latvių", value: 21},
            {label: "Lenkų", value: 22},
            {label: "Lietuvių", value: 23},
            {label: "Lotynų", value: 24},
            {label: "Norvegų", value: 25},
            {label: "Olandų", value: 26},
            {label: "Portugalų", value: 27},
            {label: "Prancūzų", value: 28},
            {label: "Rumunų", value: 29},
            {label: "Rusų", value: 30},
            {label: "Serbų", value: 31},
            {label: "Slovakų", value: 32},
            {label: "Slovėnų", value: 33},
            {label: "Suomių", value: 34},
            {label: "Švedų", value: 35},
            {label: "Turkų", value: 36},
            {label: "Ukrainiečių", value: 37},
            {label: "Vengrų", value: 38},
            {label: "Vokiečių", value: 39}
            ]
        }
    },
    "languages.$.speaking": {
        label: 'Kalbėjimas',
        type: Number,
        allowedValues: [0,1,2,3,4,5],
        defaultValue: 0,
        autoform: {
            class: "browser-default",
            options: [
                {label: "Blogai", value: 0},
                {label: "Blogiau nei vidutiniškai", value: 1},
                {label: "Vidutiniškai", value: 2},
                {label: "Geriau nei vidutiniškai", value: 3},
                {label: "Gerai", value: 4}
            ]
        }
    },
    "languages.$.listening": {
        label: 'Klausymas',
        type: Number,
        allowedValues: [0,1,2,3,4,5],
        defaultValue: 0,
        autoform: {
            class: "browser-default",
            options: [
                {label: "Blogai", value: 0},
                {label: "Blogiau nei vidutiniškai", value: 1},
                {label: "Vidutiniškai", value: 2},
                {label: "Geriau nei vidutiniškai", value: 3},
                {label: "Gerai", value: 4}
            ]
        }
    },
    "languages.$.reading": {
        label: 'Skaitymas',
        type: Number,
        allowedValues: [0,1,2,3,4,5],
        defaultValue: 0,
        autoform: {
            class: "browser-default",
            options: [
                {label: "Blogai", value: 0},
                {label: "Blogiau nei vidutiniškai", value: 1},
                {label: "Vidutiniškai", value: 2},
                {label: "Geriau nei vidutiniškai", value: 3},
                {label: "Gerai", value: 4}
            ]
        }
    },
    "languages.$.writing": {
        label: "Rašymas",
        type: Number,
        allowedValues: [0,1,2,3,4,5],
        defaultValue: 0,
        autoform: {
            class: "browser-default",
            options: [
                {label: "Blogai", value: 0},
                {label: "Blogiau nei vidutiniškai", value: 1},
                {label: "Vidutiniškai", value: 2},
                {label: "Geriau nei vidutiniškai", value: 3},
                {label: "Gerai", value: 4}
            ]
        }
    },
    skills: {
        label: " ",
        type: [Object],
        optional: true
    },
    "skills.$.name": {
        label: '',
        type: String,
        autoform: {
            afFieldInput: {
                defaultValue: ""
            },
            class: "attribute"
        }
    },
    "skills.$.experience": {
        label: 'Patirtis',
        type: Number,
        allowedValues: [0,1,2,3,4],
        defaultValue: 0,
        autoform: {
            class: "browser-default",
            options: [
                {label: "Pagrindai", value: 0},
                {label: "iki 1m.", value: 1},
                {label: "1-2m.", value: 2},
                {label: "3-4m.", value: 3},
                {label: "4m. ir daugiau", value: 4}
            ]
        }
    },
    //seminars: {
    //    type: [Object],
    //    optional: true
    //},
    //"seminars.$.name": {
    //    type: String
    //},
    //"seminars.$.organisator": {
    //    type: String
    //},
    //"seminars.$.date": {
    //    type: Date,
    //    autoform: {
    //        afFieldInput: {
    //            class: "datepicker"
    //        }
    //    }
    //},
    //"seminars.$.description": {
    //    type: String,
    //    optional: true,
    //    autoform: {
    //        afFieldInput: {
    //            type: "textarea"},
    //            class: "materialize-textarea"
    //        }
    //    }
    //},
    //organisations: {
    //    type: [Object],
    //    optional: true
    //},
    //"organisations.$.name": {
    //    type: String
    //},
    //"organisations.$.organisation": {
    //    type: String
    //},
    //"organisations.$.date_from": {
    //    type: Date,
    //    optional: true,
    //    autoform: {
    //        afFieldInput: {
    //            class: "datepicker"
    //        }
    //    }
    //},
    //"organisations.$.date_to": {
    //    type: Date,
    //    optional: true,
    //    autoform: {
    //        afFieldInput: {
    //            class: "datepicker"
    //        }
    //    }
    //},
    //"organisations.$.role": {
    //    type: String
    //},
    //"organisations.$.description": {
    //    type: String,
    //    optional: true,
    //    autoform: {
    //        afFieldInput: {
    //            type: "textarea"},
    //            class: "materialize-textarea"
    //        }
    //    }
    //},
    recommendations: {
        label: " ",
        type: [Object],
        optional: true
    },
    "recommendations.$.name": {
        label: 'Asmens vardas',
        regEx: /^([^0-9]*)$/,
        type: String
    },
    "recommendations.$.company": {
        label: 'Atstovaujama įmonė',
        type: String
    },
    "recommendations.$.role": {
        label: 'Užimamos pareigos',
        type: String
    },
    "recommendations.$.email": {
        label: 'El. paštas',
        type: String,
        autoform: {
            afFieldInput: {
                type: 'email'
            }
        }
    },
    "recommendations.$.phone": {
        label: 'Tel. Numeris',
        type: String,
        optional: true
    }
}));


Profiles.attachBehaviour('timestampable');
if(Meteor.isServer) {
    Profiles.allow({
        insert: function (userId, doc) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            // can only change your own documents
            return doc.user_id === userId;
        },
        remove: function (userId, doc) {
            // can only remove your own documents
            return doc.userId === userId;
        }
    });

    Meteor.publish('specificProfile', function(id) {
        var user = Meteor.users.findOne({"_id": this.userId});
        if(user.profile.type == 2) {
            var request = Requests.findOne({'company_id': this.userId, 'user_id': id});
            if(request != null) {
                if(request.status == 2) {
                    var profile = Profiles.find({'user_id': id});
                    console.log('hi');
                    return profile;
                }
            }
            return Profiles.find({'user_id': id},{fields: {"first_name": 0, "last_name": 0, "email": 0, "createdBy": 0, "updatedBy": 0, "education.institution_name": 0, "past_work.workplace_name": 0, "past_work.workplace_website": 0, 'picture': 0, 'phone': 0}});
        }
        return this.ready();
    });

    Meteor.publish('myProfile', function() {
        return Profiles.find({user_id: this.userId})
    });

    Meteor.publish('myBookmarkedProfiles', function() {
        var bookmarked = Bookmarks.find({company_id: this.userId}, {fields: {'user_id': 1}}).fetch();
        var array = [];
        for(var x=0; x<bookmarked.length; x++) {
            array.push(bookmarked[x].user_id);
        }
        return Profiles.find({user_id: {$in: array}}, {fields: {'user_id': 1, 'education': 1, 'languages': 1, 'past_work': 1, 'skills': 1, 'age': 1}});
    });

    Meteor.publish('myRequestedProfiles', function() {
        var requested = Requests.find({company_id: this.userId}, {fields: {'user_id': 1}}).fetch();
        var array = [];
        for(var x=0; x<requested.length; x++) {
            array.push(requested[x].user_id);
        }
        return Profiles.find({user_id: {$in: array}}, {fields: {'user_id': 1, 'education': 1, 'languages': 1, 'past_work': 1, 'skills': 1, 'age': 1}});
    });

    Meteor.publish('ticketProfiles', function() {

    });

    Meteor.publish('profilesForSearch', function() {
        return Profiles.find({},{fields: {"first_name": 0, "last_name": 0, "email": 0, "createdBy": 0, "updatedBy": 0, "education.institution_name": 0, "past_work.workplace_name": 0, "past_work.workplace_website": 0, 'picture': 0, 'phone': 0}});
    });

    Meteor.publish('profilesForStats', function() {
        return Profiles.find({}, {fields: {'_id': 1, 'work_area': 1}});
    })


}
