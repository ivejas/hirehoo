Images  = new FS.Collection("images", {
    stores: [new FS.Store.GridFS("images", {})]
});

Images.allow({
    download: function () {
        return true;
    },
    insert: function(){
        return true;
    },
    update: function() {
        return true;
    },
    fetch: null
});
if(Meteor.isServer) {
    Meteor.publish('postImages', function (id) {
        var request = Requests.findOne({'company_id': this.userId, 'user_id': id});
        if(request != null || id == this.userId) {
            var profile = Profiles.findOne({'user_id': id});
            var image = Images.find({"_id": profile['picture']});
            return image;
        }
        return this.ready();
    });

    Meteor.publish('companyImagesByCategory', function(id){
        var profiles = CompanyProfiles.find({'work_area': id}).fetch();
        console.log(profiles);
        var array = [];
        for(x=0; x<profiles.length; x++) {
            if(profiles[x].logo != null) {
                array.push(profiles[x].logo);
            }
        }
        return Images.find({"_id": {$in: array}});
    });

    Meteor.publish('companyImages', function(id) {
        var profile = CompanyProfiles.findOne({'user_id': id});
        var array = [];
        if(profile.logo != null){
            array.push(profile.logo);
        }
        if(profile.pictures != null) {
            for(x=0; x<profile.pictures.length; x++) {
                array.push(profile.pictures[x])
            }
        }
        var image = Images.find({"_id": {$in: array}});
        return image;
    });

    Meteor.publish('myPostImage', function(){
        var profile = Profiles.findOne({"user_id": this.userId});
        if(profile['picture'] != null) {
            return Images.find({"_id": profile['picture']})
        }
    });

    Meteor.publish('images', function() {
        return Images.find();
    });
};