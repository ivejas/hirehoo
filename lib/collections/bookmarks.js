Bookmarks = new Mongo.Collection("bookmarks");

Bookmarks.attachSchema(new SimpleSchema({
    company_id: {
        type: String
    },
    user_id: {
        type: String
    }
}));


Bookmarks.attachBehaviour('timestampable');

if(Meteor.isServer) {
    Meteor.publish('bookmarks', function() {
        return Bookmarks.find();
    });
    Meteor.publish('myBookmarks', function(id) {
        return Bookmarks.find({company_id: this.userId});
    });
    Meteor.publish('bookmarkList', function() {
        return Bookmarks.find({}, {"_id": 1});
    });
}