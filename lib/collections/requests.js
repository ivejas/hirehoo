Requests = new Mongo.Collection("requests");

Requests.attachSchema(new SimpleSchema({
    company_id: {
        type: String
    },
    user_id: {
        type: String
    },
    token: {
        type: String
    },
    ticket_token: {
        type: String
    },
    status: {
        type: Number
    }
}));


Requests.attachBehaviour('timestampable');

if(Meteor.isServer) {
    Meteor.publish('myRequests', function() {
        return Requests.find({"company_id": this.userId}, {fields: {'token': 0, 'ticket_token': 0}});
    });

    Meteor.publish('userRequests', function() {
        return Requests.find({"user_id": this.userId}, {fields: {"user_id": 1, "company_id": 1, "createdAt": 1, "status": 1, "token": 1}});
    });

    Meteor.publish('myReceivedRequests', function() {
        return Requests.find({"user_id": this.userId}, {fields: {"user_id": 1}});
    });

    Meteor.publish('sucRequestList', function() {
        return Requests.find({"status": 2}, {fields: {"_id": 1}});
    });
}
