SuggestedCities = new Mongo.Collection("suggestedCities");

SuggestedCities.attachSchema(new SimpleSchema({
    city: {
        type: String
    },
    user_id: {
        type: String
    }
})
);


SuggestedCities.attachBehaviour('timestampable');

if(Meteor.isServer) {
    Meteor.publish('suggestedCities', function() {
        return SuggestedCities.find();
    });
    Meteor.publish('myCities', function(id) {
        return SuggestedCities.find({user_id: id});
    });
}

