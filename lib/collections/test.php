<?php
	ini_set('memory_limit', '400M');
	ini_set('max_execution_time', 0);
	header('Content-Type: text/html; charset=utf-8');

	$_SERVER['DOCUMENT_ROOT'] = "/kunden/84739_38100/webseiten/ap24.ebay";
	require_once($_SERVER['DOCUMENT_ROOT']."/ap24.includes/startloader_base.php");

	if(!session_id())session_start();

	$config 					= parse_ini_file(EBAY_INI_FILE, true);
	$site 						= $config['settings']['site'];
	$compatibilityLevel 		= $config['settings']['compatibilityLevel'];
	$token 						= $config[$site]['authToken'];
	$sDuration 					= $config[$site]['ListingDuration'];
	$sPayPalEmail 				= $config[$site]['PayPalEmailAddress'];
	$dev 						= $config[$site]['devId'];
	$app 						= $config[$site]['appId'];
	$cert 						= $config[$site]['cert'];
	$location 					= $config[$site]['gatewaySOAP'];
	$MIP_Server 				= $config[$site]['MIP_Server'];
	$MIP_User 					= $config[$site]['MIP_User'];
	$MIP_Pass 					= $config[$site]['MIP_Pass'];

	$SE_DK_WECHSELKURS = SE_EXCHANGE_RATE;

	$sRefPrefix = "D".substr(date('Y'),-1).date('W');

	$inventoryEmpty = true;

	$iOutputCounter = 1;
	$iInventoryOutputCounter = 1;
	$iCounter = 1;
	$iInventoryCounter = 1;
	$iUploads = 10000;
	$iCountsPerUpload = 25000;
	$iDay = date('z');
	$bTestMode = false;
	$iTotalPrice = 0;

	$sOutputFileLog = "D2_".$iDay."_".$iOutputCounter."_log.csv";
	$hFilePointerLog = fopen($sOutputFileLog, "w");


	$session = new eBaySession($dev, $app, $cert);
	$session->token = $token;
	$session->site = 77;
	$session->location = $location;

	/*
		aItemArray will be used to store information about item that will require relisting.
	*/
	$aItemArray = array();


	$sOutputFile = "D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.csv";
	$hFilePointer = fopen($sOutputFile, "w");

	/*
		Script logic begins
		All clients are selected from DB84739_12
	*/
	$oSQL = new SQL(DB_NAME_12);
	$sQuery = "
		select * from clients where ID = 1
	";
	$oSQL->Query( $sQuery );
	$_SESSION['ClientData'] = $oSQL->getNext();

	/*
	Remote area owner array
	*/
	$remoteAreaOwners = array("0036", "0127", "0300", "0145", "0026", "0030");

	/*
	calculate remote shipping fee
	*/
	$remoteShippingExtraFee = 100;

	$FrPrice = new Price($remoteShippingExtraFee, "DE", 0);
	$RemoteShippingFeeEuro = $FrPrice->GetSalesPriceBrutto();



	$oSQL = new SQL(DB_NAME_8);

	$aDescriptionText = array(
		'Build' => 'Baujahr',
		'km' => 'Laufleistung',
		'Motor' => 'Motor',
		'Motorcode' => 'Motorcode',
		'Getriebe' => 'Getriebe',
		'Getriebecode' => 'Getriebecode',
		'ArticleManufacturer' => 'Artikelhersteller',
		'ArticleManufacturerCode' => 'Herstellercode',
		'Quality' => utf8_decode("Qualität"),
		'ReferenceNo' => 'Referenznummer'

	);
	$oTotalTimer = new Timer();
	$oTotalTimer->start();
	$iCounter = 1;

	$oSQL = new SQL(DB_NAME_8);
	$aArticles = array();
	/*
		All default images that have ActiveBS (Active BergundStroem) values set to yes are selected
	*/
	$sQuery = "
		SELECT *
		FROM `ebay_default_images`
		WHERE `Active` = 'Y'
		AND `ActiveBS` = 'Y'
		AND BS <> ''
		order by ID
	";

	$hMSQ = mysql_unbuffered_query($sQuery) or die(mysql_error());
	while($aData = mysql_fetch_assoc($hMSQ)){
		array_push($aArticles, $aData);
	}

	$articleDataArray = array();
	$articleDataCounter = 0;

	mysql_free_result($hMSQ);

	foreach($aArticles as $i => $aArticleData){

		$iArticleID = $aArticleData['ID'];
		$sGetAllKtypes = $aArticleData['AllKtypes'];
		$sArticlenamePlain = $aArticleData['Name'];
		$iPrimaryCategory = $aArticleData['Category'];

		$oSQL = new SQL(DB_NAME_4);

		$sAddTextQuery = "select DEUTSCH from ".TABLE_LOCALES_LONG." where SHORTCUT = '".$iArticleID."_AddText"."'";
		$hMSQ = mysql_unbuffered_query($sAddTextQuery) or die(mysql_error() . "<br />" . $sAddTextQuery);
		if(mysql_num_rows($hMSQ) > 0){
			$aAddTextData = mysql_fetch_assoc($hMSQ);
			$sAddText = trim($aAddTextData['DEUTSCH']);
		}else{
			$sAddText = "";
		}

		mysql_free_result($hMSQ);
		$oSQL = new SQL(DB_NAME_8);

		$sQuery = "
			SELECT a.*, f.*
			FROM `articles_new` a
			LEFT JOIN fracht f ON a.FRACHTGRUPPE = f.ID
			WHERE a.ID = ".$iArticleID."
		";
		$hMSQ = mysql_unbuffered_query($sQuery) or die(mysql_error() . "<br />" . $sQuery);
		$aArticleData = mysql_fetch_assoc($hMSQ);
		if($aArticleData['BERGUNDSTROEM'] == null) {
			continue;
		}
		$sArticlenamePlain = str_replace(' rund', '', $sArticlenamePlain);
		$sArticlenamePlain = str_replace(' vierkantig', '', $sArticlenamePlain);

		unset($sPosition);
		include('../cronjobs/ebay_export_position.php');

		$aPosition = explode(' ', $sPosition);
		$aPosition = array_unique($aPosition);
		$sPosition = implode(' ', $aPosition);

		$aArticlename = explode(' ', $sArticlenamePlain . $sPosition);
		$aArticlename = array_unique($aArticlename);
		$sArticlename = implode(' ', $aArticlename) ;

		$oPrice = new Price( ($aArticleData['PREIS3']), "DE", 0);
		$iFrachtpreis	= 	$oPrice->GetSalesPriceBrutto();
		$iWechselkurs = $SE_DK_WECHSELKURS;
		$iReturnprice = $iFrachtpreis;
		$time = date("Y-m-d H:m:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")-14, date('Y')));
		$sSubQuery = "
				select b.*
				from ".TABLE_ATRLAGER." b
				where b.ArticleID in (".$aArticleData['BERGUNDSTROEM'].")
				and b.ModelID > 0
				and (Tecdoc > 0 and Tecdoc is not null)
				and Image > 0
				and ebayReady_de IS NOT NULL
				and (ebay <= '".$time."' OR ebay IS NULL)
				ORDER BY ebay IS NULL DESC, ebay DESC
		";
		$hMSQ2 = mysql_query($sSubQuery) or die(mysql_error());
		/*
			This logic is used to run through all item entries in the database and pick out those need to be relisted.
			Currently items that do not have SKU_TEMP are selected to be relisted automatically to apply SKU_TEMP.
			This is done to ensure that items will have SKU_TEMPs assigned because they will be used to decide wheteher
				item has to be relisted.

			If SKU_TEMP is present in the item entry - it is checked against current day count and if the difference is
				larger or equal to 14 days - the item is marked for relisting.

			Days were used instead of weeks because it will easier to handle change of year. Currently if deducting
				upload day number from current day number will results in a negative result - this will mean that a
				new year has started and appropriate adjustments will be applied.
		*/

		while($aData = mysql_fetch_assoc($hMSQ2)){
			$newFiles = true;
			$error = false;
			$tempArray = array($aData, $articleDataCounter);
			$aItemArray[] = $tempArray;
			$aItemSKUArray[] = $sSku;
			// $stockArray = checkInInventory($aData['Owner'], $aData['RefNr']);
			// if(count($stockArray) != 0) {
			// 	$client = new eBaySOAP($session);
			// 	foreach($stockArray as $stockSKU) {
			// 		try {
			// 			$aInput = array(
			// 				'Version' 			=> $compatibilityLevel,
			// 				'EndingReason'		=> 'NotAvailable',
			// 				'ItemID'		=> $stockSKU[1]
			// 			);
			// 		$oOutput = $client->EndFixedPriceItem($aInput);
			// 		pre($oOutput);
			// 		} catch (SOAPFault $f) {
			// 			fwrite($hFilePointerInventoryLog, "SOAP-Error: " . $f . "\n");
			// 			die();
			// 		}
			// 	}
			// } else {
			// 	pre('Not in stock');
			// }

			$iCounter++;
			pre("Counter ".$iCounter);
			if($iCounter == $iUploads){
					$tempArray = array($sArticlenamePlain, $sPosition, $iFrachtpreis, $iWechselkurs, $iReturnprice, $aArticleData, $sAddText,
						$iArticleID, $sGetAllKtypes, $iPrimaryCategory, $sArticlename);
					$articleDataArray[] = $tempArray;
					$articleDataCounter++;
					unset($tempArray);
					pre('InventoryLimit Reached');
					break 2;
				}

			unset($aData);
		}
		$tempArray = array($sArticlenamePlain, $sPosition, $iFrachtpreis, $iWechselkurs, $iReturnprice, $aArticleData, $sAddText,
			$iArticleID, $sGetAllKtypes, $iPrimaryCategory, $sArticlename);
		$articleDataArray[] = $tempArray;
		$articleDataCounter++;
		unset($tempArray);
	}

	fclose($hFilePointerOut);
	mysql_free_result($hMSQ2);
	unset($aData);

		$inventoryEmpty = true;
		$iCounter = 1;
	/*
		Information about items marked for relisting is retrieved from aItemArray instead of the database.
	*/
		$oSQL = new SQL(DB_NAME_8);
		pre(count($aItemArray));

	for($x=0; $x<count($aItemArray); $x++) {
		$aData = $aItemArray[$x][0];
		$sArticlenamePlain = $articleDataArray[$aItemArray[$x][1]][0];
		$sPosition = $articleDataArray[$aItemArray[$x][1]][1];
		$iFrachtpreis = $articleDataArray[$aItemArray[$x][1]][2];
		$iWechselkurs = $articleDataArray[$aItemArray[$x][1]][3];
		$iReturnprice = $articleDataArray[$aItemArray[$x][1]][4];
		$aArticleData = $articleDataArray[$aItemArray[$x][1]][5];
		$sAddText = $articleDataArray[$aItemArray[$x][1]][6];
		$iArticleID = $articleDataArray[$aItemArray[$x][1]][7];
		$sGetAllKtypes = $articleDataArray[$aItemArray[$x][1]][8];
		$iPrimaryCategory = $articleDataArray[$aItemArray[$x][1]][9];
		$sArticlename = $articleDataArray[$aItemArray[$x][1]][10];

			/*
				Original listing generation logic is applied here.
			*/
			$sTestStr = strtolower(implode(', ', $aData));
			if(strpos('endast', $sTestStr) !== false)continue;
			if($aData['Quality1'] == 'M')continue;
			if($aData['Quality2'] == 'M')continue;
			if($aData['Preis'] <= 200)continue;
			if($aData['Preis'] <= 1000 && $aData['ArticleID'] == 7255)continue;
			if(!empty($aArticleData['Build_DE'])){
				if($aData['Build'] < $aArticleData['Build_DE'])
					continue;
			}
			if($aArticleData['OEM_DE'] == 'Y'){
				$aONumbers = array();
				if(!empty($aData['BoschNr1']))array_push($aONumbers, $aData['BoschNr1']);
				if(!empty($aData['BoschNr2']))array_push($aONumbers, $aData['BoschNr2']);
				if(!empty($aData['OriginalNr']))array_push($aONumbers, $aData['OriginalNr']);
				$aONumbers = array_unique($aONumbers);
				if(empty($aONumbers))continue;
			}

			$iModel = $aData['ModelID'];
			$sModelQuery = "
				select mo.Hersteller, mo.Modell, mo.BJvon, mo.BJbis,  mo.KTypes
				from modelle mo
				where mo.BERGUNDSTROEM like '%".$iModel."%' and mo.ACTIVE = 'Y'
				limit 1
			";
			$hMSQModel = mysql_query($sModelQuery) or die(mysql_error());
			if(mysql_num_rows($hMSQModel) != 1){
				$sMissingModelQuery = "
					insert ignore into missing_models set ModelID = '".$iModel."'
				";
				$hMSQModelsMissing = mysql_query($sMissingModelQuery) or die(mysql_error());
				continue;
			}
			$sModelData = mysql_fetch_assoc($hMSQModel);
			$aData['Hersteller'] = $sModelData['Hersteller'];
			$aData['Modell'] = $sModelData['Modell'];
			$aData['BJvon'] = $sModelData['BJvon'];
			$aData['BJbis'] = $sModelData['BJbis'];
			$aData['KTypes'] = (empty($aData['Tecdoc'])) ? $sModelData['KTypes'] : $aData['Tecdoc'];
			if(empty($aData['KTypes'])){
				$sKtypeQuery0 = "
					insert ignore into missing_ktypes set
					ModelID = '".$iModel."',
					Hersteller = '".$aData['Hersteller']."',
					Modell = '".$aData['Modell']."',
					BJ_From = '".$aData['BJvon']."',
					BJ_Until = '".$aData['BJbis']."'
				";
				mysql_query($sKtypeQuery0) or die(mysql_error());
				continue;
			}


			foreach($aData as $key => $value){
				$value = str_replace(';', ',', $value);
				$value = nl2br($value);
				$value = mysql_real_escape_string($value);
				$aData[$key] = $value;
			}

			$sBJ = '';
			if($aData['BJvon']){$sBJ = ''.$aData['BJvon'].' -';}
			if($aData['BJbis']){$sBJ .= ' ' . $aData['BJbis'];}
			if($aData['BJvon']){$sBJ .= '';}
			unset($sPosition);

			include('../cronjobs/ebay_export_position.php');

			$aPosition = explode(' ', $sPosition);
			$aPosition = array_unique($aPosition);
			$sPosition = implode(' ', $aPosition);
			$aArticlename = explode(' ', $sArticlenamePlain . $sPosition);
			$aArticlename = array_unique($aArticlename);
			$sArticlename = implode(' ', $aArticlename) ;
			$aItem = array();
			array_push($aItem, '3000'); // Condition: used

			$aHeadlineAdds = array();

			$aONumbers = array();
			if(!empty($aData['BoschNr1']))array_push($aONumbers, $aData['BoschNr1']);
			if(!empty($aData['BoschNr2']))array_push($aONumbers, $aData['BoschNr2']);
			if(!empty($aData['OriginalNr']))array_push($aONumbers, $aData['OriginalNr']);
			$aONumbers = array_unique($aONumbers);
			$aData['BoschNr1'] = implode(' / ', $aONumbers);


			if(!empty($aData['Getriebe'])){
				$aData['Getriebe'] = str_replace("vxl", "Gang", $aData['Getriebe']);
				$aData['Getriebe'] = str_replace("VXL", "Gang", $aData['Getriebe']);
				$aData['Getriebe'] = str_replace("VX", "Gang", $aData['Getriebe']);
			}
			if(!empty($aData['Getriebecode'])){
				$aData['Getriebecode'] = str_replace("vxl", "Gang", $aData['Getriebecode']);
				$aData['Getriebecode'] = str_replace("VXL", "Gang", $aData['Getriebecode']);
				$aData['Getriebecode'] = str_replace("VX", "Gang", $aData['Getriebecode']);
			}


			$aEbayUpload['HEADLINE'] = $aData['Hersteller'] . " " . $aData['Modell'] . " " . $sArticlename;

			if(!empty($aData['Motorcode']))array_push($aHeadlineAdds, $aData['Motorcode']);
			if(!empty($aData['BoschNr1']))array_push($aHeadlineAdds, $aData['BoschNr1']);
			if(!empty($aData['Build']))array_push($aHeadlineAdds, "Bj ".$aData['Build']);
			if(!empty($aData['Miles']))array_push($aHeadlineAdds, $aData['Miles'] * 10 . " km");
			$sHeadlineAdds = implode(" - ", array_unique($aHeadlineAdds));
			if(!empty($sHeadlineAdds))$sHeadlineAdds = " - " . $sHeadlineAdds;

			$sTitle = substr($aEbayUpload['HEADLINE'] . $sHeadlineAdds, 0, 79);

			array_push($aItem, utf8_encode($sTitle)); // Title

			/*
			Make price
			*/
			$oPrice = new Price( ($aData['Preis'] * $iWechselkurs * 0.8), "DE", 1);
			$iPreis	= 	$oPrice->GetSalesPriceBrutto();

			/*
			Check for remote area dismantlers and add to
			*/
			if (in_array($aData['Owner'], $remoteAreaOwners))
			$iMyPreis = $iPreis + $iFrachtpreis + $RemoteShippingFeeEuro;
			else
			$iMyPreis = $iPreis + $iFrachtpreis;

			$iMyPreis = $iMyPreis / 100 * 110;


			$iTotalPrice += $iMyPreis.'d';

			array_push($aItem, sprintf("%01.2f", (ceil($iMyPreis )))); // StartPrice
			array_push($aItem, $iPrimaryCategory); // Item_PrimaryCategory

			$sSKU = $sRefPrefix . "_".$aData['Owner']."_".$aData['RefNr'];
			array_push($aItem, $sSKU); // Item_SKU

			$sImage = '';
			$sInnerImage1 = "http://ebay.autoparts24.eu/images/defaultimages/".$iArticleID."_example.jpg";
			$sImageLast = "A";
			$iFirst = $aData['RefNr']{3};
			$iFirst = substr($aData['RefNr'], -3, 1);

			$sServerImage = "/kunden/84739_38100/atracco/kund_".$aData['Owner']."/i".$iFirst."/L".$aData['RefNr'].$sImageLast.".jpg";
			$sWebImage = "http://eimages.autoparts24.eu/kund_".$aData['Owner']."/i".$iFirst."/L".$aData['RefNr'].$sImageLast.".jpg";
			if(is_readable($sServerImage) && filesize($sServerImage) > 5000){
				$sInnerImage1 = $sWebImage;
			}else{

				$sMissingImagesQuery = "
					insert into missing_images set
					Owner = '".$aData['Owner']."',
					RefNr = '".$aData['RefNr']."',
					ArticleID  = '".$aData['ArticleID']."',
					Images = '".$aData['Images']."'
				";
				mysql_query($sMissingImagesQuery) or die(mysql_error());

				continue;
			}

			$sImageLast = "B";
			$sServerImage = "/kunden/84739_38100/atracco/kund_".$aData['Owner']."/i".$iFirst."/L".$aData['RefNr'].$sImageLast.".jpg";
			$sWebImage = "http://eimages.autoparts24.eu/kund_".$aData['Owner']."/i".$iFirst."/L".$aData['RefNr'].$sImageLast.".jpg";
			$sInnerImage2 = '';
			if(is_readable($sServerImage) && filesize($sServerImage) > 5000){
				$sInnerImage2 = $sWebImage;
			}

			$sImageLast = "C";
			$sServerImage = "/kunden/84739_38100/atracco/kund_".$aData['Owner']."/i".$iFirst."/L".$aData['RefNr'].$sImageLast.".jpg";
			$sWebImage = "http://eimages.autoparts24.eu/kund_".$aData['Owner']."/i".$iFirst."/L".$aData['RefNr'].$sImageLast.".jpg";
			$sInnerImage3 = '';
			if(is_readable($sServerImage) && filesize($sServerImage) > 5000){
				$sInnerImage3 = $sWebImage;
			}

			array_push($aItem, $sInnerImage1); // Item_Image1
			array_push($aItem, $sInnerImage1); // Item_Image2
			array_push($aItem, $sInnerImage2); // Item_Image3
			array_push($aItem, $sInnerImage3); // Item_Image4

			array_push($aItem, 3); // MinTime
			array_push($aItem, 4); // MaxTime
			$aDescription = array();
			$aDescription['Artikel'] = utf8_encode($aData['Hersteller'] . " " . $aData['Modell'] . " " . $sArticlename);
				if(!empty($aData['Anmerkung']))$aDescription[$aDescriptionText['ArticleManufacturer']] = $aData['Anmerkung'];
				if(!empty($aData['BoschNr1']))$aDescription[$aDescriptionText['ArticleManufacturerCode']] = $aData['BoschNr1'];
				if(!empty($aData['Motortyp']))$aDescription[$aDescriptionText['Motor']] = $aData['Motortyp'];
				if(!empty($aData['Motorcode']))$aDescription[$aDescriptionText['Motorcode']] = $aData['Motorcode'];
				if(!empty($aData['Build']))$aDescription[$aDescriptionText['Build']] = $aData['Build'];
				if(!empty($aData['Getriebe']))$aDescription[$aDescriptionText['Getriebe']] = $aData['Getriebe'];
				if(!empty($aData['Anmerkung']))$aDescription[$aDescriptionText['Anmerkung']] = $aData['Anmerkung'];
				if(!empty($aData['Getriebecode']))$aDescription[$aDescriptionText['Getriebecode']] = $aData['Getriebecode'];
				if($aData['Miles'] > 0){
					$aDescription[$aDescriptionText['km']] = $aData['Miles'] * 10 . " km";
					$sKM = $aData['Miles'] * 10 . " km";
				}


			array_push($aItem, $aDescription['Artikel']); // Item_Description_Title
			array_push($aItem, utf8_encode($aData['BoschNr1'])); // Item_Description_BoschNr1
			array_push($aItem, utf8_encode($aData['Motortyp'])); // Item_Description_Motortyp
			array_push($aItem, utf8_encode($aData['Motorcode'])); // Item_Description_Motorcode
			array_push($aItem, utf8_encode($aData['Build'])); // Item_Description_Build
			array_push($aItem, utf8_encode($aData['Getriebe'])); // Item_Description_Getriebe
			array_push($aItem, utf8_encode($aData['Getriebecode'])); // Item_Description_Getriebecode
			array_push($aItem, $sKM); // Item_Description_km
			array_push($aItem, utf8_encode($aData['Anmerkung'])); // Item_Description_Anmerkung
			array_push($aItem, utf8_encode($aData['ChassisNr'])); // Item_Description_Fahrgestellnummer


			$aKTypes = array();
			if(!empty($aData['KTypes'])){
				$aTMP = explode(",", $aData['KTypes']);
				foreach($aTMP as $i => $iKType){array_push($aKTypes, $iKType);}
			}


			if(!empty($aKTypes)){
				$iFirstKType = $aKTypes[0];
				$sKTypeQuery = "
					select Platform from ebay_modelle where KType = ".$iFirstKType." and Platform <> '--'
				";
				$hMSQ3 = mysql_query($sKTypeQuery) or die(mysql_error());
				$aKData = mysql_fetch_assoc($hMSQ3);
			}

			if($sGetAllKtypes == 'Y' && mysql_num_rows($hMSQ3) > 0){
				$sKTypeQuery2 = "
					select KType from ebay_modelle
					where Platform = '".$aKData['Platform']."'
					and Manufacturer = '".$aData['Hersteller']."'
				";
				$hMSQ4 = mysql_query($sKTypeQuery2) or die(mysql_error());
				if(mysql_num_rows($hMSQ4) > 0){
					$aKTypes = array();
					while($aKData3 = mysql_fetch_assoc($hMSQ4)){
						array_push($aKTypes, $aKData3['KType']);
					}
				}
			}
			sort($aKTypes);
			$sKTypes = implode('|', array_unique($aKTypes));
			array_push($aItem, $sKTypes); // Item_ItemCompatibilityList_Compatibility



			array_push($aItem, utf8_encode($aData['Hersteller'])); // Item_ItemSpecifics_Marke
			array_push($aItem, utf8_encode($aData['Modell'])); // Item_ItemSpecifics_Modell
			array_push($aItem, utf8_encode($sBJ)); // Item_ItemSpecifics_Bauzeitraum
			array_push($aItem, utf8_encode($aKData['Platform'])); // Item_ItemSpecifics_Platform
			array_push($aItem, utf8_encode($sArticlename)); // Item_ItemSpecifics_Artikelbezeichnung
			array_push($aItem, 1); // Item_Quantity
			array_push($aItem,  "gratis"); // Item_Returnprice
			array_push($aItem, utf8_encode($sAddText)); // Item_AddText

			$sLine = implode(';', $aItem) . PHP_EOL;
			$sLine = stripslashes( $sLine );
			$sLine = str_replace('"', '', $sLine);
			fwrite($hFilePointer,  $sLine);

			$sLogOutput = $aData['ArticleID'] .";".$sSKU;
			fwrite($hFilePointerLog,  $sLogOutput . PHP_EOL);

			$inventoryEmpty = false;
			$iCounter++;
			pre("Product: ".$iCounter);

			if($iCounter == $iUploads){
					$limitReached = true;
					pre('ProductLimit Reached');
					break;
				}

			if(!($iCounter % 500)){
				$iPriceNotifier = sprintf("%01.2f", $iTotalPrice );
			}
			//unsetting data to save space
			$aItemArray[$x] = null;
			if($iCounter % $iCountsPerUpload == 0){
				pre($counter);
				pre('started');
				die();

				fclose($hFilePointer);
				$sCommand = "cd ". $_SERVER['DOCUMENT_ROOT'] ."/mip_codes/; zip -Dqu "."D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip ".$sOutputFile ;
				system($sCommand);

				if($bTestMode == false){
					try
					{
						$sftp = new SFTPConnection($MIP_Server, 22);
						$sftp->login($MIP_User, $MIP_Pass);
						$sLocalFile = $_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip";
						$sUploadFile = "/store/product/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip";

						$bUpload = $sftp->uploadFile($sLocalFile, $sUploadFile);
						if($bUpload == true){
							$oSQL = new SQL(DB_NAME_8);
							$sLogQuery = "
								insert into ebay_upload_log set
								Filename = '".mysql_real_escape_string("D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip")."',
								Source = 'D',
								Counter = ".$iOutputCounter.",
								Datum = CURDATE()
							";
							mysql_query($sLogQuery) or die(mysql_error());

							unlink($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.csv");
							unlink($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip");
						}
					}
					catch (Exception $e)
					{
						echo $e->getMessage() . "\n";
					}



				$looper = true;
				$sOutputFileZip = "D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip";
				$fileDone = false;
				while($looper == true) {
					sleep(10);

					try {
						$sftp = new SFTPConnection($MIP_Server, 22);
						$sftp->login($MIP_User, $MIP_Pass);
						$filesystem = $sftp->scanFilesystem("/store/product/inprocess/");
						pre($filesystem);
						if(!empty($filesystem) && $fileDone == false) {
							foreach($filesystem as $filePointer) {
								if(strpos($filePointer, $sOutputFileZip) !== false) {
									$file = $filePointer.".xml";
								} else {
									$fileDone = true;
								}
							}
						} else {
							$looper = false;
							$dateArray = explode('-', $file);
							$locationRoot = "/store/product/output/".$dateArray[1]."-".$dateArray[2]."-".$dateArray[3]."/";

							$location = $locationRoot.$file;
							$sftp->downloadFile($location, $_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D_RELIST_PRO_OUTPUT.xml");
							$output = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D_RELIST_PRO_OUTPUT.xml");
							$oSQL = new SQL(DB_NAME_8);
							if(isset($output->RequestDetails->Errors)) {
								foreach($output->RequestDetails->Errors->Error as $i => $error) {
									$sQuery = '
										insert ignore into ebay_upload_errors
											set SKU = "Non-specific_'.rand().'",
											Errorcode = "'.mysql_real_escape_string($error->Code).'",
											Errormessage = "'.mysql_real_escape_string($error->Message).'",
											source = "iways_relist_cron.php product_output",
											timestamp = "'.date('Y-m-d G:i:s').'"
									';
									$hMSQ2 = mysql_query($sQuery) or die(mysql_error());
								}
							}
							$oSQL = new SQL(DB_NAME_8);
							foreach($output->ProductResult as $i => $response) {
								if($response->Result == 'Success') {
									$sku = explode("_", $response->Skus->Sku);
									updateSKU($sku[1], $sku[2], $response->Skus->Sku);
									continue;
								} else {

									$sQuery = "
										insert into ebay_upload_errors (SKU, Errorcode, Errormessage, source, timestamp) VALUES
											('".mysql_real_escape_string($response->Skus->Sku)."', '".mysql_real_escape_string($response->Errors->Error->Code)."', '".mysql_real_escape_string($response->Errors->Error->Message)."',
												'iways_relist_cron.php product_output', '".date('Y-m-d G:i:s')."')
											ON DUPLICATE KEY UPDATE
											SKU = '".mysql_real_escape_string($response->Skus->Sku)."',
											Errorcode = '".mysql_real_escape_string($response->Errors->Error->Code)."',
											Errormessage = '".mysql_real_escape_string($response->Errors->Error->Message)."',
											source = 'iways_relist_cron.php product_output',
											timestamp = '".date('Y-m-d G:i:s')."'
									";
									$hMSQ2 = mysql_query($sQuery) or die(mysql_error());
								}
							}
						}
					} catch (Exception $e) {
						echo $e->getMessage() . "\n";
						die();
					}
				}
				unlink($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D_RELIST_PRO_OUTPUT.xml");
				}

				$iOutputCounter++;

				if($iCounter == $iUploads){
					pre("TotalPrice: " . $iTotalPrice);

					$oTotalTimer->stop();
					$iTotaltime = $oTotalTimer->getTime();
					echo $iTotaltime;
					pre($iCounter);
					exit;
				}

				$sOutputFile = "D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.csv";
				$hFilePointer = fopen($sOutputFile, "w");

			}

		}
	fclose($hFilePointer);
	pre($counter);
	pre('yo');
	die();
	// final upload
	if($inventoryEmpty == false) {
		if(!file_exists($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip")){




			$sCommand = "cd ". $_SERVER['DOCUMENT_ROOT'] ."/mip_codes/; zip -Dqu "."D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip ".$sOutputFile ;
			system($sCommand);

			if($bTestMode == false){
				pre("final product upload started");
				try
				{

					$sftp = new SFTPConnection($MIP_Server, 22);
					$sftp->login($MIP_User, $MIP_Pass);

					$sLocalFile = $_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip";
					$sUploadFile = "/store/product/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip";
					$bUpload = $sftp->uploadFile($sLocalFile, $sUploadFile);
					$oSQL = new SQL(DB_NAME_8);
					if($bUpload == true){
						$sLogQuery = "
							insert into ebay_upload_log set
							Filename = '".mysql_real_escape_string("D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip")."',
							Source = 'D',
							Counter = ".$iOutputCounter.",
							Datum = CURDATE()
						";
						mysql_query($sLogQuery) or die(mysql_error());

						unlink($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.csv");
						unlink($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D2_".$iDay."_".$iOutputCounter."_autoparts24.eu.zip");
					}

				}
				catch (Exception $e)
				{
					print_r("\n");
					echo $e->getMessage() . "\n";
					die();
				}

			}

		}
	if($bTestMode == false){

		$sOutputFileZip = $iDay."_".$iOutputCounter."_autoparts24.eu.zip";
		$looper = true;
		$file = null;
		$fileDone = false;
		while($looper == true) {
				sleep(10);
				try {
					$sftp = new SFTPConnection($MIP_Server, 22);
					$sftp->login($MIP_User, $MIP_Pass);
					$filesystem = $sftp->scanFilesystem("/store/product/inprocess/");
					pre($filesystem);
					if(!empty($filesystem) && $fileDone == false) {
						foreach($filesystem as $filePointer) {
							if(strpos($filePointer, $sOutputFileZip) !== false) {
								$file = $filePointer.".xml";
							} else {
								$fileDone = true;
							}
						}
					} else {
						$looper = false;
						$dateArray = explode('-', $file);
						$locationRoot = "/store/product/output/".$dateArray[1]."-".$dateArray[2]."-".$dateArray[3]."/";
						$location = $locationRoot.$file;
						$sftp->downloadFile($location, $_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D_RELIST_PRO_OUTPUT.xml");
						$output = simplexml_load_file($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D_RELIST_PRO_OUTPUT.xml");
						$oSQL = new SQL(DB_NAME_8);
						if(isset($output->RequestDetails->Errors)) {
							foreach($output->RequestDetails->Errors->Error as $i => $error) {
								$sQuery = '
									insert ignore into ebay_upload_errors
										set SKU = "Non-specific_'.rand().'",
										Errorcode = "'.mysql_real_escape_string($error->Code).'",
										Errormessage = "'.mysql_real_escape_string($error->Message).'",
										source = "iways_relist_cron.php product_output",
										timestamp = "'.date('Y-m-d G:i:s').'"
								';
								$hMSQ2 = mysql_query($sQuery) or die(mysql_error());
							}
						}
						foreach($output->ProductResult as $i => $response) {
							if($response->Result == 'Success') {
								$sku = explode("_", $response->Skus->Sku);
								updateSKU($sku[1], $sku[2], $response->Skus->Sku);
								continue;
							} else {
								$sQuery = "
									insert into ebay_upload_errors (SKU, Errorcode, Errormessage, source, timestamp) VALUES
										('".mysql_real_escape_string($response->Skus->Sku)."', '".mysql_real_escape_string($response->Errors->Error->Code)."', '".mysql_real_escape_string($response->Errors->Error->Message)."',
											'iways_relist_cron.php product_output', '".date('Y-m-d G:i:s')."')
										ON DUPLICATE KEY UPDATE
										SKU = '".mysql_real_escape_string($response->Skus->Sku)."',
										Errorcode = '".mysql_real_escape_string($response->Errors->Error->Code)."',
										Errormessage = '".mysql_real_escape_string($response->Errors->Error->Message)."',
										source = 'iways_relist_cron.php product_output',
										timestamp = '".date('Y-m-d G:i:s')."'
								";
								pre('Error logged');
								$hMSQ2 = mysql_query($sQuery) or die(mysql_error());
							}
						}
					}
				} catch (Exception $e) {
					echo $e->getMessage() . "\n";
					die();
				}
			}
			unlink($_SERVER['DOCUMENT_ROOT'] ."/mip_codes/D_RELIST_PRO_OUTPUT.xml");
		}
	}

	pre("TotalPrice: " . $iTotalPrice);

	$oTotalTimer->stop();
	$iTotaltime = $oTotalTimer->getTime();
	echo $iTotaltime;
	pre($iCounter);


	function checkInInventory($sOwner, $sRefNr) {
		$oSQL = new SQL(DB_NAME_16);
		$sSubQuery = "
			SELECT * FROM stock_de
			WHERE Owner = '".$sOwner."' AND RefNr = '".$sRefNr."'
			";
		$hMSQ2 = mysql_unbuffered_query($sSubQuery) or die(mysql_error());
		$skuArray = array();
		while($stockData = mysql_fetch_assoc($hMSQ2)){
			$skuArray[] = array($stockData['SKU'], $stockData['ItemID']);
		}
		return $skuArray;
	}

	function updateSKU($sOwner, $sRefNr, $sSku) {
		$oSQL = new SQL(DB_NAME_8);
		$sUpdateQuery = "
			UPDATE atr_".$sOwner." SET
			SKU_DE = '". $sSku."',
			ebay = '".date("Y-m-d H:i:s")."'
			WHERE RefNr = '".$sRefNr."'
			AND Owner = '".$sOwner."'";
			pre($sUpdateQuery);
		mysql_query($sUpdateQuery) or die(mysql_error());
		return true;
	}
?>






