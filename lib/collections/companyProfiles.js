CompanyProfiles = new Mongo.Collection("companyProfiles");

CompanyProfiles.attachSchema(new SimpleSchema({
    user_id: {
        type: String
    },
    approved: {
        type: Number,
        defaultValue: 0
    },
    company_name: {
        label: 'Įmonės pavadinimas',
        type: String,
        max: 50

    },
    company_code: {
        max: 20,
        label: 'Įmonės kodas',
        type: String
    },
    vat_code: {
        max: 30,
        label: 'PVM mokėtojo kodas',
        optional: true,
        type: String
    },
    logo: {
        label: 'Logotipas',
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'Images',
                accept: 'image/*',
                label: "Įkelti nuotrauką"
            }
        }
    },
    address: {
        max: 100,
        label: 'Adresas',
        type: String
    },
    city: {
        max: 30,
        label: 'Miestas',
        type: String,
        regEx: /^([^0-9]*)$/
    },
    country: {
        type: Number,
        allowedValues: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28],
        defaultValue: 0,
        autoform: {
            options: [
                {label: "Anglija", value: 0},
                {label: "Baltarusija", value: 1},
                {label: "Bulgarija", value: 2},
                {label: "Čekija", value: 3},
                {label: "Danija", value: 4},
                {label: "Estija", value: 5},
                {label: "Graikija", value: 6},
                {label: "Gruzija", value: 7},
                {label: "Ispanija", value: 8},
                {label: "Italija", value: 9},
                {label: "Kroatija", value: 10},
                {label: "Latvija", value: 11},
                {label: "Lenkija", value: 12},
                {label: "Lietuva", value: 13},
                {label: "Norvegija", value: 14},
                {label: "Olandija", value: 15},
                {label: "Portugalija", value: 16},
                {label: "Prancūzija", value: 17},
                {label: "Rumunija", value: 18},
                {label: "Rusija", value: 19},
                {label: "Serbija", value: 20},
                {label: "Slovakija", value: 21},
                {label: "Slovėnija", value: 22},
                {label: "Suomija", value: 23},
                {label: "Švedija", value: 24},
                {label: "Turkija", value: 25},
                {label: "Ukraina", value: 26},
                {label: "Vengrija", value: 27},
                {label: "Vokietija", value: 28}
            ]
        }
    },
    website: {
        max: 100,
        label: 'Internetinis puslapis',
        type: String,
        optional: true,
        regEx: SimpleSchema.RegEx.Url
    },
    work_area: {
        label: 'Veiklos sritis',
        type: Number,
        allowedValues: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
        defaultValue: 1,
        autoform: {
            options: [
                {value: 1, label: "Administravimas/sekretoriavimas"},
                {value: 2, label:  "Apskaita/finansai/auditas"},
                {value: 3, label: "Dizainas/architektūra"},
                {value: 4, label: "Draudimas"},
                {value: 5, label: "Eksportas"},
                {value: 6, label: "Elektronika/telekomunikacijos"},
                {value: 7, label: "Energetika"},
                {value: 8, label: "Inžinerija/mechanika"},
                {value: 9, label: "Klientų aptarnavimas/paslaugos"},
                {value: 10, label: "Kompiuteriai/IT/internetas"},
                {value: 11, label: "Kultūra/kūryba"},
                {value: 12, label: "Logistika/transportas"},
                {value: 13, label: "Maisto gamyba"},
                {value: 14, label: "Marketingas/reklama"},
                {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
                {value: 16, label: "Nekilnojamasis turtas"},
                {value: 17, label: "Pardavimų vadyba"},
                {value: 18, label: "Personalo valdymas"},
                {value: 19, label: "Pirkimai/tiekimas"},
                {value: 20, label: "Pramonė/gamyba"},
                {value: 21, label: "Prekyba - konsultavimas"},
                {value: 22, label: "Sandėliavimas"},
                {value: 23, label: "Statyba"},
                {value: 24, label: "Švietimas/mokymai"},
                {value: 25, label: "Teisė"},
                {value: 26, label: "Turizmas/viešbučiai"},
                {value: 27, label: "Vadovavimas/valdymas"},
                {value: 28, label: "Valstybės tarnyba"},
                {value: 29, label: "Žemės ūkis/žuvininkystė"},
                {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
            ]
        }
    },
    short_description: {
        label: "Aprašymas",
        type: String,
        autoform: {
            afFormGroup : {
                'formgroup-class': 'col s12 m12'
            },
            afFieldInput: {
                type: "textarea",
                class: "materialize-textarea"
            }
        }
    },
    established_year: {
        label: 'Įkurta',
        type: Number,
        allowedValues: [2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980]
    },
    worker_count: {
        min: 1,
        label: 'Darbuotojų skaičius',
        type: Number,
        optional: true
    },
    pictures: {
        label: 'Nuotraukos',
        optional: true,
        type: [String]

    },
    "pictures.$": {
        autoform: {
            afFieldInput: {
                type: 'fileUpload',
                collection: 'Images',
                accept: 'image/*',
                label: "Įkelti nuotrauką"
            }
        }
    },
    weOffer: {
        label: " ",
        type: [Object],
        optional: true
    },
    "weOffer.$.name": {
        label: ' ',
        type: String,
        max: 50,
        autoform: {
            afFieldInput: {
                defaultValue: ""
            },
            class: "attribute"
        }
    }
}));


CompanyProfiles.attachBehaviour('timestampable');

if(Meteor.isServer) {
    CompanyProfiles.allow({
        insert: function (userId, doc) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            // can only change your own documents
            return doc.user_id === userId;
        },
        remove: function (userId, doc) {
            // can only remove your own documents
            return doc.userId === userId;
        }
    });

    Meteor.publish('specificCompanyProfile', function(id) {
        return CompanyProfiles.find({user_id: id});
    });
    Meteor.publish('companyProfileByCategory', function(id) {
        return CompanyProfiles.find({work_area: id});
    });
    Meteor.publish('myCompanyProfile', function() {
        return CompanyProfiles.find({user_id: this.userId}, {fields: {'createdAt': 0, 'createdBy': 0, 'updatedBy': 0, 'updatedAt': 0}});
    });
    Meteor.publish('companyProfileList', function(id) {
        return CompanyProfiles.find({}, {fields: {"work_area": 1, "company_name": 1, "user_id": 1}});
    });
}