var subs = new SubsManager();
exposed = FlowRouter.group({});
loggedIn = FlowRouter.group({
    triggersEnter: [
        function() {
            var route;
            //if (!(Meteor.loggingIn() || Meteor.userId())) {
            //    route = FlowRouter.current();
            //    if (route.route.name !== 'login') {
            //        Session.set('redirectAfterLogin', route.path);
            //    }
            //    Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            //    return FlowRouter.go('/login');
            //}
        }
    ]
});

//company = loggedIn.group({
//    triggersEnter: [
//        function() {
//            //FlowRouter.wait();
//            if (Meteor.user()['profile']['type'] != 2) {
//                //FlowRouter.initialize();
//                return FlowRouter.go(BlazeRouter.path('/'));
//            }
//        }
//    ]
//});
//
//candidate = loggedIn.group({
//    triggersEnter: [
//        function() {
//                if (Meteor.user()['profile']['type'] != 1) {
//                    //FlowRouter.initialize();
//                    return FlowRouter.go(FlowRouter.path('/'));
//                }
//        }
//    ]
//});
//
admin = loggedIn.group({
    triggersEnter: [
        function() {
            FlowRouter.wait();
            if (Meteor.user()['profile']['type'] != 0) {
                FlowRouter.initialize();
                return FlowRouter.go(FlowRouter.path('/'));
            }
        }
    ]
});


exposed.route('/', {
    action:function(params, queryParams) {
        console.log(queryParams);
        var controller = queryParams.controller;
        if(controller == 'y' || controller == 'n') {
            var token = queryParams.token;
            Meteor.call('checkRequestToken', token, controller);
        }
        BlazeLayout.render('defaultLayout', {content: 'home'});
    }
});

exposed.route('/signUp', {
    name: 'signUp',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'signup'});
    }
});

exposed.route('/login', {
    name: 'login',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'login'});
    }
});

loggedIn.route('/user/dashboard', {
    name: 'cvDashboard',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'cvDashboardLoader'});
    }
});

loggedIn.route('/company/dashboard', {
    name: 'cvDashboard',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'companyDashboardLoader'});
    }
});

loggedIn.route('/cv/add', {
    name: 'cvAdd',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'cvAdd'});
    }
});

loggedIn.route('/cv/edit/:cvID', {
    name: 'cvEdit',
    action: function() {
        BlazeLayout.render('defaultLayout', {content: 'cvEdit'});
    }
});

loggedIn.route('/cv/view/:cvID', {
    name: 'cvView',
    action: function() {
        BlazeLayout.render('defaultLayout', {content: 'cvViewLoader'});
    }
});

loggedIn.route('/company/add', {
    name: 'companyAdd',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'companyAdd'});
    }
});

loggedIn.route('/company/edit/:companyID', {
    name: 'companyEdit',
    action: function() {
        BlazeLayout.render('defaultLayout', {content: 'companyEdit'});
    }
});

loggedIn.route('/company/view/:companyID', {
    name: 'companyView',
    action: function() {
        BlazeLayout.render('defaultLayout', {content: 'companyView'});
    }
});

loggedIn.route('/company/index', {
    name: 'companyListIndex',
    action: function() {
        BlazeLayout.render('defaultLayout', {content: 'companyListIndex'});
    }
});

loggedIn.route('/company/:categoryId/index', {
    name: 'companyCategoryIndex',
    action: function() {
        BlazeLayout.render('defaultLayout', {content: 'companyCategoryIndex'});
    }
});

admin.route('/adminIndex', {
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'adminIndex'});
    }
});
loggedIn.route('/search', {
    name: 'search',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'searchLoader'});
    }
});
loggedIn.route('/bookmarks', {
    name: 'bookmarks',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'bookmarkIndex'});
    }
});
loggedIn.route('/requests', {
    name: 'requests',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'requestIndex'});
    }
});
loggedIn.route('/tickets', {
    name: 'tickets',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'ticketIndex'});
    }
});

loggedIn.route('/user/requests', {
    name: 'myRequests',
    action: function () {
        BlazeLayout.render('defaultLayout', {content: 'cvRequestIndex'});
    }
});