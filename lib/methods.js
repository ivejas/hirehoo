Meteor.methods({
    removeBookmark: function (company_id, user_id) {
        Bookmarks.remove({"company_id": company_id, "user_id": user_id});
    },
    addBookmark: function (company_id, user_id) {
        Bookmarks.insert({"company_id": company_id, "user_id": user_id});
    },

    removeRequest: function (company_id, user_id) {
        Requests.remove({"company_id": company_id, "user_id": user_id});
    },

    getTickets: function(count, company_id) {
        var company = Meteor.users.findOne({"_id": company_id});
        if(company != null) {
            for (x = 0; x < count; x++) {
                var date = new Date();
                var token = RandToken.generate(64);
                Tickets.insert({"owner": company_id, "token": token, "datetime_created": date, "used": 0});
            }
        }

    },

    requestInfo: function(company_id, user_id) {
        var company = Meteor.users.findOne({"_id": company_id});
        var user = Meteor.users.findOne({"_id": user_id});
        var token = RandToken.generate(64);
        var ticket = Tickets.findOne({'used_on': null});
        if(ticket != null) {
            Requests.insert({"company_id": company_id, "user_id": user_id, "token": token, "ticket_token": ticket.token, "status": 1});
            Tickets.update({"_id": ticket._id}, {$set: {"used": 1}});
            Email.send({
                to: user.emails[0].address,
                from: company.emails[0].address,
                subject: 'Įmonė nori gauti jūsų kontaktinius duomenis!',
                text: 'Įmonė paprašė jūsų kontaktinių duomenų. Įmonės profilis: ' + Meteor.absoluteUrl() + 'company/view/' + company['_id'] +' . Kad juos suteiktumete sekite šią nuorodą: ' + Meteor.absoluteUrl() + '?token=' + token + '&controller=y . ' +
                'Atsisakykite suteikti prieiga paspaude: ' + Meteor.absoluteUrl() + '?token=' + token + '&controller=n'
            });
        }
    },

    checkRequestToken: function(token, controller) {
        var request = Requests.findOne({"token": token});
        if(request == null || request.status != 1) {
            return 0;
        } else if(controller == 'y'){
            var ticketToken = Tickets.findOne({"token": request.ticket_token});
            Requests.update({"token": token}, {$set: {"status": 2}});
            Tickets.update({"_id": ticketToken._id}, {$set: {"datetime_used": new Date(), "used_on": request.user_id, "used": 2}});
            return 1
        } else if(controller == 'n') {
            var ticketToken = Tickets.findOne({"token": request.ticket_token});
            Requests.update({"token": token}, {$set: {"status": 0}});
            Tickets.update({"_id": ticketToken._id}, {$set: {"used": 0}});
            return 2
        } else {
            return 3
        }
    },

    saveSearch: function(search) {
        LastSearches.insert(search);
    },

    searchSuccess: function(profile) {
        var query = [];
        var tempArray = [];
        var tempArray2 = [];
        var temp = 0;
        for(x=0; x<profile.education.length; x++) {
            if(profile.education[x] != null) {
                if (profile.education[x].degree_type > temp) {
                    temp = profile.education[x].degree_type;
                }
            }
        }
        query.push({$or: [{'degree_type': {$exists: false}}, {'degree_type': {$lte: temp}}]});
        temp = 0;
        //query.push({'work_areas': profile.work_areas});
        tempArray = [];
        for(x=0; x<profile.work_areas.length; x++) {
            if(profile.work_areas[x] != null) {
                tempArray.push({"work_areas.area": profile.work_areas[x].area});
            }
        }
        query.push({$or: tempArray});
        for(x = 0; x < profile.requirements.length; x++) {
            if(profile.requirements[x] != null) {
                tempArray.push({
                    'city': {
                        '$regex': '^.*' + profile.requirements[x].city + ".*\\b",
                        "$options": "i"
                    }
                });
                tempArray.push({'salary_min': {$lte: profile.requirements[x].salary_from}});
                tempArray.push({$or: [{'salary_max': {$exists: false}}, {'salary_max': {$gte: profile.requirements[x].salary_from}}]});

                tempArray2.push({$and: tempArray});
                tempArray = [];
            }
        }
        query.push({$or: tempArray2});
        tempArray2 = [];
        tempArray = [];

        for(x=0; x<profile.languages.length; x++) {
            if(profile.languages[x] != null) {
                tempArray.push({'languages.language': profile.languages[x].language});
                tempArray.push({$or: [{'languages.speaking': {$lte: profile.languages[x].speaking}}, {'languages.speaking': {$exists: false}}]});
                tempArray.push({$or: [{'languages.reading': {$lte: profile.languages[x].speaking}}, {'languages.reading': {$exists: false}}]});
                tempArray.push({$or: [{'languages.writing': {$lte: profile.languages[x].speaking}}, {'languages.writing': {$exists: false}}]});
                tempArray2.push({$and: tempArray});
                tempArray = [];
            }
        }
        query.push({$or: tempArray2});
        tempArray = [];

        for(x=0; x<profile.skills.length; x++) {
            if(profile.skills[x] != null) {
                tempArray.push({
                    'skills.name': {'$regex': '^.*' + profile.skills[x].name + ".*\\b", "$options": "i"},
                    'skills.experience': {$lte: profile.skills[x].experience}
                });
            }
        }
        query.push({$or: tempArray});
        var result = LastSearches.find({$and: query});
        return result.count();
    },

    logSearchSuccess: function(user_id) {
        var user = SearchSuccesses.findOne({"user_id": user_id});
        if(user != null) {
            SearchSuccesses.update({"user_id": user_id}, {$set: {"count": user.count + 1}});
        } else {
            SearchSuccesses.insert({"user_id": user_id, "count": 1});
        }
        return true;
    },

    getPopSkills: function(profile) {
        var workArea = profile.work_area;
        var sortable = [];
        var tempArray = [];
        for(var x=0;x<profile.work_areas.length; x++) {
            tempArray.push({"work_areas.area": profile.work_areas[x].area});
        }
        var searches = LastSearches.find({$or: tempArray}).fetch();
        var userSkills = [];
        for (x = 0; x < profile.skills.length; x++) {
            userSkills.push(profile.skills[x].name);
        }
        var skillsArray = [];
        for (x = 0; x < searches.length; x++) {
            if (searches[x].skills != null) {
                for (y = 0; y < searches[x].skills.length; y++) {
                    skillsArray.push(searches[x].skills[y].name)
                }
            }
        }
        skillsArray = skillsArray.filter(function (el) {
            return userSkills.indexOf(el) < 0;
        });

        var counts = {};
        for (var i = 0; i < skillsArray.length; i++) {
            counts[skillsArray[i]] = 1 + (counts[skillsArray[i]] || 0);
        }
        for (var skillName in counts) {
            sortable.push([skillName, counts[skillName]])
        }
        sortable.sort(function(a,b) {return b[1] - a[1]});
        return sortable;
    },

    listCitySuggestions: function(request, city) {
        request[0] = {'requirements.city': {$not: new RegExp(city, 'i')}};
        var searchResult = Profiles.find({$and: request}).fetch();
        for(x=0; x<searchResult.length; x++) {
            if(SuggestedCities.findOne({"user_id": searchResult[x].user_id, 'city': new RegExp(city, 'i')}) == null) {
                SuggestedCities.insert({"user_id": searchResult[x].user_id, "city": city});
            }
        }
    },

    getCitySuggestions: function(profile){
        return SuggestedCities.find({"user_id": profile.user_id}).fetch();

    },

    addView: function(company, user) {
        //if(Views.findOne({"user_id": user, "company_id": company}) == null) {
            Views.insert({"user_id": user, "company_id": company});
        //}
        return true;
    },

    addSkill: function(user_id, skill, experience) {
        Profiles.update({'user_id': user_id}, {$push: {'skills': {'name': skill, 'experience': experience}}});
    },
    addCity: function(user_id, city, salary_from) {
        Profiles.update({'user_id': user_id}, {$push: {'requirements': {'country': 13, 'city': city, 'salary_from': salary_from}}});
        SuggestedCities.remove({'user_id': user_id, 'city': city});
    },

    search: function(query) {
        console.log(Profiles.find({$and: query}, {fields: {first_name: 0, last_name: 0, email: 0}}).fetch());
        console.log(query);

        return Profiles.find({$and: query}, {fields: {first_name: 0, last_name: 0, email: 0}}).fetch();
    },

    profilePreview: function(id) {
        return Profiles.findOne({"user_id": id});
    },

    userCreate: function(username, password, type) {
        Accounts.createUser({email: username, password: password, profile: {type: type}}, function(data) {
            console.log(data);
        })
    }
});
