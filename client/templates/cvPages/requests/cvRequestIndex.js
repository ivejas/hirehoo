Template.cvRequestIndex.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 1) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }

    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('userRequests');
        var handle2 = PostSubs.subscribe('companyProfileList');
        self.ready.set(handle.ready() && handle2.ready());
    });
    Session.set('currentPage', 'requests');
});

Template.cvRequestIndex.helpers({
    requests: function() {
        return Requests.find();
    },

    company_name: function(id) {
        return CompanyProfiles.findOne({"user_id": id})['company_name'];
    },

    date: function(date) {
        var date = new Date(date);
        var month = date.getMonth() + 1;
        if(month == 13) {
            month = '01'
        } else if(month < 10) {
            month = '0'+month;
        }
        return date.getUTCFullYear() + '-' + month + '-' + date.getUTCDate();
    },
    isAccepted: function(status) {
        if(status == 2) {
            return true;
        }
        return false;
    },
    isRejected: function(status) {
        if(status == 0) {
            return true;
        }
        return false;
    }

});