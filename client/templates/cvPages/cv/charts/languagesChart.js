Template.languagesChart.onCreated(function() {

});

Template.languagesChart.onRendered(function() {
    var languages = Session.get('languages');
    var labels = [];
    var series = [];
    var data = {};
    data['series'] = [];
    labels.push(getLanguage(languages[this.data.index].language));
    switch(languages[this.data.index].speaking) {
        case 0:
            data['series'].push([{meta: "Kalbėjimas: blogai", value: 5}]);
            break;
        case 1:
            data['series'].push([{meta: "Kalbėjimas: blogiau nei vidutiniškai", value:10}]);
            break;
        case 2:
            data['series'].push([{meta: "Kalbėjimas: vidutiniškai", value:15}]);
            break;
        case 3:
            data['series'].push([{meta: "Kalbėjimas: geriau nei vidutiniškai", value:20}]);
            break;
        case 4:
            data['series'].push([{meta: "Kalbėjimas: gerai", value:25}]);
            break;
    }
    switch(languages[this.data.index].listening) {
        case 0:
            data['series'].push([{meta: "Klausymas: blogai", value: 5}]);
            break;
        case 1:
            data['series'].push([{meta: "Klausymas: blogiau nei vidutiniškai", value:10}]);
            break;
        case 2:
            data['series'].push([{meta: "Klausymas: vidutiniškai", value:15}]);
            break;
        case 3:
            data['series'].push([{meta: "Klausymas: geriau nei vidutiniškai", value:20}]);
            break;
        case 4:
            data['series'].push([{meta: "Klausymas: gerai", value:25}]);
            break;
    }
    switch(languages[this.data.index].reading) {
        case 0:
            data['series'].push([{meta: "Skaitymas: blogai", value: 5}]);
            break;
        case 1:
            data['series'].push([{meta: "Skaitymas: blogiau nei vidutiniškai", value:10}]);
            break;
        case 2:
            data['series'].push([{meta: "Skaitymas: vidutiniškai", value:15}]);
            break;
        case 3:
            data['series'].push([{meta: "Skaitymas: geriau nei vidutiniškai", value:20}]);
            break;
        case 4:
            data['series'].push([{meta: "Skaitymas: gerai", value:25}]);
            break;
    }
    switch(languages[this.data.index].writing) {
        case 0:
            data['series'].push([{meta: "Rašymas: blogai", value: 5}]);
            break;
        case 1:
            data['series'].push([{meta: "Rašymas: blogiau nei vidutiniškai", value:10}]);
            break;
        case 2:
            data['series'].push([{meta: "Rašymas: vidutiniškai", value:15}]);
            break;
        case 3:
            data['series'].push([{meta: "Rašymas: geriau nei vidutiniškai", value:20}]);
            break;
        case 4:
            data['series'].push([{meta: "Rašymas: gerai", value:25}]);
            break;
    }

    
    data['labels'] = labels;
    console.log(data);
    var hi = [
        [{meta: 'hi', value:1000000}, 1200000, 1400000, 1300000],
        [200000, 400000, 500000, 300000],
        [100000, 200000, 400000, 600000]
    ];
    console.log(hi);

    var options = {
        high: 100,
        low: 0,
        seriesBarDistance: 12,
        chartPadding: 30,
        labelOffset: 50,
        labelDirection: 'explode',
        height: 35,
        stackBars: true,
        horizontalBars: true,
        plugins: [
            Chartist.plugins.tooltip({
            })
        ]
    };

    var responsiveOptions = [
        ['screen and (min-width: 640px)', {
            labelOffset: 100,
            labelDirection: 'explode',
            labelInterpolationFnc: function(value) {
                return value;
            }
        }],
        ['screen and (min-width: 1024px)', {
            labelOffset: 45
        }]
    ];

    chart = new Chartist.Bar('#languages-bar'+this.data.index, data, options, responsiveOptions).on('draw', function(data) {
        if (data.type === 'bar') {
            data.element.attr({
                style: 'stroke-width: 30px; stroke:'+pickRandom(backgrounds)+''
            });
        }
    });
});

Template.languagesChart.helpers({
});

var pickRandom = function (arr) {
    return arr[Math.floor(Math.random() * arr.length)];
};

var getLanguage = function(index) {
    for(var x=0; x<languages.length; x++) {
        if(languages[x].value == index ) {
            return languages[x].label;
            break;
        }
    }
};

var backgrounds = ['#1abc9c', '#2980b9',
    '#f1c40f', '#27ae60', '#7f8c8d' ];

var languages = [
    {label: "Anglų", value: 0},
    {label: "Arabų", value: 1},
    {label: "Armėnų", value: 2},
    {label: "Azerbaidžaniečių", value: 3},
    {label: "Baltarusių", value: 4},
    {label: "Bulgarų", value: 5},
    {label: "Čekų", value: 6},
    {label: "Danų", value: 7},
    {label: "Esperanto", value: 8},
    {label: "Estų", value: 9},
    {label: "Filipinų", value: 10},
    {label: "Graikų", value: 11},
    {label: "Gruzinų", value: 12},
    {label: "Hebrajų", value: 13},
    {label: "Hindi", value: 14},
    {label: "Ispanų", value: 15},
    {label: "Italų", value: 16},
    {label: "Japonų", value: 17},
    {label: "Kinų", value: 18},
    {label: "Korėjiečių", value: 19},
    {label: "Kroatų", value: 20},
    {label: "Latvių", value: 21},
    {label: "Lenkų", value: 22},
    {label: "Lietuvių", value: 23},
    {label: "Lotynų", value: 24},
    {label: "Norvegų", value: 25},
    {label: "Olandų", value: 26},
    {label: "Portugalų", value: 27},
    {label: "Prancūzų", value: 28},
    {label: "Rumunų", value: 29},
    {label: "Rusų", value: 30},
    {label: "Serbų", value: 31},
    {label: "Slovakų", value: 32},
    {label: "Slovėnų", value: 33},
    {label: "Suomių", value: 34},
    {label: "Švedų", value: 35},
    {label: "Turkų", value: 36},
    {label: "Ukrainiečių", value: 37},
    {label: "Vengrų", value: 38},
    {label: "Vokiečių", value: 39}
];