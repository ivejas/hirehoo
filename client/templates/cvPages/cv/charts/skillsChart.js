Template.skillsChart.onCreated(function() {

});

Template.skillsChart.onRendered(function() {
    console.log(this.data.index);
    var skills = Session.get('skills');
    var labels = [];
    var series = [];

    labels.push(skills[this.data.index].name);
    switch(skills[this.data.index].experience) {
        case 0:
            series.push({meta: skills[this.data.index].name+": pagrindai", value: 20});
            break;
        case 1:
            series.push({meta: skills[this.data.index].name+": iki 1m.", value:40});
            break;
        case 2:
            series.push({meta: skills[this.data.index].name+": 1-2m.", value:60});
            break;
        case 3:
            series.push({meta: skills[this.data.index].name+": 3-4m.", value:80});
            break;
        case 4:
            series.push({meta: skills[this.data.index].name+": 4m. ir daugiau", value:100});
            break;
    }

    var data = {};
    data['labels'] = labels;
    data['series'] = [series];
    console.log(data);

    var options = {
        high: 100,
        low: 0,
        seriesBarDistance: 12,
        chartPadding: 30,
        labelOffset: 50,
        labelDirection: 'explode',
        height: 35,
        stackBars: true,
        horizontalBars: true,
        plugins: [
            Chartist.plugins.tooltip({
            })
        ]
    };

    var responsiveOptions = [
        ['screen and (min-width: 640px)', {
            labelOffset: 100,
            labelDirection: 'explode',
            labelInterpolationFnc: function(value) {
                return value;
            }
        }],
        ['screen and (min-width: 1024px)', {
            labelOffset: 45
        }]
    ];

    chart = new Chartist.Bar('#skills-bar'+this.data.index, data, options, responsiveOptions).on('draw', function(data) {
        if (data.type === 'bar') {
            data.element.attr({
                style: 'stroke-width: 30px; stroke:'+pickRandom(backgrounds)+''
            });
        }
    });

    var $chart = $('#skills-bar'+this.data.index);

    var $toolTip = $chart
        .append('<div class="tooltip"></div>')
        .find('.tooltip')
        .hide();

    $chart.on('mouseenter', '.ct-bar', function() {
        var $point = $(this),
            value = $point.attr('ct:value'),
            seriesName = $point.attr('ct:meta');
        console.log($point.attr('ct:value'));
        console.log($point.attr('ct:meta'));
        $toolTip.html(seriesName + '<br>' + value).show();
    });

    $chart.on('mouseleave', '.ct-bar', function() {
        $toolTip.hide();
    });

    $chart.on('mousemove', function(event) {
        $toolTip.css({
            left: (event.offsetX || event.originalEvent.layerX) - $toolTip.width() / 2 - 10,
            top: (event.offsetY || event.originalEvent.layerY) - $toolTip.height() - 40
        });
    });
});

Template.skillsChart.helpers({
});

var pickRandom = function (arr) {
    return arr[Math.floor(Math.random() * arr.length)];
};

var backgrounds = ['#1abc9c', '#2980b9',
    '#f1c40f', '#27ae60', '#7f8c8d' ];