Template.cvAdd.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 1) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    Session.set('currentPage', 'cvAdd');
    Session.set('reqCount', 1);
    Session.set('pastWorkCount', 0);
    Session.set('educationCount', 1);
    Session.set('languageCount', 1);
    Session.set('skillCount', 0);
    Session.set('recCount', 0);
    Session.set('areaCount', 1);
    if(Meteor.user() !== undefined) {
        if (Meteor.user()['profile']['type'] != 1) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    if(Profiles.findOne({"user_id": Meteor.userId()}) != null) {
        FlowRouter.go('/cv/edit/'+Meteor.userId());
    }
    Session.set('skills', null);
    Session.set('skillSearch', null);

    Session.set('weOffer', null);
    Session.set('weOfferSearch', null);

    FS.debug = true;
    this.subscribe = Meteor.subscribe('skills');
    this.subscribe = Meteor.subscribe('weOffer');
    this.subscribe = Meteor.subscribe('images');
});
Template.cvAdd.onRendered(function() {
    AutoForm.debug();
    $('input#input_text, textarea#short_description').characterCounter();
    $('select').parent().find('.caret').remove();
    $('select').material_select();
    $(document).ready(function(){
        $('.button-collapse').sideNav();
        $('.parallax').parallax();
    });
    $(document).ready(function(){
        $('.collapsible').collapsible({
            accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });

    $(document).ready(function() {
        $('.scrollspy').scrollSpy();
    });
});

Template.cvAdd.helpers({
    skill_search: function() {
        if(Session.get('skillSearch') != '' && Session.get('skillSearch') != null) {
            var workAreaArray = [];
            var results = [];
            $('.work_areas select').each(function(index) {
                workAreaArray.push($(this).val());
            });
            var search = Session.get('skillSearch').toLowerCase();
            for(x=0; x<workAreaArray.length; x++) {
                results.push(Skills.find({'work_area': Number(workAreaArray[x]), 'name': new RegExp(RegExp.escape(search), 'i')}).fetch());
            }
            var emptyResult = true;
            for(x=0; x<workAreaArray.length; x++) {
                if(results[x].length != 0) emptyResult = false;
            }
            if(emptyResult == true) {
                results = [];
                search = {name: search, work_area: Number(workAreaArray[0])};
                for(x=0; x<workAreaArray.length; x++) {
                    results.push(Skills.find({'work_area': Number(workAreaArray[x])}).fetch());
                }
                results.unshift(search);
            }
            var finalArray = [];
            for(x=0; x<results.length; x++){
                for(y=0; y<results[x].length; y++){
                    finalArray.push(results[x][y].name.toLowerCase());
                }
            }
            results = [];
            results.unshift(search);
            $.each(finalArray, function(i, el){
                if($.inArray(el, results) === -1) results.push(el);
            });
            console.log(results);
            return results;
        } else {
            return false;
        }
    },

    weOffer_search: function() {
        if(Session.get('weOfferSearch') != '' && Session.get('weOfferSearch') != null) {
            var search = Session.get('weOfferSearch');
            var results = WeOffer.find({'work_area': Number($("#work_area").val()), 'name': {'$regex': '^.*'+Session.get('weOfferSearch') + ".*\\b", "$options": "i"}});
            if(results.fetch().length == 0) {
                search = {name: search, work_area: Number($("#work_area").val())};
                results = WeOffer.find({'work_area': Number($("#work_area").val())}).fetch();
                results.unshift(search);
                return results
            }
            return results;
        } else {
            return false;
        }
    },
    showImage: function() {
        if(Session.get('picture') != null) {
            var imageId = Session.get('picture');
            return Images.findOne({"_id": imageId}).url()
        } else {
            return false;
        }
    },

    categories: function() {
        return [
            {value: 1, label: "Administravimas/sekretoriavimas"},
            {value: 2, label:  "Apskaita/finansai/auditas"},
            {value: 3, label: "Dizainas/architektūra"},
            {value: 4, label: "Draudimas"},
            {value: 5, label: "Eksportas"},
            {value: 6, label: "Elektronika/telekomunikacijos"},
            {value: 7, label: "Energetika"},
            {value: 8, label: "Inžinerija/mechanika"},
            {value: 9, label: "Klientų aptarnavimas/paslaugos"},
            {value: 10, label: "Kompiuteriai/IT/internetas"},
            {value: 11, label: "Kultūra/kūryba"},
            {value: 12, label: "Logistika/transportas"},
            {value: 13, label: "Maisto gamyba"},
            {value: 14, label: "Marketingas/reklama"},
            {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
            {value: 16, label: "Nekilnojamasis turtas"},
            {value: 17, label: "Pardavimų vadyba"},
            {value: 18, label: "Personalo valdymas"},
            {value: 19, label: "Pirkimai/tiekimas"},
            {value: 20, label: "Pramonė/gamyba"},
            {value: 21, label: "Prekyba - konsultavimas"},
            {value: 22, label: "Sandėliavimas"},
            {value: 23, label: "Statyba"},
            {value: 24, label: "Švietimas/mokymai"},
            {value: 25, label: "Teisė"},
            {value: 26, label: "Turizmas/viešbučiai"},
            {value: 27, label: "Vadovavimas/valdymas"},
            {value: 28, label: "Valstybės tarnyba"},
            {value: 29, label: "Žemės ūkis/žuvininkystė"},
            {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
        ]
    },

    reqCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('reqCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    pastWorkCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('pastWorkCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    educationCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('educationCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    languageCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('languageCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    skillCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('skillCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    recCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('recCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    areaCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('areaCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    subsReady: function() {
        return FlowRouter.subsReady();
    }
});

Template.cvAdd.events({
    //"documentSubmit": function(event,tmpl,doc) {
    //    doc['user_id'] = Meteor.userId();
    //    event.preventDefault();
    //    var tempArray = [];
    //    var tempObject = {};
    //    var name = null;
    //    var index = 9999;
    //    var tempIndex = null;
    //    for(var key in doc) {
    //        console.log(key);
    //        var split = key.split('.');
    //        if(split.length > 1) {
    //            if(split[1] != index && index != 9999) {
    //                alert('yo');
    //                tempArray.push(tempObject);
    //                tempObject = {};
    //                index = split[1];
    //                name = split[0];
    //                tempObject[split[2]] = doc[key];
    //                delete doc[key];
    //            } else {
    //                alert('hi');
    //                name = split[0];
    //                index = split[1];
    //                tempObject[split[2]] = doc[key];
    //                delete doc[key];
    //            }
    //        } else if(tempArray.length != 0) {
    //            tempArray.push(tempObject);
    //            tempObject = {};
    //            doc[name] = tempArray;
    //            tempArray = [];
    //
    //        }
    //    }
    //    console.log(tempArray);
    //    console.log(doc);
    //},
    //"submit #profileInsertForm": function(event) {
    //    event.preventDefault();
        //var form = Forms.instance();
        //console.log(form);
        //var x = $("#profileInsertForm").serializeArray();
        //var insertObject = {};
        //var temp = 9999;
        //var tempArray = [];
        //var tempName = "";
        //var object = {};
        //$.each(x, function(i, field){
        //    if(field.value != "") {
        //        var split_name = field.name.split(".");
        //        if (split_name.length > 1) {
        //            if(split_name[1] != temp && temp != 9999) {
        //                alert('zdarow');
        //                tempArray.push(object);
        //                insertObject[tempName] = tempArray;
        //                tempArray = [];
        //                object = {};
        //                tempName = split_name[0];
        //                temp = split_name[1];
        //                var name = split_name[2];
        //
        //                object[name] = field.value;
        //
        //            } else {
        //                alert('hallo');
        //                var name = split_name[2];
        //                object[name] = field.value;
        //                temp = split_name[1];
        //                tempName = split_name[0];
        //            }
        //            alert(tempName);
        //        }else {
        //            if(temp != 9999) {
        //                alert('yo');
        //                tempArray.push(object);
        //                insertObject[tempName] = tempArray;
        //                object = {};
        //                tempArray = [];
        //                tempName = 0;
        //                temp = 9999;
        //            }
        //            var name = field.name;
        //            //var object = {};
        //            insertObject[name] = field.value;
        //            //object[name] = field.value;
        //            //insertArray.push(object);
        //        }
        //    }
            //console.log(field.name + ":" + field.value + " ");
            //tempArray.push(field.serialize());
        //});
        //if(temp != 9999) {
        //    tempArray.push(object);
        //    console.log(object);
        //    alert('yo');
        //    insertObject[tempName] = tempArray;
        //    tempArray = [];
        //    object = {};
        //    tempName = 0;
        //    temp = 9999;
        //}

        //console.log(x);
    //},
    "click #addArea": function(event) {
        var count = Session.get('areaCount') + 1;
        Session.set('areaCount', count);
    },
    "click #addRequirement": function(event) {
        var count = Session.get('reqCount') + 1;
        Session.set('reqCount', count);
    },
    "click .addPastWork": function(event) {
        var count = Session.get('pastWorkCount') + 1;
        Session.set('pastWorkCount', count);
    },
    "click .addEducation": function(event) {
        var count = Session.get('educationCount') + 1;
        Session.set('educationCount', count);
    },
    "click .addLanguage": function(event) {
        var count = Session.get('languageCount') + 1;
        Session.set('languageCount', count);
    },
    "click .addSkill": function(event) {
        var count = Session.get('skillCount') + 1;
        Session.set('skillCount', count);
    },
    "click .addRec": function(event) {
        var count = Session.get('recCount') + 1;
        Session.set('recCount', count);
    },
    "click .attribute": function(event) {
        Session.set('target', event.currentTarget.name);
        $('#skillModal').openModal();
        $('#skill_search').val('');
        $("#skill_search").focus();
        $("#skill_search").typing({
            stop: function(event, $elem) {
                Session.set("skillSearch", $("#skill_search").val());
            },
            delay: 400
        });
    },
    "click .attribute-button": function(event) {
        console.log(event.currentTarget.value);
        $('input[name="'+Session.get('target')+'"]').val(event.currentTarget.value);
        $('#skillModal').closeModal();
        $('input[name="'+Session.get('target')+'"]').focus();
    },
    "click .weOffer": function(event) {
        Session.set('target', event.currentTarget.name);
        $('#weOfferModal').openModal();
        $("#weOffer_search").focus();
        $("#weOffer_search").typing({
            stop: function(event, $elem) {
                Session.set("weOfferSearch", $("#weOffer_search").val());
            },
            delay: 400
        });
    },
    "click .weOffer-button": function(event) {
        $('input[name="'+Session.get('target')+'"]').val(event.currentTarget.value);
        $('#weOfferModal').closeModal();
    },
    'change .myFileInput': function(event, template) {
        FS.Utility.eachFile(event, function(file) {
            Images.insert(file, function (err, fileObj) {
                if (err){
                    // handle error
                } else {
                    // handle success depending what you need to do
                //    var userId = Meteor.userId();
                //    var imagesURL = {
                //  'profile.image': '/cfs/files/images/' + fileObj._id
                //};
                //Meteor.users.update(userId, {$set: imagesURL});
                    Session.set('picture', fileObj._id);
            }
        });
    });
    }
});

AutoForm.hooks({
    'insertProfileForm': {
        formToDoc: function(doc) {
            if(doc.education != null) {
                var i = doc.education.length;
                while (i--) {
                    if (doc.education[i] == null) {
                        doc.education.splice(i, 1);
                    }
                }
            }
            if(doc.requirements != null) {
                i = doc.requirements.length;
                while (i--) {
                    if (doc.requirements[i] == null) {
                        doc.requirements.splice(i, 1);
                    }
                }
            }
            if(doc.past_work != null) {
                i = doc.past_work.length;
                while (i--) {
                    if (doc.past_work[i] == null) {
                        doc.past_work.splice(i, 1);
                    }
                }
            }
            if(doc.languages != null) {
                i = doc.languages.length;
                while (i--) {
                    if (doc.languages[i] == null) {
                        doc.languages.splice(i, 1);
                    }
                }
            }
            if(doc.skills != null) {
                i = doc.skills.length;
                while (i--) {
                    if (doc.skills[i] == null) {
                        doc.skills.splice(i, 1);
                    }
                }
            }
            if(doc.recommendations != null) {
                i = doc.recommendations.length;
                while (i--) {
                    if (doc.recommendations[i] == null) {
                        doc.recommendations.splice(i, 1);
                    }
                }
            }
            doc.user_id = Meteor.userId();
            doc.email = Meteor.user().emails[0].address;
            Session.set('work_areas',doc.work_areas);
            Session.set('weOffer', doc.weOffer);
            Session.set('skills', doc.skills);
            console.log(doc);
            return doc;
        },
        onSuccess: function(formType, result) {
            var skills = Session.get('skills');
            var workAreas = Session.get('work_areas');
            for(x=0; x<skills.length; x++) {
                for(y=0; y<workAreas.length; y++) {
                    if (Skills.findOne({name: RegExp.escape(skills[x].name), work_area: workAreas[y].area}) == null) {
                        Skills.insert({name: skills[x].name, "work_area": workAreas[y].area});
                    }
                }
            }
            FlowRouter.go('/user/dashboard');
        }
    }
});