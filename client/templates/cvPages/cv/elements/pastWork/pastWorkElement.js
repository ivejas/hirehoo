Template.pastWorkElement.onCreated(function() {

});

Template.pastWorkElement.onRendered(function() {
    $('select').parent().find('.caret').remove();
    $('select').material_select();
    $('.pastWorkDesc').parent().find('.character-counter').remove();
    $('input#input_text, textarea.pastWorkDesc').characterCounter();
});

Template.pastWorkElement.helpers({
    getCompanyNameValue: function(post, count) {
        return post[count].workplace_name;
    },
    getWorkplaceWebsiteValue: function(post, count) {
        return post[count].workplace_website;
    },
    getWorkTitleValue: function(post, count) {
        return post[count].work_title;
    },
    getStartTimeYearValue: function(post, count) {
        return post[count].start_time_year;
    },
    getStartTimeMonthValue: function(post, count) {
        return post[count].start_time_month;
    },
    getEndTimeYearValue: function(post, count) {
        return post[count].end_time_year;
    },
    getEndTimeMonthValue: function(post, count) {
        return post[count].end_time_month;
    },
    years: function() {
        return [2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980,
            1979,1978,1977,1976,1975,1974,1973,1972,1971,1970,1969,1968,1967,1966,1965,1964,1963,1962,1961,1960];
    },
    months: function() {
        return [1,2,3,4,5,6,7,8,9,10,11,12];
    },
    checkDate: function(source, date) {
        if(source == date) {
            return true;
        }
        return false;
    },
    categories: function() {
        return [
            {value:"0", label: "Pasirinkite"},
            {value:"1", label: "Administravimas/sekretoriavimas"},
            {value:"2", label: "Apskaita/finansai/auditas"},
            {value:"3", label: "Dizainas/architektūra"},
            {value:"4", label: "Draudimas"},
            {value:"5", label: "Eksportas"},
            {value:"6", label: "Elektronika/telekomunikacijos"},
            {value:"7", label: "Energetika"},
            {value:"8", label: "Inžinerija/mechanika"},
            {value:"9", label: "Klientų aptarnavimas/paslaugos"},
            {value:"10", label: "Kompiuteriai/IT/internetas"},
            {value:"11", label: "Kultūra/kūryba"},
            {value:"12", label: "Logistika/transportas"},
            {value:"13", label: "Maisto gamyba"},
            {value:"14", label: "Marketingas/reklama"},
            {value:"15", label: "Medicina/sveikatos apsauga/farmacija"},
            {value:"16", label: "Nekilnojamasis turtas"},
            {value:"17", label: "Pardavimų vadyba"},
            {value:"18", label: "Personalo valdymas"},
            {value:"19", label: "Pirkimai/tiekimas"},
            {value:"20", label: "Pramonė/gamyba"},
            {value:"21", label: "Prekyba - konsultavimas"},
            {value:"22", label: "Sandėliavimas"},
            {value:"23", label: "Statyba"},
            {value:"24", label: "Švietimas/mokymai"},
            {value:"24", label: "Teisė"},
            {value:"25", label: "Turizmas/viešbučiai"},
            {value:"26", label: "Vadovavimas/valdymas"},
            {value:"27", label: "Valstybės tarnyba"},
            {value:"28", label: "Žemės ūkis/žuvininkystė"},
            {value:"29", label: "Žiniasklaida/viešieji ryšiai"}
        ]
    },
    getWorkArea: function (count) {
        return 'past_work.' + count + '.work_area'
    },
    getCompanyName: function (count) {
        return 'past_work.' + count + '.workplace_name'
    },
    getWorkplaceWebsite: function (count) {
        return 'past_work.' + count + '.workplace_website'
    },
    getWorkTitle: function (count) {
        return 'past_work.' + count + '.work_title'
    },
    getStartTimeYear: function (count) {
        return 'past_work.' + count + '.start_time_year'
    },
    getEndTimeYear: function (count) {
        return 'past_work.' + count + '.end_time_year'
    },
    getEndTimeMonth: function (count) {
        return 'past_work.' + count + '.end_time_month'
    },
    getStartTimeMonth: function (count) {
        return 'past_work.' + count + '.start_time_month'
    },
    getShortDescription: function (count) {
        return 'past_work.' + count + '.short_description'
    }
});

Template.pastWorkElement.events({
    "click .removePastWork": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('pastWorkCount') - 1;
        $("#pastWork"+reqNumber).remove();
    },
    "change .endTimeYear select": function(event) {
        var id = event.currentTarget.id.split('.');
        var nameStart = id[0]+'\\.'+id[1];
        if(Number($("#"+nameStart+"\\.end_time_year").val()) > 0) {
            var startTime = $("#" + nameStart + '\\.start_time_year').val() * 12 + $("#" + nameStart + '\\.start_time_month').val();
            var endTime = $("#" + nameStart + '\\.end_time_year').val() * 12 + $("#" + nameStart + '\\.end_time_month').val();
            if ((startTime - endTime) > 1) {
                if($("#error\\."+id[1]).length == false) {
                    $("#" + nameStart + "\\.error").append("<span id='error." + id[1] + "'>Baigimo data negali būti ankčiau nei pradžios data</span>");
                }
            } else {
                if($("#error\\."+id[1]).length != false) {
                    $("#error\\."+id[1]).remove();
                }
            }
        } else {
            if($("#error\\."+id[1]).length != false) {
                $("#error\\."+id[1]).remove();
            }
        }
        var query = '#pastWork' + id[1] + ' .input-field .endTimeMonth input';
        if (event.currentTarget.value == 0) {
            $(query).attr('disabled', true);
        } else {
            $(query).attr('disabled', false);
        }
    },
    "change .endTimeMonth select": function(event) {
        var id = event.currentTarget.id.split('.');
        var nameStart = id[0]+'\\.'+id[1];
        if(Number($("#"+nameStart+"\\.end_time_year").val()) > 0) {
            var startTime = $("#" + nameStart + '\\.start_time_year').val() * 12 + $("#" + nameStart + '\\.start_time_month').val();
            var endTime = $("#" + nameStart + '\\.end_time_year').val() * 12 + $("#" + nameStart + '\\.end_time_month').val();
            if ((startTime - endTime) >= 0) {
                if($("#error\\."+id[1]).length == false) {
                    $("#" + nameStart + "\\.error").append("<span id='error." + id[1] + "'>Baigimo data negali būti ankčiau nei pradžios data</span>");
                }
            } else {
                if($("#error\\."+id[1]).length != false) {
                    $("#error\\."+id[1]).remove();
                }
            }
        } else {
            if($("#error\\."+id[1]).length != false) {
                $("#error\\."+id[1]).remove();
            }
        }
    },
    "change .startTimeYear select": function(event) {
        var id = event.currentTarget.id.split('.');
        var nameStart = id[0]+'\\.'+id[1];
        if(Number($("#"+nameStart+"\\.end_time_year").val()) > 0) {
            var startTime = $("#" + nameStart + '\\.start_time_year').val() * 12 + $("#" + nameStart + '\\.start_time_month').val();
            var endTime = $("#" + nameStart + '\\.end_time_year').val() * 12 + $("#" + nameStart + '\\.end_time_month').val();
            if ((startTime - endTime) >= 0) {
                if($("#error\\."+id[1]).length == false) {
                    $("#" + nameStart + "\\.error").append("<span id='error." + id[1] + "'>Baigimo data negali būti ankčiau nei pradžios data</span>");
                }
            } else {
                if($("#error\\."+id[1]).length != false) {
                    $("#error\\."+id[1]).remove();
                }
            }
        } else {
            if($("#error\\."+id[1]).length != false) {
                $("#error\\."+id[1]).remove();
            }
        }
    },
    "change .startTimeMonth select": function(event) {
        var id = event.currentTarget.id.split('.');
        var nameStart = id[0]+'\\.'+id[1];
        if(Number($("#"+nameStart+"\\.end_time_year").val()) > 0) {
            var startTime = $("#" + nameStart + '\\.start_time_year').val() * 12 + $("#" + nameStart + '\\.start_time_month').val();
            var endTime = $("#" + nameStart + '\\.end_time_year').val() * 12 + $("#" + nameStart + '\\.end_time_month').val();
            if ((startTime - endTime) >= 0) {
                if($("#error\\."+id[1]).length == false) {
                    $("#" + nameStart + "\\.error").append("<span id='error." + id[1] + "'>Baigimo data negali būti ankčiau nei pradžios data</span>");
                }
            } else {
                if($("#error\\."+id[1]).length != false) {
                    $("#error\\."+id[1]).remove();
                }
            }
        } else {
            if($("#error\\."+id[1]).length != false) {
                $("#error\\."+id[1]).remove();
            }
        }
    }
});