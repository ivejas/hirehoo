Template.skillsElement.onRendered(function() {
    $('select').parent().find('.caret').remove();
    $('select').material_select();
});

Template.skillsElement.helpers({
    exp: function() {
        return [
            {label: "Pagrindai", value: 0},
            {label: "iki 1m.", value: 1},
            {label: "1-2m.", value: 2},
            {label: "3-4m.", value: 3},
            {label: "4m. ir daugiau", value: 4}
        ];
    },
    getSkill: function(count) {
        return 'skills.'+count+'.name';
    },
    getExperience: function(count) {
        return 'skills.'+count+'.experience';
    }
});

Template.skillsElement.events({
    "click .removeSkill": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('educationCount') - 1;
        $("#ski"+reqNumber).remove();
    }
});