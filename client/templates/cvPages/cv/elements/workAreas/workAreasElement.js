Template.workAreasElement.onRendered(function() {
    $('select').parent().find('.caret').remove();
    $('select').material_select();
});

Template.workAreasElement.helpers({
    getArea: function (count) {
        return 'work_areas.' + count + '.area';
    }
});

Template.workAreasElement.events({
    "click .removeArea": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('areaCount') - 1;
        $("#area"+reqNumber).remove();
    }
});