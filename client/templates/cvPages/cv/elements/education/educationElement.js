Template.educationElement.onCreated(function() {

});

Template.educationElement.onRendered(function() {
    $('select').parent().find('.caret').remove();
    $('select').material_select();
});

Template.educationElement.helpers({
    getDegreeNameValue: function(post, count) {
        console.log(post);
        return post[count].degree_name;
    },
    getInstitutionNameValue: function(post, count) {
        return post[count].institution_name;
    },
    degree: function() {
        return [
            {label: "Vidurinysis", value: 0},
            {label: "Bakalauras", value: 1},
            {label: "Magistras", value: 2},
            {label: "Daktaras", value: 3}
        ];
    },
    year: function() {
        return [2015,2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002,2001,2000,1999,1998,1997,1996,1995,1994,1993,1992,1991,1990,1989,1988,1987,1986,1985,1984,1983,1982,1981,1980];

    },
    getDegreeType: function (count) {
        return 'education.' + count + '.degree_type'
    },
    getDegreeName: function (count) {
        return 'education.' + count + '.degree_name'
    },
    getInstitutionName: function (count) {
        return 'education.' + count + '.institution_name'
    },
    getStartYear: function (count) {
        return 'education.' + count + '.start_time_year'
    },
    getEndYear: function (count) {
        return 'education.' + count + '.end_time_year'
    }
});

Template.educationElement.events({
    "click .removeEducation": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('educationCount') - 1;
        $("#edu"+reqNumber).remove();
    },
    "change .educationStart select": function(event) {
        var id = event.currentTarget.id.split('.');
        var nameStart = id[0]+'\\.'+id[1];
        if($("#"+nameStart+"\\.end_time_year").val() > 0) {
            var startTime = $("#" + nameStart + '\\.start_time_year').val() * 12;
            var endTime = $("#" + nameStart + '\\.end_time_year').val() * 12;
            if ((startTime - endTime) >= 0) {
                if($("#eduError\\."+id[1]).length == false) {
                    $("#" + nameStart + "\\.error").append("<span id='eduError." + id[1] + "'>Baigimo data negali būti ankčiau nei pradžios data</span>");
                }
            } else {
                if($("#eduError\\."+id[1]).length != false) {
                    $("#eduError\\."+id[1]).remove();
                }
            }
        }
    },
    "change .educationEnd select": function(event) {
        var id = event.currentTarget.id.split('.');
        var nameStart = id[0]+'\\.'+id[1];
        if($("#"+nameStart+"\\.end_time_year").val() > 0) {
            var startTime = $("#" + nameStart + '\\.start_time_year').val() * 12;
            var endTime = $("#" + nameStart + '\\.end_time_year').val() * 12;
            if ((startTime - endTime) >= 0) {
                if($("#eduError\\."+id[1]).length == false) {
                    $("#" + nameStart + "\\.error").append("<span id='eduError." + id[1] + "'>Baigimo data negali būti ankčiau nei pradžios data</span>");
                }
            } else {
                if($("#eduError\\."+id[1]).length != false) {
                    $("#eduError\\."+id[1]).remove();
                }
            }
        }
    }
});