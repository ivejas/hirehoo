Template.recommendationsElement.helpers({
    getName: function (count) {
        return 'recommendations.' + count + '.name'
    },
    getCompany: function (count) {
        return 'recommendations.' + count + '.company'
    },
    getRole: function (count) {
        return 'recommendations.' + count + '.role'
    },
    getEmail: function (count) {
        return 'recommendations.' + count + '.email'
    },
    getPhone: function (count) {
        return 'recommendations.' + count + '.phone'
    },
    getNameValue: function (post, count) {
        console.log(post);
        return post[count].name;
    },
    getCompanyValue: function (post, count) {
        return post[count].company;
    },
    getRoleValue: function (post, count) {
        return post[count].role;
    },
    getEmailValue: function (post, count) {
        return post[count].email;
    },
    getPhoneValue: function (post, count) {
        return post[count].phone;
    }
});

Template.recommendationsElement.events({
    "click .removeRec": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('recCount') - 1;
        $("#rec"+reqNumber).remove();
    }
});