Template.requirementsElement.onCreated(function() {

});

Template.requirementsElement.onRendered(function() {

});

Template.requirementsElement.helpers({
    getCity: function(count){
        return 'requirements.'+count+'.city'
    },
    getSalaryFrom: function(count){
        return 'requirements.'+count+'.salary_from'
    }
});

Template.requirementsElement.events({
    "click .removeRequirement": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('reqCount') - 1;
        $("#req"+reqNumber).remove();

    }
});