Template.languagesElement.onRendered(function() {
    $('select').parent().find('.caret').remove();
    $('select').material_select();
});

Template.languagesElement.helpers({
    languages: function() {
        return [
            {label: "Anglų", value: 0},
            {label: "Arabų", value: 1},
            {label: "Armėnų", value: 2},
            {label: "Azerbaidžaniečių", value: 3},
            {label: "Baltarusių", value: 4},
            {label: "Bulgarų", value: 5},
            {label: "Čekų", value: 6},
            {label: "Danų", value: 7},
            {label: "Esperanto", value: 8},
            {label: "Estų", value: 9},
            {label: "Filipinų", value: 10},
            {label: "Graikų", value: 11},
            {label: "Gruzinų", value: 12},
            {label: "Hebrajų", value: 13},
            {label: "Hindi", value: 14},
            {label: "Ispanų", value: 15},
            {label: "Italų", value: 16},
            {label: "Japonų", value: 17},
            {label: "Kinų", value: 18},
            {label: "Korėjiečių", value: 19},
            {label: "Kroatų", value: 20},
            {label: "Latvių", value: 21},
            {label: "Lenkų", value: 22},
            {label: "Lietuvių", value: 23},
            {label: "Lotynų", value: 24},
            {label: "Norvegų", value: 25},
            {label: "Olandų", value: 26},
            {label: "Portugalų", value: 27},
            {label: "Prancūzų", value: 28},
            {label: "Rumunų", value: 29},
            {label: "Rusų", value: 30},
            {label: "Serbų", value: 31},
            {label: "Slovakų", value: 32},
            {label: "Slovėnų", value: 33},
            {label: "Suomių", value: 34},
            {label: "Švedų", value: 35},
            {label: "Turkų", value: 36},
            {label: "Ukrainiečių", value: 37},
            {label: "Vengrų", value: 38},
            {label: "Vokiečių", value: 39}
        ];
    },
    experience: function() {
        return [
            {label: "Blogai", value: 0},
            {label: "Blogiau nei vidutiniškai", value: 1},
            {label: "Vidutiniškai", value: 2},
            {label: "Geriau nei vidutiniškai", value: 3},
            {label: "Gerai", value: 4}
        ];
    },
    getLanguage: function (count) {
        return 'languages.' + count + '.language'
    },
    getSpeaking: function (count) {
        return 'languages.' + count + '.speaking'
    },
    getListening: function (count) {
        return 'languages.' + count + '.listening'
    },
    getWriting: function (count) {
        return 'languages.' + count + '.writing'
    },
    getReading: function (count) {
        return 'languages.' + count + '.reading'
    }
});

Template.languagesElement.events({
    "click .removeLanguage": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('educationCount') - 1;
        $("#lang"+reqNumber).remove();
    }
});