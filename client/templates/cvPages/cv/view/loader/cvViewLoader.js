Template.cvViewLoader.onCreated(function() {
    if(this.data.id != null) {
        Session.set('id', this.data.id);
    }
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        if(FlowRouter.getParam('cvID') != null) {
            var postId = FlowRouter.getParam('cvID');
            var handle = PostSubs.subscribe('specificProfile', postId);
            var handle2 = PostSubs.subscribe('postImages', postId);
            var handle3 = PostSubs.subscribe('myRequests');
            //if(Meteor.user()['profile']['type'] == 2) {
            var handle4 = PostSubs.subscribe('myCompanyProfile');
            //} else if(Meteor.user['profile']['type'] == 2) {
            var handle5 = PostSubs.subscribe('myProfile');
            var handle6 = PostSubs.subscribe('views');
        } else {
            var postId = Session.get('id');
            var handle = Meteor.subscribe('specificProfile', postId);
            var handle2 = Meteor.subscribe('postImages', postId);
            var handle3 = Meteor.subscribe('myRequests');
            var handle6 = Meteor.subscribe('views');
            //if(Meteor.user()['profile']['type'] == 2) {
            var handle4 = Meteor.subscribe('myCompanyProfile');
            //} else if(Meteor.user['profile']['type'] == 2) {
            var handle5 = Meteor.subscribe('myProfile');
        }

        //}

        self.ready.set(handle.ready() && handle2.ready() && handle3.ready() && handle4.ready() && handle5.ready() && handle6.ready());
    });
});

Template.cvViewLoader.helpers({
    subsReady: function() {
        return Template.instance().ready.get();
    }
});