var post;
Template.cvView.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    }
});

Template.cvView.onRendered(function(){
    //Session.set('myChartIsCreated', true);
    //$('.scrollspy').scrollSpy();
    if(Meteor.user()['profile']['type'] == 2) {
        if(FlowRouter.getParam('cvID') != null) {
            Meteor.call("addView", Meteor.userId(), FlowRouter.getParam('cvID'));
        } else {
            Meteor.call("addView", Meteor.userId(), Session.get('id'));
        }
    }
});

Template.cvView.helpers({
    fromSearch: function() {
        if(Session.get('id') != null) {
            return true;
        }
        Session.set('currentPage', 'preview');
        return false;
    },
    post: function() {
        if(FlowRouter.getParam('cvID') != null) {
            var postId = FlowRouter.getParam('cvID');
        } else if(Session.get('id') != null) {
            var postId = Session.get('id');
        } else {
            return false;
        }
        post = Profiles.findOne({"user_id": postId}) || {};
        console.log(post);
        Session.set('skills', post['skills']);
        Session.set('languages', post['languages']);
        return post;
    },

    //isRequested: function(user_id) {
    //    var request = Requests.findOne({"user_id": user_id, "company_id": Meteor.userId(), 'status': 2});
    //    console.log(request);
    //    if(request != null) {
    //        Session.set('age', post['age']);
    //        Session.set('phone', post['phone']);
    //        Session.set('email', post['email']);
    //        Session.set('name', post['first_name']+' '+post['last_name']);
    //        return true;
    //    } else {
    //        alert('hi');
    //        alert(post['age']);
    //        Session.set('age', post['age']);
    //        Session.set('phone', false);
    //        Session.set('email', false);
    //        Session.set('name', 'Vartotojas '+post['user_id']);
    //        return false;
    //    }
    //},
    isApproved: function(user_id) {
        if(FlowRouter.getParam('cvID') != null) {
            var profile = Profiles.findOne({"user_id": Meteor.userId()});
            if(profile != null) {
                if (FlowRouter.getParam('cvID') == profile['user_id']) {
                    return true;
                }
            }
            var request = Requests.findOne({"user_id": FlowRouter.getParam('cvID'), "company_id": Meteor.userId()});
            if (request != null) {
                if (request.status == 2) {
                    return true;
                }
            }
        }
        return false
    },
    isPastWork: function(user) {
        if(user.past_work != null) {
            return true;
        } else {
            return false;
        }
    },
    isRecommendation: function(user) {
        if(user.recommendations != null) {
            return true;
        } else {
            return false;
        }
    },
    getImage: function (image) {
        return Images.findOne({"_id": image}).url();
    },
    getDegree: function(degree) {
        var degreeArray = [
            {label: "Vidurinysis", value: 0},
            {label: "Bakalauras", value: 1},
            {label: "Magistras", value: 2},
            {label: "Daktaras", value: 3}
        ];
        for(x=0;x<degreeArray.length;x++) {
            if(degreeArray[x].value == degree) {
                return degreeArray[x].label;
            }
        }
    },
    getCategory: function (id) {
        var categories = [
            {value: 1, label: "Administravimas/sekretoriavimas"},
            {value: 2, label: "Apskaita/finansai/auditas"},
            {value: 3, label: "Dizainas/architektūra"},
            {value: 4, label: "Draudimas"},
            {value: 5, label: "Eksportas"},
            {value: 6, label: "Elektronika/telekomunikacijos"},
            {value: 7, label: "Energetika"},
            {value: 8, label: "Inžinerija/mechanika"},
            {value: 9, label: "Klientų aptarnavimas/paslaugos"},
            {value: 10, label: "Kompiuteriai/IT/internetas"},
            {value: 11, label: "Kultūra/kūryba"},
            {value: 12, label: "Logistika/transportas"},
            {value: 13, label: "Maisto gamyba"},
            {value: 14, label: "Marketingas/reklama"},
            {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
            {value: 16, label: "Nekilnojamasis turtas"},
            {value: 17, label: "Pardavimų vadyba"},
            {value: 18, label: "Personalo valdymas"},
            {value: 19, label: "Pirkimai/tiekimas"},
            {value: 20, label: "Pramonė/gamyba"},
            {value: 21, label: "Prekyba - konsultavimas"},
            {value: 22, label: "Sandėliavimas"},
            {value: 23, label: "Statyba"},
            {value: 24, label: "Švietimas/mokymai"},
            {value: 25, label: "Teisė"},
            {value: 26, label: "Turizmas/viešbučiai"},
            {value: 27, label: "Vadovavimas/valdymas"},
            {value: 28, label: "Valstybės tarnyba"},
            {value: 29, label: "Žemės ūkis/žuvininkystė"},
            {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
        ];
        for (x = 0; x < categories.length; x++) {
            if (categories[x].value == id) {
                return categories[x].label;
            }
        }
    },
    getLanguage: function(id) {
        var languages = [
            {label: "Anglų", value: 0},
            {label: "Arabų", value: 1},
            {label: "Armėnų", value: 2},
            {label: "Azerbaidžaniečių", value: 3},
            {label: "Baltarusių", value: 4},
            {label: "Bulgarų", value: 5},
            {label: "Čekų", value: 6},
            {label: "Danų", value: 7},
            {label: "Esperanto", value: 8},
            {label: "Estų", value: 9},
            {label: "Filipinų", value: 10},
            {label: "Graikų", value: 11},
            {label: "Gruzinų", value: 12},
            {label: "Hebrajų", value: 13},
            {label: "Hindi", value: 14},
            {label: "Ispanų", value: 15},
            {label: "Italų", value: 16},
            {label: "Japonų", value: 17},
            {label: "Kinų", value: 18},
            {label: "Korėjiečių", value: 19},
            {label: "Kroatų", value: 20},
            {label: "Latvių", value: 21},
            {label: "Lenkų", value: 22},
            {label: "Lietuvių", value: 23},
            {label: "Lotynų", value: 24},
            {label: "Norvegų", value: 25},
            {label: "Olandų", value: 26},
            {label: "Portugalų", value: 27},
            {label: "Prancūzų", value: 28},
            {label: "Rumunų", value: 29},
            {label: "Rusų", value: 30},
            {label: "Serbų", value: 31},
            {label: "Slovakų", value: 32},
            {label: "Slovėnų", value: 33},
            {label: "Suomių", value: 34},
            {label: "Švedų", value: 35},
            {label: "Turkų", value: 36},
            {label: "Ukrainiečių", value: 37},
            {label: "Vengrų", value: 38},
            {label: "Vokiečių", value: 39}
        ];

        for(x=0; x<languages.length; x++) {
            if(languages[x].value == id) {
                return languages[x].label;
            }
        }
    },
    getLanguageSkill: function(id) {
        var skills = [
            {label: "Nemoku", value: 0},
            {label: "Blogai", value: 1},
            {label: "Blogiau nei vidutiniškai", value: 2},
            {label: "Vidutiniškai", value: 3},
            {label: "Geriau nei vidutiniškai", value: 4},
            {label: "Gerai", value: 5}
        ];
        for(x=0; x<skills.length; x++) {
            if(skills[x].value == id) {
                return skills[x].label;
            }
        }
    },

    getExpValue: function(exp) {
        experience = [
            {label: "Pagrindai", value: 0},
            {label: "iki 1m.", value: 1},
            {label: "1-2m.", value: 2},
            {label: "3-4m.", value: 3},
            {label: "4m. ir daugiau", value: 4}
        ];
        for(x = 0; x<experience.length; x++) {
            if (experience[x].value == exp) {
                return experience[x].label;
            }
        }
    },
    getRegistered: function(profile) {
        var x =new Date(profile.createdAt);
        var month = x.getMonth() +1;

        return x.getFullYear()+'-'+ month +'-' + x.getDate();
    },
    getEdited: function(profile) {
        if(profile.updatedAt != null) {
            var x = new Date(profile.updatedAt);
        } else {
            var x =new Date(profile.createdAt);
        }
        var month = x.getMonth() +1;

        return x.getFullYear()+'-'+ month +'-' + x.getDate();
    },
    getBookmarks: function(profile) {
        return Bookmarks.find({"user_id": profile['user_id']}).count();
    },
    getViews: function(profile) {
        return Views.find({"user_id": profile['user_id']}).count();
    },
    getRequests: function(profile) {
        return Requests.find({"user_id": profile['user_id']}).count();
    },
    stillWork: function(end_time) {
        if(end_time == 0) {
            return true;
        }
        return false;
    }
});

Template.cvView.events({
    "click #request": function(event) {
        //Requests.insert({"company_id": Meteor.userId(), "user_id": event.currentTarget.value, "paid": false, "status": 1});
        Meteor.call('requestInfo', Meteor.userId(), event.currentTarget.value);
    }
});