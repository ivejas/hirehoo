Template.cvEditLoader.onCreated(function() {
    var postId = FlowRouter.getParam('cvID');
    if(postId != Meteor.userId()){
        FlowRouter.go('/');
    }
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('myProfile');
        var handle2 = PostSubs.subscribe('myPostImage');
        var handle3 = PostSubs.subscribe('skills');
        var handle4 = PostSubs.subscribe('cities');
        self.ready.set(handle.ready() && handle2.ready() && handle3.ready() && handle4.ready());
    });
    if(Meteor.user()['profile']['type'] != 1) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }
});

Template.cvEditLoader.helpers({
    subsReady: function(name) {
        if(Template.instance().ready.get() == true) {
            var postId = FlowRouter.getParam('cvID');
            post = Profiles.findOne({user_id: postId}) || {};
            console.log(post);
            if (post != {}) {
                if (post.user_id != Meteor.userId()) {
                    Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
                    FlowRouter.go('/');
                }
                if (post['requirements'] != null) {
                    Session.set('reqCount', post['requirements'].length);
                }
                if (post['past_work'] != null) {
                    Session.set('pastWorkCount', post['past_work'].length);
                }
                if (post['education'] != null) {
                    Session.set('educationCount', post['education'].length);
                }
                if (post['languages'] != null) {
                    Session.set('languageCount', post['languages'].length);
                }
                if (post['skills'] != null) {
                    Session.set('skillCount', post['skills'].length);
                }
                if (post['recommendations'] != null) {
                    Session.set('recCount', post['recommendations'].length);
                }
                if (post['work_areas'] != null) {
                    Session.set('areaCount', post['work_areas'].length);
                }
            }
        }
        return Template.instance().ready.get();
    }
});