var post;
Template.cvEdit.onCreated(function() {
    var postId = FlowRouter.getParam('cvID');
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 1) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    if(postId != Meteor.userId()){
        FlowRouter.go('/');
    }
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('specificProfile', postId);
        var handle2 = PostSubs.subscribe('myPostImage');
        var handle3 = PostSubs.subscribe('skills');
        var handle4 = PostSubs.subscribe('cities');
        self.ready.set(handle.ready() && handle2.ready() && handle3.ready() && handle4.ready());
    });
    if(Meteor.user()['profile']['type'] != 1) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }
    Session.set('currentPage', 'edit');
    Session.set('skills', []);
    Session.set('cities', []);
    Session.set('skillSearch', null);
    Session.set('work_area', null);

    Session.set('weOffer', null);
    Session.set('weOfferSearch', null);
    Session.set('reqCount', 0);
    Session.set('pastWorkCount', 0);
    Session.set('educationCount', 0);
    Session.set('languageCount', 0);
    Session.set('skillCount', 0);
    Session.set('recCount', 0);
    Session.set('areaCount', 0);

});


Template.cvEdit.onRendered(function() {
    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.collapsible').collapsible({
        accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });
    $('input#input_text, textarea#short_description').characterCounter();
    $('select').parent().find('.caret').remove();
    $('select').material_select();
});

Template.cvEdit.events({
    "click #addArea": function(event) {
        var count = Session.get('areaCount') + 1;
        Session.set('areaCount', count);
    },
    "click #addRequirement": function(event) {
        var count = Session.get('reqCount') + 1;
        Session.set('reqCount', count);
    },
    "click .addPastWork": function(event) {
        var count = Session.get('pastWorkCount') + 1;
        Session.set('pastWorkCount', count);
    },
    "click .addEducation": function(event) {
        var count = Session.get('educationCount') + 1;
        Session.set('educationCount', count);
    },
    "click .addLanguage": function(event) {
        var count = Session.get('languageCount') + 1;
        Session.set('languageCount', count);
    },
    "click .addSkill": function(event) {
        var count = Session.get('skillCount') + 1;
        Session.set('skillCount', count);
    },
    "click .addRec": function(event) {
        var count = Session.get('recCount') + 1;
        Session.set('recCount', count);
    },
    "click .attribute": function(event) {
        Session.set('target', event.currentTarget.name);
        $('#skillModal').openModal();
        $('#skill_search').val('');
        $("#skill_search").focus();
        $("#skill_search").typing({
            stop: function(event, $elem) {
                Session.set("skillSearch", $("#skill_search").val());
            },
            delay: 400
        });
    },
    "click .attributeCity": function(event) {
        Session.set('targetCity', event.currentTarget.name);
        $('#cityModal').openModal();
        $('#city_search').val('');
        $("#city_search").focus();
        $("#city_search").typing({
            stop: function(event, $elem) {
                Session.set("citySearch", $("#city_search").val());
            },
            delay: 400
        });
    },
    "click .attribute-button": function(event) {
        $('input[name="'+Session.get('target')+'"]').val(event.currentTarget.value);
        //$('input[name="'+Session.get('target')+'"]').focus();
        $('#skillModal').closeModal();
    },
    "click .attribute-button-city": function(event) {
        $('input[name="'+Session.get('targetCity')+'"]').val(event.currentTarget.value);
        //$('input[name="'+Session.get('target')+'"]').focus();
        $('#cityModal').closeModal();
    },

    "click .weOffer": function(event) {
        Session.set('target', event.currentTarget.name);
        $('#weOfferModal').openModal();
        $("#weOffer_search").focus();
        $("#weOffer_search").typing({
            stop: function(event, $elem) {
                Session.set("weOfferSearch", $("#weOffer_search").val());
            },
            delay: 400
        });
    },
    "click .weOffer-button": function(event) {
        $('input[name="'+Session.get('target')+'"]').val(event.currentTarget.value);
        $('#weOfferModal').closeModal();
    }
});

Template.cvEdit.helpers({
    subsReady: function(name) {
        if(Template.instance().ready.get() == true) {
            var postId = FlowRouter.getParam('cvID');
            post = Profiles.findOne({user_id: postId}) || {};
            console.log(post);
            if (post != {}) {
                if (post.user_id != Meteor.userId()) {
                    Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
                    FlowRouter.go('/');
                }
                if (post['requirements'] != null) {
                    Session.set('reqCount', post['requirements'].length);
                }
                if (post['past_work'] != null) {
                    Session.set('pastWorkCount', post['past_work'].length);
                }
                if (post['education'] != null) {
                    Session.set('educationCount', post['education'].length);
                }
                if (post['languages'] != null) {
                    Session.set('languageCount', post['languages'].length);
                }
                if (post['skills'] != null) {
                    Session.set('skillCount', post['skills'].length);
                }
                if (post['recommendations'] != null) {
                    Session.set('recCount', post['recommendations'].length);
                }
                if (post['work_areas'] != null) {
                    Session.set('areaCount', post['work_areas'].length);
                }
            }
        }
        return Template.instance().ready.get();
    },
    weOffer_search: function() {
        if(Session.get('weOfferSearch') != '' && Session.get('weOfferSearch') != null) {
            var search = Session.get('weOfferSearch');
            var results = WeOffer.find({'work_area': Number($("#work_area").val()), 'name': {'$regex': '^.*'+Session.get('weOfferSearch') + ".*\\b", "$options": "i"}});
            if(results.fetch().length == 0) {
                search = {name: search, work_area: Number($("#work_area").val())};
                results = WeOffer.find({'work_area': Number($("#work_area").val())}).fetch();
                results.unshift(search);
                return results
            }
            return results;
        } else {
            return false;
        }
    },

   isAdmin: function() {
       if(Meteor.user()['profile']['type'] == 0) {
           return true;
       } else {
           return false;
       }
   },
    city_search: function() {
        if(Session.get('citySearch') != '' && Session.get('citySearch') != null) {
            var search = Session.get('citySearch').toLowerCase();
            var results = Cities.find({"city": new RegExp(RegExp.escape(search), 'i')}).fetch();
            console.log(search);
            console.log(results);
            if(results.length < 1) {
                return [{city: search}];
            } else {
                return results;
            }
        }
    },
    skill_search: function() {
        if(Session.get('skillSearch') != '' && Session.get('skillSearch') != null) {
            var workAreaArray = [];
            var results = [];
            $('.work_areas select').each(function(index) {
                workAreaArray.push($(this).val());
            });
            var search = Session.get('skillSearch').toLowerCase();
            for(x=0; x<workAreaArray.length; x++) {
                results.push(Skills.find({'work_area': Number(workAreaArray[x]), 'name': new RegExp(RegExp.escape(search), 'i')}).fetch());
            }
            var emptyResult = true;
            for(x=0; x<workAreaArray.length; x++) {
                if(results[x].length != 0) {
                    emptyResult = false;
                }
            }
            if(emptyResult == true) {
                results = [];
                //search = {name: search, work_area: Number(workAreaArray[0])};
                console.log(search);
                for(x=0; x<workAreaArray.length; x++) {
                    results.push(Skills.find({'work_area': Number(workAreaArray[x])}).fetch());
                }
            }
            var finalArray = [];
            for(x=0; x<results.length; x++){
                for(y=0; y<results[x].length; y++){
                    finalArray.push(results[x][y].name.toLowerCase());
                }
            }
            results = [];
            results.unshift(search);
            $.each(finalArray, function(i, el){
                if($.inArray(el, results) === -1) results.push(el);
            });
            console.log(results);
            return results;
        } else {
            return false;
        }
    },
    post: function() {
        return post;
    },
    reqCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('reqCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    pastWorkCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('pastWorkCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    educationCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('educationCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    languageCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('languageCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    skillCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('skillCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    recCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('recCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    //subsReady: function() {
    //    return FlowRouter.subsReady();
    //},
    areaCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('areaCount'); i++){
            countArr.push({});
        }
        return countArr;
    }
});

AutoForm.hooks({
    'updateProfileForm': {
        formToDoc: function(doc) {
            doc.user_id = Meteor.userId();
            doc.email = Meteor.user().emails[0].address;
            return doc;
        },
        formToModifier: function(modifier) {
            if(modifier.$set.education != null) {
                var i = modifier.$set.education.length;
                while (i--) {
                    if (modifier.$set.education[i] == null) {
                        modifier.$set.education.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.requirements != null) {
                i = modifier.$set.requirements.length;
                while (i--) {
                    if (modifier.$set.requirements[i] == null) {
                        modifier.$set.requirements.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.past_work != null) {
                i = modifier.$set.past_work.length;
                while (i--) {
                    if (modifier.$set.past_work[i] == null) {
                        modifier.$set.past_work.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.languages != null) {
                i = modifier.$set.languages.length;
                while (i--) {
                    if (modifier.$set.languages[i] == null) {
                        modifier.$set.languages.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.skills != null) {
                i = modifier.$set.skills.length;
                while (i--) {
                    if (modifier.$set.skills[i] == null) {
                        modifier.$set.skills.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.recommendations != null) {
                i = modifier.$set.recommendations.length;
                while (i--) {
                    if (modifier.$set.recommendations[i] == null) {
                        modifier.$set.recommendations.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.work_areas != null) {
                i = modifier.$set.work_areas.length;
                while (i--) {
                    if (modifier.$set.work_areas[i] == null) {
                        modifier.$set.work_areas.splice(i, 1);
                    }
                }
            }
            Session.set('work_area', modifier.$set.work_area);
            Session.set('work_areas', modifier.$set.work_areas);
            Session.set('skills', modifier.$set.skills);
            Session.set('cities', modifier.$set.requirements);
            return modifier;
        },
        onSuccess: function(formType, result) {
            var skills = Session.get('skills');
            var workAreas = Session.get('work_areas');
            var cities = Session.get('cities');
            for(x=0; x<skills.length; x++) {
                for(y=0; y<workAreas.length; y++) {
                    if (Skills.findOne({name: RegExp.escape(skills[x].name), work_area: workAreas[y].area}) == null) {
                        Skills.insert({name: skills[x].name, "work_area": workAreas[y].area});
                    }
                }
            }
            for(y=0; y<cities.length; y++) {
                if (Cities.findOne({city: RegExp.escape(cities[y].city), country: 23}) == null) {
                    Cities.insert({city: cities[y].city, "country": 23});
                }
            }
            FlowRouter.go('/user/dashboard');
        }
    }
});

RegExp.escape = function(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};