
Template.cvDashboard.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 1) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
});

Template.cvDashboard.onRendered(function() {
    //alert(moment().subtract(7, 'days').toISOString());
    $('select').material_select();
});

Template.cvDashboard.helpers({
    profileExists: function() {
        var profile = Profiles.find({"user_id": Meteor.userId});
        if(profile != null) {
            return true;
        }
        return false;
    },
    searchSuccess: function() {

        return Session.get('searchSuccess');
        //var searches = LastSearches.find({"work_area": profile.work_area});
    },
    popularSkills: function() {
        var profile = Profiles.findOne({"user_id": Meteor.userId()});
        Meteor.call('getPopSkills', profile, function(error, result){
            Session.set('popSkills', result);
        });
        return Session.get('popSkills');
    },
    skillsPresent: function() {
        if(Session.get('popSkills').length == 0) {
            return false;
        } else {
            return true;
        }
    },
    citySuggestions: function() {
        var profile = Profiles.findOne({"user_id": Meteor.userId()});
        Meteor.call('getCitySuggestions', profile, function(error, result){
            Session.set('citySuggestions', result);
        });
        return Session.get('citySuggestions');
    },
    citiesPresent: function() {
        if(Session.get('citySuggestions').length == 0) {}
    },
    getBookmarks: function() {
        return Bookmarks.find({"user_id": Meteor.userId()}).count();
    },
    getRequests: function() {
        return Requests.find().count();
    },
    getViews: function() {
        return Views.find().count();
    },
    getSearchSuccesses: function() {
        var count = SearchSuccesses.find().fetch();
        if(count.length == 0) {
            return 0;
        }
        return count[0]['count'];
    },
    getCompanyCount: function() {
        return CompanyProfiles.find({}).count();
    },
    getCompanyCount7d: function() {
        var weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 6);
        return CompanyProfiles.find({"user_id": Meteor.userId(), "createdAt": {$lte: new Date()}, "createdAt": {$gte: weekAgo}}).count();
    },
    getProfileCount: function() {
        return Profiles.find().count();
    },
    getProfileCount7d: function() {
        var weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 6);
        return Profiles.find({"user_id": Meteor.userId(), "createdAt": {$lte: new Date()}, "createdAt": {$gte: weekAgo}}).count();
    },
    getTicketCount: function() {
        return Tickets.find().count();
    },
    getTicketCount7d: function() {
        var weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 6);
        return Tickets.find({"user_id": Meteor.userId(), "createdAt": {$lte: new Date()}, "createdAt": {$gte: weekAgo}}).count();
    },
    getBookmarkCount: function() {
        return Bookmarks.find().count();
    },
    getApprovedRequests: function() {
        return Requests.find({"status": 2}).count();
    }
});

Template.cvDashboard.events({
    "click .skill-add": function(event) {
        $('#skillModal').openModal();
        $('#skillName').attr("value", event.currentTarget.value);
    },
    "click .city-add": function(event) {
        $('#cityModal').openModal();
        $('#city').attr("value", event.currentTarget.value);
    },
    "click #skillSubmit": function(event) {
        Meteor.call('addSkill', Meteor.userId(), $('#skillName').val(), $('#exp').val());
        $('#skillModal').closeModal();
    },
    "click #citySubmit": function(event) {
        Meteor.call('addCity', Meteor.userId(), $('#city').val(), $('#salary_from').val());
        $('#cityModal').closeModal();
    }
});