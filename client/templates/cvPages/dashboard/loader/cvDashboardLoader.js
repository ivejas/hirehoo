Template.cvDashboardLoader.onCreated(function() {
    Session.set('currentPage', 'dashboard');
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('lastSearches');
        var handle2 = PostSubs.subscribe('mySearchSuccesses');
        var handle3 = PostSubs.subscribe('myProfile');
        var handle4 = PostSubs.subscribe('myCities');
        var handle5 = PostSubs.subscribe('myPersonalViews');
        var handle6 = PostSubs.subscribe('profilesForStats');
        var handle7 = PostSubs.subscribe('companyProfileList');
        var handle8 = PostSubs.subscribe('bookmarkList');
        //var handle9 = PostSubs.subscribe('sucRequestList');
        var handle10 = PostSubs.subscribe('userRequests');
        self.ready.set(handle.ready() && handle2.ready() && handle3.ready() && handle4.ready() && handle5.ready() && handle6.ready() && handle7.ready()
        && handle8.ready() && handle10.ready());
    });
});

Template.cvDashboardLoader.helpers({
    subsReady: function() {
        return Template.instance().ready.get();
    }
});