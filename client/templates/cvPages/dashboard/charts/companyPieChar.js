var chart;
var chartData = [
    {
        value: 0,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Įmonės mano kategorijoje"
    },
    {
        value: 0,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Įmonės kitose kategorijose"
    }
];
Template.companyPieChart.onCreated(function() {
    Session.set('companyChartRendered', false);
});

Template.companyPieChart.onDestroyed(function() {
    delete Session.keys['companyChartRendered'];
});

Template.companyPieChart.onRendered(function() {
    chart = new Chart(document.getElementById("canvas1").getContext("2d")).Pie(chartData, {
    });
    window.dispatchEvent(new Event('resize'));
    Session.set('companyChartRendered', true);
});

Template.companyPieChart.helpers({
    updateData: function() {
        if(Session.get('companyChartRendered')) {
            var profile = Profiles.findOne({"user_id": Meteor.userId()});
            var totalCompanyCount = CompanyProfiles.find().count();
            Session.set('companyCount', totalCompanyCount);
            var catCompanyCount = CompanyProfiles.find({"work_area": profile.work_area}).count();
            Session.set('catCompanyCount', catCompanyCount);
            totalCompanyCount = totalCompanyCount - catCompanyCount;

            chart.segments[0].value = totalCompanyCount;
            chart.segments[1].value = Session.get('catCompanyCount');
            chart.update();
            window.dispatchEvent(new Event('resize'));
        }
    }
});