var chart;
var chartData = [
    {
        value: 0,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Paieškos mano kategorijoje"
    },
    {
        value: 0,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Paieškos kitose kategorijose"
    }
];
Template.searchesPieChart.onCreated(function() {
    Session.set('searchesChartRendered', false);
});

Template.searchesPieChart.onDestroyed(function() {
    delete Session.keys['searchesChartRendered'];
});

Template.searchesPieChart.onRendered(function() {
    chart = new Chart(document.getElementById("canvas3").getContext("2d")).Pie(chartData, {
    });
    Session.set('searchesChartRendered', true);
    window.dispatchEvent(new Event('resize'));
});

Template.searchesPieChart.helpers({
    updateData: function() {
        if(Session.get('searchesChartRendered')) {
            var profile = Profiles.findOne({"user_id": Meteor.userId()});
            var searches = LastSearches.find().count();
            Session.set('searchCount', searches);
            searches = LastSearches.find({"work_area": profile.work_area}).count();
            Session.set('catSearches', searches);
            var otherCats = Session.get('searchCount') - Session.get('catSearches');

            chart.segments[0].value = Session.get('catSearches');
            chart.segments[1].value = otherCats;
            chart.update()
            window.dispatchEvent(new Event('resize'));
        }
    }
});