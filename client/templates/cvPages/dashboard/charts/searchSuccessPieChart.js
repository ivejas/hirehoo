var chart;
var chartData = [
    {
        value: 50,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Patekau į"
    },
    {
        value: 50,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Nepatekau į"
    }
];
Template.searchSuccessPieChart.onCreated(function() {
    Session.set('searchSuccessChartRendered', false);
});

Template.searchSuccessPieChart.onDestroyed(function() {
    delete Session.keys['searchSuccessChartRendered'];
});

Template.searchSuccessPieChart.onRendered(function() {
    var canvas = document.getElementById("canvas4").getContext("2d");
    chart = new Chart(canvas).Pie(chartData, {});
    window.dispatchEvent(new Event('resize'));
    Session.set('searchSuccessChartRendered', true);
});

Template.searchSuccessPieChart.helpers({
    updateData: function() {
        if(Session.get('searchSuccessChartRendered')) {
            var profile = Profiles.findOne({"user_id": Meteor.userId()});
            var searches = LastSearches.find({"work_area": profile.work_area}).count();
            Session.set('catSearches', searches);
            var successes = SearchSuccesses.findOne({"user_id": profile.user_id});
            if (successes == null) {
                successes = 0
            }
            Session.set('searchSuccesses', successes.count);
            var otherCats = Session.get('catSearches') - Session.get('searchSuccesses');

            chart.segments[0].value = Session.get('searchSuccesses');
            chart.segments[1].value = otherCats;
            window.dispatchEvent(new Event('resize'));
            chart.update();
            window.dispatchEvent(new Event('resize'));
        }
    }
});