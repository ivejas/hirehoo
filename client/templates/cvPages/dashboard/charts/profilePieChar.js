var chart;
var chartData = [
    {
        value: 0,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Įmonės mano kategorijoje"
    },
    {
        value: 0,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Įmonės kitose kategorijose"
    }
];
Template.profilePieChart.onCreated(function() {
    Session.set('profileChartRendered', false);
});

Template.profilePieChart.onDestroyed(function() {
    delete Session.keys['profileChartRendered'];
});

Template.profilePieChart.onRendered(function() {
    chart = new Chart(document.getElementById("canvas2").getContext("2d")).Pie(chartData, {
    });
    Session.set('profileChartRendered', true);
    window.dispatchEvent(new Event('resize'));
});

Template.profilePieChart.helpers({
    updateData: function() {
        if(Session.get('profileChartRendered')) {
            var profile = Profiles.findOne({"user_id": Meteor.userId()});
            var totalProfileCount = Profiles.find().count();
            Session.set('profileCount', totalProfileCount);
            var catProfileCount = Profiles.find({"work_area": profile.work_area}).count();
            Session.set('catProfileCount', catProfileCount);
            totalProfileCount = totalProfileCount - catProfileCount;

            chart.segments[0].value = Session.get('catProfileCount');
            chart.segments[1].value = totalProfileCount;
            chart.update();
            window.dispatchEvent(new Event('resize'));
        }
    }
});