var chart;
var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
            label: "Peržiūrų skaičius",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [65, 59, 80, 81, 56, 55, 40]
        }
    ]
};
Template.viewLineChart.onCreated(function() {
    Session.set('lineChartRendered', false);
});

Template.viewLineChart.onDestroyed(function() {
    delete Session.keys['lineChartRendered'];
});

Template.viewLineChart.onRendered(function() {
    //var data = {
    //    labels: Session.get('viewLabels'),
    //    series: [
    //        Session.get('viewValues')
    //    ]
    //};
    //console.log(data);
    //var options = {
    //    fullWidth: true,
    //    height: 300,
    //    chartPadding: {
    //        right: 40
    //    },
    //    plugins: [
    //        Chartist.plugins.tooltip()
    //    ]
    //};
    //
    //var responsiveOptions = [
    //    ['screen and (min-width: 640px)', {
    //        chartPadding: 30,
    //        labelOffset: 100,
    //        labelDirection: 'explode',
    //        labelInterpolationFnc: function(value) {
    //            return value;
    //        }
    //    }],
    //    ['screen and (min-width: 1024px)', {
    //        labelOffset: 45,
    //        chartPadding: 30
    //    }]
    //];
    //chart = new Chartist.Line('#view-line', data, options, responsiveOptions);
    chart = new Chart(document.getElementById("canvas5").getContext("2d")).Line(data, {bezierCurve: false,
    });
    window.dispatchEvent(new Event('resize'));
    Session.set('lineChartRendered', true);
});

Template.viewLineChart.helpers({
    updateData: function() {
        if(Session.get('lineChartRendered') == true) {
            var weekAgo = new Date();
            weekAgo.setDate(weekAgo.getDate() - 6);
            var searches = Views.find({
                "createdAt": {$lte: new Date()},
                "createdAt": {$gte: weekAgo}
            }).fetch();
            Session.set('weeklyViews', searches);
            var labels = [];
            var viewCountArray = [0,0,0,0,0,0,0];
            for (var x = 0; x < 7; x++) {
                if (weekAgo.getMonth() + 1 < 10) {
                    if (weekAgo.getDate() < 10) {
                        var date = "0" + weekAgo.getMonth() + 1 + '-0' + weekAgo.getDate();
                    } else {
                        var date = "0" + weekAgo.getMonth() + 1 + '-' + weekAgo.getDate();
                    }
                } else {
                    if (weekAgo.getDate() < 10) {
                        var date = weekAgo.getMonth() + 1 + '-0' + weekAgo.getDate();
                    } else {
                        var date = weekAgo.getMonth() + 1 + '-' + weekAgo.getDate();
                    }
                }
                weekAgo.setDate(weekAgo.getDate() + 1);
                labels.push(date);
            }
            Session.set('lineLabels', labels);

            for(var x=0;x<searches.length;x++) {
                if(searches[x].createdAt.getMonth()+1 < 10) {
                    if(searches[x].createdAt.getDate() < 10) {
                        var date = "0" + searches[x].createdAt.getMonth()+1 + '-0' + searches[x].createdAt.getDate();
                    } else {
                        var date = "0" + searches[x].createdAt.getMonth()+1 + '-' + searches[x].createdAt.getDate();
                    }
                } else {
                    if(weekAgo.getDate() < 10) {
                        var date = searches[x].createdAt.getMonth()+1 + '-0' + searches[x].createdAt.getDate();
                    } else {
                        var date = searches[x].createdAt.getMonth()+1 + '-' + searches[x].createdAt.getDate();
                    }
                }
                for(var y=0;y<labels.length;y++) {
                    if(date == labels[y]) {
                        viewCountArray[y]++;
                    }
                }

            }
            Session.set('viewCount', viewCountArray);
            for(var x=0;x< viewCountArray.length; x++) {
                chart.datasets[0].points[x].value = viewCountArray[x];
                chart.datasets[0].points[x].label = Session.get('lineLabels')[x];
            }
            console.log(chart.datasets[0].points[0]);
            chart.scale.xLabels = Session.get('lineLabels');
            console.log('hi');
            console.log(chart);
            chart.update();
            console.log('to');
            window.dispatchEvent(new Event('resize'));
            return viewCountArray;
        }
    }
});