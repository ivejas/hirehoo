Template.requestIndex.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 2) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('myRequestedProfiles');
        var handle2 = PostSubs.subscribe('myRequests');
        self.ready.set(handle.ready() && handle2.ready());
    });
    Session.set('currentPage', 'requests');
});

Template.requestIndex.onRendered(function() {
    $('.tooltipped').tooltip({delay: 5});
});

Template.requestIndex.helpers({
    subsReady: function() {
        return Template.instance().ready.get();
    },
    results: function() {
        var requests = Requests.find({"company_id": Meteor.userId()}).fetch();
        var profileArray = [];
        for(x = 0; x<requests.length; x++) {
            var tempProfile = Profiles.findOne({"user_id": requests[x].user_id});
            profileArray.push(tempProfile);
        }
        return profileArray;
    },

    getExperience: function(result) {
        var years = 0;
        var months = 0;
        for(x = 0; x < result.past_work.length; x++) {
            if(result.past_work[x].work_area == Session.get('work_area')) {
                tempEndYear = result.past_work[x].end_time_year;
                tempEndMonth = result.past_work[x].end_time_month;
                if(tempEndYear == 0) {
                    tempEndYear = new Date();
                    tempEndYear = tempEndYear.getFullYear();
                    tempEndMonth = new Date();
                    tempEndMonth = tempEndMonth.getMonth() + 1;
                }

                tempStartYear = result.past_work[x].start_time_year;
                tempStartMonth = result.past_work[x].start_time_month;
                tempYear = tempEndYear - tempStartYear;
                tempMonth = tempEndMonth - tempStartMonth;
                if(tempMonth < 0) {
                    tempYear--;
                    tempMonth = 12 + tempMonth;
                }
                years += tempYear;
                months += tempMonth;
            }
        }
        if(years == 0 && months == 0) {
            return "Nėra";
        } else {
            if (years == 0) {
                if (months == 1) {
                    return "" + months + " mėnesis";
                }
                return "" + months + " mėnesiai";
            }
            if (months == 1) {
                return "" + years + " m. " + months + " mėnesis";
            } else {
                return "" + years + " m. " + months + " mėnesiai";
            }
        }

    },

    getSalary: function(result) {
        for (x = 0; x < result.requirements.length; x++){
            for (y = 0; y < result.requirements[x].workplace.length; y++) {
                if (result.requirements[x].workplace[y].city == Session.get('city') || result.requirements[x].workplace[y].city.toLowerCase() == Session.get('city').toLowerCase()) {
                    return result.requirements[x].workplace[y].salary_from;
                }
            }
        }
        return false;
    },

    getPosition: function(result) {
        for (x = 0; x < result.requirements.length; x++){
            for (y = 0; y < result.requirements[x].workplace.length; y++) {
                if (result.requirements[x].workplace[y].city == Session.get('city') || result.requirements[x].workplace[y].city.toLowerCase() == Session.get('city').toLowerCase()) {
                    return result.requirements[x].desired_position;
                }
            }
        }
        return false;
    },

    getDegree: function(result) {
        var tempDegree = -1;
        for(x = 0; x < result.education.length; x++) {
            if(result.education[x].degree_type > tempDegree) {
                tempDegree = result.education[x].degree_type;
            }
        }
        if(tempDegree == 0 ) {
            return "Vidurinis";
        } else if(tempDegree == 1) {
            return "Bakalauras";
        } else if(tempDegree == 2) {
            return "Magistras"
        } else if(tempDegree == 3) {
            return "Daktaras";
        }
        return false;
    },

    getLanguages: function(result) {
        languages = [
            {label: "Anglų", value: 0},
            {label: "Arabų", value: 1},
            {label: "Armėnų", value: 2},
            {label: "Azerbaidžaniečių", value: 3},
            {label: "Baltarusių", value: 4},
            {label: "Bulgarų", value: 5},
            {label: "Čekų", value: 6},
            {label: "Danų", value: 7},
            {label: "Esperanto", value: 8},
            {label: "Estų", value: 9},
            {label: "Filipinų", value: 10},
            {label: "Graikų", value: 11},
            {label: "Gruzinų", value: 12},
            {label: "Hebrajų", value: 13},
            {label: "Hindi", value: 14},
            {label: "Ispanų", value: 15},
            {label: "Italų", value: 16},
            {label: "Japonų", value: 17},
            {label: "Kinų", value: 18},
            {label: "Korėjiečių", value: 19},
            {label: "Kroatų", value: 20},
            {label: "Latvių", value: 21},
            {label: "Lenkų", value: 22},
            {label: "Lietuvių", value: 23},
            {label: "Lotynų", value: 24},
            {label: "Norvegų", value: 25},
            {label: "Olandų", value: 26},
            {label: "Portugalų", value: 27},
            {label: "Prancūzų", value: 28},
            {label: "Rumunų", value: 29},
            {label: "Rusų", value: 30},
            {label: "Serbų", value: 31},
            {label: "Slovakų", value: 32},
            {label: "Slovėnų", value: 33},
            {label: "Suomių", value: 34},
            {label: "Švedų", value: 35},
            {label: "Turkų", value: 36},
            {label: "Ukrainiečių", value: 37},
            {label: "Vengrų", value: 38},
            {label: "Vokiečių", value: 39}
        ];
        langArray = [];
        for(x = 0; x < result.languages.length; x++) {
            for(y = 0; y < languages.length; y++) {
                if(languages[y].value == result.languages[x].language) {
                    langArray.push(languages[y].label);
                    break;
                }
            }
        }
        return langArray.toString();
    },

    getSkills: function(result) {
        skillArray = [];
        if(result.skills != null) {
            for (x = 0; x < result.skills.length; x++) {
                skillArray.push(result.skills[x].name);
            }
            return skillArray.toString();
        } else {
            return 'None';
        }
    },

    getDesires: function(result) {
        desireArray = [];
        for(x=0; x<result.requirements.length; x++) {
            if (result.requirements[x].desires != null) {
                for (y = 0; y < result.requirements[x].desires.length; y++) {
                    desireArray.push(result.requirements[x].desires[y]);
                }
            }
        }
        if(desireArray.length == 0) {
            return "None"
        } else {
            return desireArray.toString();
        }
    },
    isAccepted: function(user_id) {
        var requests = Requests.findOne({"company_id": Meteor.userId(), "user_id": user_id});
        if(requests.status == 2) {
            return true;
        } else {
            return false;
        }
    },
    isRejected: function(user_id) {
        var requests = Requests.findOne({"company_id": Meteor.userId(), "user_id": user_id});
        if(requests.status == 0) {
            return true;
        } else {
            return false;
        }
    }
});

Template.requestIndex.events({
    "click .remove-request": function(event) {
        if(!$(event.currentTarget).hasClass("disabled")) {
            Meteor.call('removeRequest', Meteor.userId(), event.currentTarget.value);
        }
    }
});