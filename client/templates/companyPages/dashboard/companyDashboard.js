Template.companyDashboard.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 2) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    Session.set('currentPage', 'dashboard');
});

Template.companyDashboard.onRendered(function() {
    //alert(moment().subtract(7, 'days').toISOString());
    $('.scrollspy').scrollSpy();
    $('select').material_select();
});

Template.companyDashboard.helpers({
    profileExists: function() {
        var profile = Profiles.find({"user_id": Meteor.userId});
        if(profile != null) {
            return true;
        }
        return false;
    },
    searchSuccess: function() {

        return Session.get('searchSuccess');
        //var searches = LastSearches.find({"work_area": profile.work_area});
    },
    popularSkills: function() {
        var profile = Profiles.findOne({"user_id": Meteor.userId()});
        Meteor.call('getPopSkills', profile, function(error, result){
            Session.set('popSkills', result);
        });
        return Session.get('popSkills');
    },
    skillsPresent: function() {
        if(Session.get('popSkills').length == 0) {
            return false;
        } else {
            return true;
        }
    },
    citySuggestions: function() {
        var profile = Profiles.findOne({"user_id": Meteor.userId()});
        Meteor.call('getCitySuggestions', profile, function(error, result){
            Session.set('citySuggestions', result);
        });
        return Session.get('citySuggestions');
    },
    citiesPresent: function() {
        if(Session.get('citySuggestions').length == 0) {}
    },
    subsReady: function() {
        return FlowRouter.subsReady();
    },
    getBookmarks: function() {
        return Bookmarks.find({"user_id": Meteor.userId()}).count();
    },
    getRequests: function() {
        return Requests.find({"user_id": Meteor.userId()}).count();
    },
    getViews: function() {
        return Views.find({"user_id": Meteor.userId()}).count();
    },
    getSearchSuccesses: function() {
        return SearchSuccesses.find({"user_id": Meteor.userId()}).count();
    },
    getCompanyCount: function() {
        return CompanyProfiles.find({}).count();
    },
    getCompanyCount7d: function() {
        var weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 6);
        return CompanyProfiles.find({"createdAt": {$lte: new Date()}, "createdAt": {$gte: weekAgo}}).count();
    },
    getProfileCount: function() {
        return Profiles.find().count();
    },
    getProfileCount7d: function() {
        var weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 6);
        return Profiles.find({"createdAt": {$lte: new Date()}, "createdAt": {$gte: weekAgo}}).count();
    },
    getTicketCount: function() {
        return Tickets.find().count();
    },
    getTicketCount7d: function() {
        var weekAgo = new Date();
        weekAgo.setDate(weekAgo.getDate() - 6);
        return Tickets.find({"createdAt": {$lte: new Date()}, "createdAt": {$gte: weekAgo}}).count();
    },
    getBookmarkCount: function() {
        return Bookmarks.find().count();
    },
    getApprovedRequests: function() {
        return Requests.find({"status": 2}).count();
    }
});