Template.companyDashboardLoader.onCreated(function() {
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('lastSearches');
        var handle2 = PostSubs.subscribe('searchSuccesses');
        var handle3 = PostSubs.subscribe('specificCompanyProfile');
        var handle4 = PostSubs.subscribe('companyProfileList');
        var handle5 = PostSubs.subscribe('profilesForStats');
        var handle6 = PostSubs.subscribe('bookmarkList');
        var handle7 = PostSubs.subscribe('sucRequestList');
        self.ready.set(handle.ready() && handle2.ready() && handle3.ready() && handle4.ready() && handle5.ready() && handle6.ready()
        && handle7.ready());
    });
});

Template.companyDashboardLoader.helpers({
    subsReady: function() {
        return Template.instance().ready.get();
    }
});