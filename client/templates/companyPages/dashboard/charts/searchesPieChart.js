var chart;
var chartData = [
    {
        value: 0,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Paieškos mano kategorijoje"
    },
    {
        value: 0,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Paieškos kitose kategorijose"
    }
];
Template.profilePieChartComp.onCreated(function() {
    Session.set('searchChartRendered', false);
});

Template.profilePieChartComp.onDestroyed(function() {
    delete Session.keys['searchChartRendered'];
});

Template.searchesPieChartComp.onRendered(function() {
    chart = new Chart(document.getElementById("canvas3").getContext("2d")).Pie(chartData, {
    });
    Session.set('searchChartRendered', true);
});

Template.searchesPieChartComp.helpers({
    updateData: function() {
        if(Session.get('searchChartRendered')) {
            var profile = CompanyProfiles.findOne({"user_id": Meteor.userId()});
            var searches = LastSearches.find().count();
            Session.set('searchCount', searches);
            searches = LastSearches.find({"work_area": profile.work_area}).count();
            Session.set('catSearches', searches);
            var otherCats = Session.get('searchCount') - Session.get('catSearches');

            chart.segments[0].value = Session.get('catSearches');
            chart.segments[1].value = otherCats;
            chart.update();
        }
    }
});