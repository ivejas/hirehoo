var chart;
var chartData = [
    {
        value: 0,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Vartotojai mano kategorijoje"
    },
    {
        value: 0,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Vartotojai kitose kategorijose"
    }
];
Template.profilePieChartComp.onCreated(function() {
    Session.set('profileChartRendered', false);
});

Template.profilePieChartComp.onDestroyed(function() {
    delete Session.keys['profileChartRendered'];
});

Template.profilePieChartComp.onRendered(function() {
    chart = new Chart(document.getElementById("canvas2").getContext("2d")).Pie(chartData, {
    });
    Session.set('profileChartRendered', true);
});

Template.profilePieChartComp.helpers({
    updateData: function() {
        if(Session.get('profileChartRendered')) {
            var profile = CompanyProfiles.findOne({"user_id": Meteor.userId()});
            var totalProfileCount = CompanyProfiles.find().count();
            Session.set('profileCount', totalProfileCount);
            var catProfileCount = CompanyProfiles.find({"work_area": profile.work_area}).count();
            Session.set('catProfileCount', catProfileCount);
            totalProfileCount = totalProfileCount - catProfileCount;
            chart.segments[0].value = Session.get('catProfileCount');
            chart.segments[1].value = totalProfileCount;
            chart.update();
        }
    }
});