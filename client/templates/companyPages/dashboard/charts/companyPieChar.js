var chart;
var chartData = [
    {
        value: 0,
        color: "#FFAB40",
        highlight: "#5AD3D1",
        label: "Įmonės mano kategorijoje"
    },
    {
        value: 0,
        color: "#1A97A7",
        highlight: "#FFC870",
        label: "Įmonės kitose kategorijose"
    }
    ];
Template.companyPieChartComp.onCreated(function() {
    Session.set('companyChartRendered', false);
});

Template.companyPieChartComp.onDestroyed(function() {
    delete Session.keys['companyChartRendered'];
});



Template.companyPieChartComp.onRendered(function() {
    chart = new Chart(document.getElementById("canvas").getContext("2d")).Pie(chartData, {
    });
    Session.set('companyChartRendered', true);
});

Template.companyPieChartComp.helpers({
    updateData: function() {
        if(Session.get('companyChartRendered')) {
            var profile = CompanyProfiles.findOne({"user_id": Meteor.userId()});
            var totalCompanyCount = CompanyProfiles.find().count();
            Session.set('companyCount', totalCompanyCount);
            var catCompanyCount = CompanyProfiles.find({"work_area": profile.work_area}).count();
            Session.set('catCompanyCount', catCompanyCount);
            totalCompanyCount = totalCompanyCount - catCompanyCount;
            chart.segments[0].value = Session.get('catCompanyCount');
            chart.segments[1].value = totalCompanyCount;
            chart.update();
        }
    }
});