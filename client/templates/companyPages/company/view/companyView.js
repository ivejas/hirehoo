Template.companyView.onCreated(function() {
    var postId = FlowRouter.getParam('companyID');
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('specificCompanyProfile', postId);
        var handle2 = PostSubs.subscribe('companyImages', postId);
        var handle3 = PostSubs.subscribe('weOffer');
        self.ready.set(handle.ready() && handle2.ready() && handle3.ready());
    });
    Session.set('imageArray', null);
    Session.set('imageArrayNew', 1);
    Session.set('currentPage', "companyPreview");
});

Template.companyView.onRendered(function(){
    $('.scrollspy').scrollSpy();
});

Template.companyView.helpers({
    getImage: function(image){
        var image = Images.findOne({"_id": image}).url();
        return image;
    },
    getCategory: function(id) {
        var categories = [
            {value: 1, label: "Administravimas/sekretoriavimas"},
            {value: 2, label:  "Apskaita/finansai/auditas"},
            {value: 3, label: "Dizainas/architektūra"},
            {value: 4, label: "Draudimas"},
            {value: 5, label: "Eksportas"},
            {value: 6, label: "Elektronika/telekomunikacijos"},
            {value: 7, label: "Energetika"},
            {value: 8, label: "Inžinerija/mechanika"},
            {value: 9, label: "Klientų aptarnavimas/paslaugos"},
            {value: 10, label: "Kompiuteriai/IT/internetas"},
            {value: 11, label: "Kultūra/kūryba"},
            {value: 12, label: "Logistika/transportas"},
            {value: 13, label: "Maisto gamyba"},
            {value: 14, label: "Marketingas/reklama"},
            {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
            {value: 16, label: "Nekilnojamasis turtas"},
            {value: 17, label: "Pardavimų vadyba"},
            {value: 18, label: "Personalo valdymas"},
            {value: 19, label: "Pirkimai/tiekimas"},
            {value: 20, label: "Pramonė/gamyba"},
            {value: 21, label: "Prekyba - konsultavimas"},
            {value: 22, label: "Sandėliavimas"},
            {value: 23, label: "Statyba"},
            {value: 24, label: "Švietimas/mokymai"},
            {value: 25, label: "Teisė"},
            {value: 26, label: "Turizmas/viešbučiai"},
            {value: 27, label: "Vadovavimas/valdymas"},
            {value: 28, label: "Valstybės tarnyba"},
            {value: 29, label: "Žemės ūkis/žuvininkystė"},
            {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
        ];
        for(x=0; x<categories.length; x++) {
            if(categories[x].value == id) {
                return categories[x].label;
            }
        }
    },
    getPictures: function(pictures) {
        var images = [];
        console.log("Pictures:");
        console.log(pictures);
        for(x=0; x<pictures.length; x++) {
            var image = Images.findOne({_id: pictures[x]});
            images.push(image.url());
        }
        console.log(images);
        if(images != []) {
            $('.slider').slider({full_width: false});
            return images;
        } else {
            return false;
        }
    },


    picturesPresent: function(pictures) {
        if(pictures != null) {
            return true;
        } else {
            return false;
        }
    },

    post: function() {
        var postId = FlowRouter.getParam('companyID');
        var post = CompanyProfiles.findOne({user_id: postId}) || {};
        if(post['logo'] != null) {

            Session.set('companyLogo', Images.findOne({"_id": post['logo']}).url())
        }
        Session.set('name', post['company_name']);
        console.log(post);
        return post;
    },

    subsReady: function() {
        return Template.instance().ready.get();
    }
});

Meteor.startup(function () {
    $(document).ready(function(){

    });
});