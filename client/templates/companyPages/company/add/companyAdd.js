Template.companyAdd.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 2) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    Session.set('currentPage', 'companyAdd');
    Session.set('weOffer', null);
    Session.set('weOfferSearch', null);
    Session.set('picCount', 0);
    Session.set('offerCount', 0);
    FS.debug = true;
    this.subscribe = Meteor.subscribe('weOffer');
    this.subscribe = Meteor.subscribe('images');
});

Template.companyAdd.onRendered(function() {
    if(Meteor.user()['profile']['type'] != 2) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }
    this.subscribe = Meteor.subscribe('companyProfiles');
    if(CompanyProfiles.findOne({"user_id": Meteor.userId()}) != null) {
        FlowRouter.go('/company/edit/'+Meteor.userId());
    }
});

Template.companyAdd.events({
    "click #addPicture": function(event) {
        var count = Session.get('picCount') + 1;
        Session.set('picCount', count);
    },
    "click #addOffer": function(event) {
        var count = Session.get('offerCount') + 1;
        Session.set('offerCount', count);
    },
    "click .attribute": function(event) {
        Session.set('target', event.currentTarget.name);
        $('#weOfferModal').openModal();
        $('#weOffer_search').val('');
        $("#weOffer_search").focus();
        $("#weOffer_search").typing({
            stop: function(event, $elem) {
                Session.set("weOfferSearch", $("#weOffer_search").val());
            },
            delay: 400
        });
    },
    "click .attribute-button": function(event) {
        console.log(event.currentTarget.value);
        $('input[name="'+Session.get('target')+'"]').val(event.currentTarget.value);
        $('input[name="'+Session.get('target')+'"]').focus();
        $('#weOfferModal').closeModal();
    }
});

Template.companyAdd.helpers({
    subsReady: function() {
        return FlowRouter.subsReady();
    },
    picCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('picCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    offerCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('offerCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    weOffer_search: function() {
        if(Session.get('weOfferSearch') != '' && Session.get('weOfferSearch') != null) {
            var search = Session.get('weOfferSearch');
            var results = WeOffer.find({'work_area': Number($("#work_area").val()), 'name': {'$regex': '^.*'+Session.get('weOfferSearch') + ".*\\b", "$options": "i"}});
            console.log(results.fetch());
            if(results.fetch().length == 0) {
                console.log(search);
                search = {name: search, work_area: Number($("#work_area").val())};
                results = WeOffer.find({'work_area': Number($("#work_area").val())}).fetch();
                results.unshift(search);
                console.log(results);
                return results
            }
            return results;
        } else {
            return false;
        }
    }
});

AutoForm.hooks({
    'insertCompanyProfileForm': {
        formToDoc: function(doc) {
            if(doc.pictures != null) {
                var i = doc.pictures.length;
                while (i--) {
                    if (doc.pictures[i] == null) {
                        doc.pictures.splice(i, 1);
                    }
                }
            }
            if(doc.weOffer != null) {
                var i = doc.weOffer.length;
                while (i--) {
                    if (doc.weOffer[i] == null) {
                        doc.weOffer.splice(i, 1);
                    }
                }
            }
            doc.user_id = Meteor.userId();
            Session.set('weOffer', doc.weOffer);
            console.log(doc);
            return doc;
        },
        onSuccess: function(formType, result) {
            var weOffer = Session.get('weOffer');

            for(x=0; x<weOffer.length; x++) {
                if(WeOffer.findOne({name: weOffer[x].name}) == null) {
                    WeOffer.insert({name: weOffer[x].name, "work_area": Number($("#work_area").val())});
                }
            }
            FlowRouter.go('/');
        }
    }
});
