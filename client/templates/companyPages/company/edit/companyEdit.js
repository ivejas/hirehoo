Template.companyEdit.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 2) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    var postId = FlowRouter.getParam('companyID');
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('myCompanyProfile');
        var handle2 = PostSubs.subscribe('companyImages', postId);
        self.ready.set(handle.ready() && handle2.ready());
    });
    if(Meteor.user()['profile']['type'] != 2) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }

    Session.set('weOffer', null);
    Session.set('weOfferSearch', null);
    Session.set('work_area', null);
    Session.set('picCount', 0);
    Session.set('offerCount', 0);


    Session.set('currentPage', 'edit');
});

Template.companyEdit.onRendered(function() {
    $(document).ready(function(){
        $('.button-collapse').sideNav();
        $('.parallax').parallax();
        $('.tooltipped').tooltip({delay: 5});
    });
    $(document).ready(function(){
        $('.collapsible').collapsible({
            accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    });


});

Template.companyEdit.events({
    "click #addPicture": function(event) {
        var count = Session.get('picCount') + 1;
        Session.set('picCount', count);
    },
    "click #addOffer": function(event) {
        var count = Session.get('offerCount') + 1;
        Session.set('offerCount', count);
    },
    "click .attribute": function(event) {
        Session.set('target', event.currentTarget.name);
        $('#weOfferModal').openModal();
        $('#weOffer_search').val('');
        $("#weOffer_search").focus();
        $("#weOffer_search").typing({
            stop: function(event, $elem) {
                Session.set("weOfferSearch", $("#weOffer_search").val());
            },
            delay: 400
        });
    },
    "click .attribute-button": function(event) {
        $('input[name="'+Session.get('target')+'"]').val(event.currentTarget.value);
        $('input[name="'+Session.get('target')+'"]').focus();
        $('#weOfferModal').closeModal();
    }
});

Template.companyEdit.helpers({
    subsReady: function() {
        if(Template.instance().ready.get() == true) {
            var profile = CompanyProfiles.findOne({"user_id": Meteor.userId()});
            if(profile.user_id != Meteor.userId()) {
                Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
                FlowRouter.go('/');
            }
        }
        return Template.instance().ready.get();
    },
    picCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('picCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    offerCount: function() {
        var countArr = [];
        for (var i=0; i<Session.get('offerCount'); i++){
            countArr.push({});
        }
        return countArr;
    },
    post: function() {
        var postId = FlowRouter.getParam('companyID');
        var post = CompanyProfiles.findOne({user_id: postId}) || {};
        if(post != null) {
            if (post['pictures'] != null) {
                Session.set('picCount', post['pictures'].length);
            }
            if (post['weOffer'] != null) {
                Session.set('offerCount', post['weOffer'].length);
            }
        }
        return post;
    },

    isAdmin: function() {
        if(Meteor.user()['profile']['type'] == 0) {
            return true;
        } else {
            return false;
        }
    },
    weOffer_search: function() {
        if(Session.get('weOfferSearch') != '' && Session.get('weOfferSearch') != null) {
            var search = Session.get('weOfferSearch');
            var results = WeOffer.find({'work_area': Number($(".work_area").val()), 'name': {'$regex': '^.*'+Session.get('weOfferSearch') + ".*\\b", "$options": "i"}});
            if(results.fetch().length == 0) {
                search = {name: search, work_area: Number($(".work_area").val())};
                results = WeOffer.find({'work_area': Number($(".work_area").val())}).fetch();
                results.unshift(search);
                return results
            }
            return results;
        } else {
            return false;
        }
    }
});

AutoForm.hooks({
    'updateCompanyProfileForm': {
        formToDoc: function(doc) {
            doc.user_id = Meteor.userId();
            Session.set('weOffer', doc.weOffer);
            //alert(Meteor.userId());
            return doc;
        },
        formToModifier: function(modifier) {
            if(modifier.$set.pictures != null) {
                var i = modifier.$set.pictures.length;
                while (i--) {
                    if (modifier.$set.pictures[i] == null) {
                        modifier.$set.pictures.splice(i, 1);
                    }
                }
            }
            if(modifier.$set.weOffer != null) {
                var i = modifier.$set.weOffer.length;
                while (i--) {
                    if (modifier.$set.weOffer[i] == null) {
                        modifier.$set.weOffer.splice(i, 1);
                    }
                }
            }
            Session.set('work_area', modifier.$set.work_area);
            Session.set('weOffer', modifier.$set.weOffer);
            return modifier;
        },
        onSuccess: function(formType, result) {
            var weOffer = Session.get('weOffer');
            for(x=0; x<weOffer.length; x++) {
                if(WeOffer.findOne({name: weOffer[x].name}) == null) {
                    WeOffer.insert({name: weOffer[x].name, "work_area": Session.get('work_area')});
                }
            }
            FlowRouter.go('/');
        }
    }
});