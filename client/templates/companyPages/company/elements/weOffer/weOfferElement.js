Template.weOfferElement.onRendered(function() {
    $('select').parent().find('.caret').remove();
    $('select').material_select();
});

Template.weOfferElement.helpers({
    getWeOffer: function(count) {
        return 'weOffer.'+count+'.name';
    }
});

Template.weOfferElement.events({
    "click .removeWeOffer": function(event) {
        var reqNumber = event.currentTarget.id.split('_')[1];
        var count = Session.get('offerCount') - 1;
        $("#offer"+reqNumber).remove();
        //Session.set('reqCount', count);

    }
});