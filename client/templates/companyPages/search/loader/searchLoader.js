Template.searchLoader.onCreated(function() {
    if(Meteor.user()['profile']['type'] != 2) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('skills');
        var handle2 = PostSubs.subscribe('myBookmarks');
        var handle3 = PostSubs.subscribe('myRequests');
        //var handle4 = PostSubs.subscribe('profilesForSearch');
        self.ready.set(handle.ready() && handle2.ready() && handle3.ready());
    });
});

Template.searchLoader.helpers({
    subsReady: function() {
        return Template.instance().ready.get();
    }
});