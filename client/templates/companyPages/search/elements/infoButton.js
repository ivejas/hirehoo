Template.infoButton.onRendered(function() {
    $('.modal-trigger').leanModal({
        ready: function() {Session.set('resultModalLoaded', true)},
        complete: function() {Session.set('resultModalLoaded', false)}
    })
});

Template.infoButton.events({
    "click .modal-trigger": function() {
        window.scrollTo(0, 0);
    }
});