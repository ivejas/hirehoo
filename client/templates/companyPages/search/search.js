Template.search.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 2) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    Session.set('currentPage', 'search');

    Session.set('atrCount', 1);
    Session.set('lanCount', 1);

    Session.set('skillSearch', null);
    Session.set('tempSkillSearch', null);

    Session.set('result', null);
    Session.set('city', null);
    Session.set('work_area', null);

    Session.set('searchResult', null);

    Session.set('mixer', 0);

    Session.set('paginationArray', []);
    Session.set('paginationPages', 1);
    Session.set('currentPaginationPage', 1);

    Session.set('showProfile', false);
    Session.set('resultModalLoaded', false);

});

Template.search.onDestroyed(function() {
    delete Session.keys['id'];
})

Template.search.onRendered(function() {
        $('select').material_select();
        $('.tooltipped').tooltip({delay: 5});
    $('body').on('keydown',function(event) {
        if(event.keyCode == 27) {
            if(Session.get('showProfile') == true) {
                Session.set('showProfile', false);
            }
        }
    });
    $('.scrollspy').scrollSpy();
});

Template.search.events({
    "click": function(event) {
        console.log(event.pageY);
    },
    "click #atrAdd": function(event) {
        var count = Session.get('atrCount') + 1;
        if(count > 1) {
            $("#atrRemove").prop("disabled", false);
            $("#atrRemove").removeClass("disabled");
        }
        Session.set('atrCount', count);
        var d = document.getElementById("divider");
        $('<div id="atr'+count+'" class="addAtr"><div class="input-field col s2"><label for="req'+count+'">Įgūdis</label><input id="req'+count+'" type="text" class="validate attribute"></div><div class="input-field col s2"><label for="exp'+count+'">Patirtis</label><select id="exp'+count+'"><option value="0">Pagrindai</option><option value="1">iki 1m.</option><option value="2">1-2m.</option><option value="3">3-4m.</option><option value="4">4m. ir daugiau</option></select></div></div>').appendTo($("#skillPointer"));
        $(document).ready(function() {
            $('#exp'+count).material_select();
        });
    },
    "click #atrRemove": function(event) {
        var count = Session.get('atrCount');

        if(count - 1 == 1) {
            $("#atrRemove").prop("disabled", true);
            $("#atrRemove").addClass("disabled");
        }
        $("#atr"+count).remove();
        Session.set('atrCount', count - 1);
    },
    "click #lanAdd": function(event) {
        var count = Session.get('lanCount') + 1;
        if(count > 1) {
            $("#lanRemove").prop("disabled", false);
            $("#lanRemove").removeClass("disabled");
        }
        Session.set('lanCount', count);
        var d = document.getElementById("languagePointer");
        $('<div id="lan'+count+'" class="col s12 addLan"><div class="input-field col s2"></div><div class="input-field col s2"><label for="lanName'+count+'">Kalba</label><select id="lanName'+count+'"><option value="0">Anglų</option><option value="1">Arabų</option><option value="2">Armėnų</option><option value="3">Azerbaidžaniečių</option><option value="4">Baltarusių</option><option value="5">Bulgarų</option><option value="6">Čekų</option><option value="7">Danų</option><option value="8">Esperanto</option><option value="9">Estų</option><option value="10">Filipinų</option><option value="11">Graikų</option><option value="12">Gruzinų</option><option value="13">Hebrajų</option><option value="14">Hindi</option><option value="15">Ispanų</option><option value="16">Italų</option><option value="17">Japonų</option><option value="18">Kinų</option><option value="19">Korėjiečių</option><option value="20">Kroatų</option><option value="21">Latvių</option><option value="22">Lenkų</option><option value="23">Lietuvių</option><option value="24">Lotynų</option><option value="25">Norvegų</option><option value="26">Olandų</option><option value="27">Portugalų</option><option value="28">Prancūzų</option><option value="29">Rumunų</option><option value="30">Rusų</option><option value="31">Serbų</option><option value="32">Slovakų</option><option value="33">Slovėnų</option><option value="34">Suomių</option><option value="35">Švedų</option><option value="36">Turkų</option><option value="37">Ukrainiečių</option><option value="38">Vengrų</option><option value="39">Vokiečių</option></select></div><div class="input-field col s2"><label for="read'+count+'">Skaitymas</label><select id="read'+count+'"><option value="0">Nesvarbu</option><option value="1">Blogai</option><option value="2">Blogiau nei vidutiniškai</option><option value="3">Vidutiniškai</option><option value="4">Geriau nei vidutiniškai</option><option value="5">Gerai</option></select></div><div class="input-field col s2"><label for="writ'+count+'">Rašymas</label><select id="writ'+count+'"><option value="0">Nesvarbu</option><option value="1">Blogai</option><option value="2">Blogiau nei vidutiniškai</option><option value="3">Vidutiniškai</option><option value="4">Geriau nei vidutiniškai</option><option value="5">Gerai</option></select></div><div class="input-field col s2"><label for="talk'+count+'">Kalbėjimas</label><select id="talk'+count+'"><option value="0">Nesvarbu</option><option value="1">Blogai</option><option value="2">Blogiau nei vidutiniškai</option><option value="3">Vidutiniškai</option><option value="4">Geriau nei vidutiniškai</option><option value="5">Gerai</option></select></div></div>').appendTo($("#languagePointer"));
        $(document).ready(function() {
            $('#lanName'+count).material_select();
            $('#read'+count).material_select();
            $('#writ'+count).material_select();
            $('#talk'+count).material_select();
        });
    },
    "click #lanRemove": function(event) {
        var count = Session.get('lanCount');

        if(count - 1 == 1) {
            $("#lanRemove").prop("disabled", true);
            $("#lanRemove").addClass("disabled");
        }
        $("#lan"+count).remove();
        Session.set('lanCount', count - 1);
    },
    "click #search": function(event) {
        var checker = true;
        if ($("#city").val() == '') {
            Materialize.toast('City is required', 4000);
            checker = false;
        }
        if ($("#work_area").val() == 0) {
            Materialize.toast('Work area is required', 4000);
            checker = false;
        }
        if (checker == true) {
            $('html, body').animate({
                scrollTop: $("#resultsSection").offset().top
            }, 500);
            var requestArray = [];
            var searchArray = {};


            requestArray.push({'requirements.city': {'$regex': '^.*'+$('#city').val() + ".*\\b", "$options": "i"}});
            searchArray.city = $('#city').val();

            if($('#experience').is(":checked")) {
                requestArray.push({'past_work': {$exists: true}});
                searchArray.past_work = true;
            } else {
                searchArray.past_work = false;
            }

            requestArray.push({'work_areas': {"area": Number($("#work_area").val())}});
            searchArray.work_areas = [{'area': Number($("#work_area").val())}];

            if($('#education').val() != 9) {
                requestArray.push({
                    "education.degree_type": {$gte: Number($('#education').val())}
                });
                searchArray.degree_type = Number($('#education').val());
            }
            if($("#min").val() != 0) {
                requestArray.push({
                    "requirements.salary_from" : {$gte: Number($("#min").val())}
                });
                searchArray.salary_min = Number($("#min").val());
            }
            if($("#max").val() != 0) {
                requestArray.push({
                    "requirements.salary_from" : {$lte: Number($("#max").val())}
                });
                searchArray.salary_max = Number($("#max").val());
            }
            if($("#req1").val() != '' && $("#req1").val() != null) {
                requestArray.push({
                    'skills.name': $("#req1").val(),
                    'skills.experience': {$gte: Number($('#exp1').val())}
                });
                searchArray.skills = [];
                searchArray.skills.push({});
                searchArray.skills[0].name = $("#req1").val();
                searchArray.skills[0].experience = Number($('#exp1').val());
            }
            for(x=2; x<$("#skillPointer .addAtr").size()+2; x++) {
                if($("#req"+x).val() != '' && $("#req"+x).val() != null) {
                    requestArray.push({
                        'skills.name': $("#req" + x).val(),
                        'skills.experience': {$gte: Number($('#exp' + x).val())}
                    });
                    searchArray.skills.push({});
                    searchArray.skills[x-1].name = $("#req" + x).val(),
                    searchArray.skills[x-1].experience = Number($('#exp' + x).val());
                }
            }
            if($("#lanName1").val() != '' && $("#lanName1").val() != null) {
                requestArray.push(
                    {
                        'languages.language': Number($("#lanName1").val())
                    }
                );
                searchArray.languages = [];
                searchArray.languages.push({});
                searchArray.languages[searchArray.languages.length-1].language = Number($("#lanName1").val());
                if($('#read1').val() != '0') {
                    requestArray[requestArray.length-1]['languages.reading'] = {$gte: Number($('#read1').val())};
                    searchArray.languages[searchArray.languages.length-1].reading = Number($("#read1").val());
                }
                if($('#writ1').val() != '0') {
                    requestArray[requestArray.length-1]['languages.writing'] = {$gte: Number($('#writ1').val())};
                    searchArray.languages[searchArray.languages.length-1].writing = Number($("#writ1").val());
                }
                if($('#talk1').val() != '0') {
                    requestArray[requestArray.length-1]['languages.speaking'] = {$gte: Number($('#talk1').val())};
                    searchArray.languages[searchArray.languages.length-1].speaking = Number($("#talk1").val());
                }
            }
            for(x=2; x<$("#languagePointer .addLan").size()+2; x++) {
                if($("#lanName"+x).val() != '' && $("#lanName"+x).val() != null) {
                    requestArray.push(
                        {
                            'languages.name': Number($("#lanName" + x).val())
                        }
                    );
                    searchArray.languages.push({});
                    searchArray.languages[x].language = Number($("#lanName" + x).val());
                    if($('#read1').val() != '0') {
                        requestArray[requestArray.length-1]['languages.reading'] = {$gte: Number(($('#read' + x).val()))};
                        searchArray.languages[searchArray.languages.length-1].reading = Number($('#read' + x).val());
                    }
                    if($('#writ1').val() != '0') {
                        requestArray[requestArray.length-1]['languages.writing'] = {$gte: Number($('#writ' + x).val())};
                        searchArray.languages[searchArray.languages.length-1].writing = Number($('#writ' + x).val());
                    }
                    if($('#talk1').val() != '0') {
                        requestArray[requestArray.length-1]['languages.speaking'] = {$gte: Number($('#talk' + x).val())};
                        searchArray.languages[searchArray.languages.length-1].speaking = Number($('#talk' + x).val());
                    }

                }

            }
            console.log(requestArray);
            Session.set('paginationArray', []);
            Session.set('currentPaginationPage', 1);
            Session.set('paginationPages', 1);
            var tempArray = [];
            searchArray.user_id = Meteor.userId();
            Meteor.call('saveSearch', searchArray);
            Meteor.call('search', requestArray, function(e, data) {
                var result = data;
                console.log(result);
                Meteor.call('listCitySuggestions', requestArray, $('#city').val());

                for (x = 0; x < result.length; x++) {
                    tempArray.push(result[x]);
                    Meteor.call('logSearchSuccess', result[x].user_id);
                    if (x % 9 == 0 && x != 0) {
                        Session.set('paginationPages', Session.get('paginationPages') + 1);
                        var tmp = Session.get('paginationArray');
                        tmp.push(tempArray);
                        Session.set('paginationArray', tmp);
                        tempArray = [];
                    }
                }
                if (tempArray != []) {
                    var tmp = Session.get('paginationArray');
                    tmp.push(tempArray);
                    Session.set('paginationArray', tmp);
                }
                if (Session.get("paginationPages") != 1) {
                    $(".chevron-right").removeClass("disabled");
                }
                Session.set('city', $("#city").val());
                Session.set('work_area', $("#work_area").val());
                Session.set('result', result);
            });
        }
    },
    "click .attribute": function(event) {
        Session.set('target', event.currentTarget.id);
        $('#skillModal').openModal();
        $("#skill_search").focus();
        $("#skill_search").typing({
            stop: function(event, $elem) {
                Session.set("skillSearch", $("#skill_search").val());
            },
            delay: 400
        });
    },
    "click .attribute-button": function(event) {
        $("#"+Session.get('target')).attr("value", event.currentTarget.value);
        $("#"+Session.get('target')).focus();
        $('#skillModal').closeModal();
    },
    "click .user-info-button": function(event) {
        Session.set('searchResult',event.currentTarget.id);
        //Meteor.call("addView", Meteor.userId(), event.currentTarget.id);
        //$('#resultModal').openModal();
        //Session.set('showProfile', true);
        //nOverlay.create('cvView',null, event.currentTarget.id);
        //nOverlay.create('cvViewOverlay', null, event.currentTarget.id);
    },
    "click #bookmark": function(event) {
        if($('#'+event.currentTarget.value+'.user-bookmark-button').hasClass("orange")) {
            Meteor.call('addBookmark',  Meteor.userId(), event.currentTarget.value);
            $('#'+event.currentTarget.value+'.user-bookmark-button').removeClass("orange");
            $('#'+event.currentTarget.value+'.user-bookmark-button').addClass("green");
        } else if($('#'+event.currentTarget.value+'.user-bookmark-button').hasClass("green")) {
            Meteor.call('removeBookmark',  Meteor.userId(), event.currentTarget.value);
            $('#'+event.currentTarget.value+'.user-bookmark-button').addClass("orange");
            $('#'+event.currentTarget.value+'.user-bookmark-button').removeClass("green");
        }
    },
    "click #request": function(event) {
        Meteor.call('requestInfo', Meteor.userId(), event.currentTarget.value);
    },
    "click .chevron-left": function(event) {
        if(!$(".chevron-left").hasClass("disabled")) {
            if (Session.get("currentPaginationPage") != 1) {
                $("#page" + Session.get("currentPaginationPage")).removeClass("active");
                if (Session.get("currentPaginationPage") == Session.get("paginationPages")) {
                    $(".chevron-right").removeClass("disabled");
                }
                Session.set("currentPaginationPage", Session.get("currentPaginationPage") - 1);
                $("#page" + Session.get("currentPaginationPage")).addClass("active");
                if (Session.get("currentPaginationPage") == 1) {
                    $(".chevron-left").addClass("disabled");
                }
            }
        }
    },
    "click .chevron-right": function(event) {
        if(!$(".chevron-right").hasClass("disabled")) {
            if (Session.get("currentPaginationPage") != Session.get("paginationPages")) {
                $("#page" + Session.get("currentPaginationPage")).removeClass("active");
                if (Session.get("currentPaginationPage") == 1) {
                    $(".chevron-left").removeClass("disabled");
                }
                Session.set("currentPaginationPage", Session.get("currentPaginationPage") + 1);
                if (Session.get("currentPaginationPage") == Session.get("paginationPages")) {
                    $(".chevron-right").addClass("disabled");
                }
                $("#page" + Session.get("currentPaginationPage")).addClass("active");
            }
        }
    },
    "click .pagination-button": function(event) {
        if(Session.get("paginationPages") != 1) {
            $("#page" + Session.get("currentPaginationPage")).removeClass("active");
            Session.set("currentPaginationPage", event.currentTarget.value);
            $("#page" + Session.get("currentPaginationPage")).addClass("active");
            if (Session.get("currentPaginationPage") != 1) {
                $(".chevron-left").removeClass("disabled");
            } else {
                $(".chevron-left").addClass("disabled");
            }
            if (Session.get("currentPaginationPage") != Session.get("paginationPages")) {
                $(".chevron-right").removeClass("disabled");
            } else {
                $(".chevron-right").addClass("disabled");
            }
        }
    },
    "click .user-bookmark-button": function(event) {
        if ($(event.currentTarget).hasClass("orange")) {
            Meteor.call('addBookmark', Meteor.userId(), event.currentTarget.id);
            $(event.currentTarget).removeClass("orange");
            $(event.currentTarget).addClass("green");
        } else if ($(event.currentTarget).hasClass("green")) {
            Meteor.call('removeBookmark', Meteor.userId(), event.currentTarget.id);
            $(event.currentTarget).addClass("orange");
            $(event.currentTarget).removeClass("green");
        }
    }
});

Template.search.helpers({
    resultModalLoaded: function() {
        return Session.get('resultModalLoaded');
    },
    modalTrigger: function() {
        $('.modal-trigger').leanModal({
            ready: function() {alert('zdarow')},
            complete: function() {alert('bye')}
        })
    },
    isBooked: function(user_id){
        var bookmark =  Bookmarks.findOne({"company_id": Meteor.userId(), "user_id": user_id});
        if(bookmark != null) {
            return true;
        } else {
            return false;
        }
    },
    mixer: function() {
        mixer = Session.get('mixer');
        if(mixer % 2 == 0) {
            mixer++;
            Session.set('mixer', mixer);
            return true;
        } else {
            mixer++;
            Session.set('mixer', mixer);
            return false;
        }
    },
    searchResult: function(){
        if(Session.get('searchResult') != null) {
            //var result = Profiles.findOne({"user_id": Session.get('searchResult')});
            return Session.get('searchResult');
        } else {
            return false;
        }
    },

    getExpValue: function(exp) {
        experience = [
            {label: "Pagrindai", value: 0},
            {label: "iki 1m.", value: 1},
            {label: "1-2m.", value: 2},
            {label: "3-4m.", value: 3},
            {label: "4m. ir daugiau", value: 4}
        ];
        for(x = 0; x<experience.length; x++) {
            if(experience[x].value == exp) {
                return experience[x].label;
            }
        }
    },
    results: function() {
        if(Session.get('paginationArray') != null) {
            var result = Session.get('paginationArray');
            var page = Session.get('currentPaginationPage');
            return result[page-1];
        } else {
            return false;
        }
    },

    pages: function() {
        if(Session.get('paginationPages') != null) {
            if(Session.get('paginationPages') == 1) {
                return [];
            } else {
                var pages = Session.get('paginationPages');
                var pageArray = [];
                for(x=2; x<=pages;x++) {
                    pageArray.push(x);
                }
                return pageArray;
            }
        } else {
            return false;
        }
    },

    getExperience: function(result) {
        var years = 0;
        var months = 0;
        Session.set('work_area', CompanyProfiles.findOne()['work_area']);
        if(result.past_work == null) {
            return 'Nėra';
        }
        for(x = 0; x < result.past_work.length; x++) {
            console.log(Session.get('work_area'));
            if(result.past_work[x].work_area == Session.get('work_area')) {
                tempEndYear = result.past_work[x].end_time_year;
                tempEndMonth = result.past_work[x].end_time_month;
                if(tempEndYear == 0) {
                    tempEndYear = new Date();
                    tempEndYear = tempEndYear.getFullYear();
                    tempEndMonth = new Date();
                    tempEndMonth = tempEndMonth.getMonth() + 1;
                }
                tempStartYear = result.past_work[x].start_time_year;
                tempStartMonth = result.past_work[x].start_time_month;

                tempYear = tempEndYear - tempStartYear;
                tempMonth = tempEndMonth - tempStartMonth;
                if(tempMonth < 0) {
                    tempYear--;
                    tempMonth = 12 + tempMonth;
                }
                years += tempYear;
                months += tempMonth;
            }
        }
        if(years == 0 && months == 0) {
            return "Nėra";
        } else {
            if (years == 0) {
                if (months == 1) {
                    return "" + months + " mėnesis";
                }
                return "" + months + " mėnesiai";
            }
            if (months == 1) {
                return "" + years + " m. " + months + " mėnesis";
            } else {
                return "" + years + " m. " + months + " mėnesiai";
            }
        }

    },

    getSalary: function(result) {
        for (x = 0; x < result.requirements.length; x++){
                if (result.requirements[x].city == Session.get('city') || result.requirements[x].city.toLowerCase() == Session.get('city').toLowerCase()) {
                    return result.requirements[x].salary_from;
                }
        }
        return false;
    },

    getPosition: function(result) {
        for (x = 0; x < result.requirements.length; x++){
                if (result.requirements[x].city == Session.get('city') || result.requirements[x].city.toLowerCase() == Session.get('city').toLowerCase()) {
                    return result.requirements[x].desired_position;
                }
        }
        return false;
    },

    getDegree: function(result) {
        var tempDegree = -1;
        for(x = 0; x < result.education.length; x++) {
            if(result.education[x].degree_type > tempDegree) {
                tempDegree = result.education[x].degree_type;
            }
        }
        if(tempDegree == 0 ) {
            return "Vidurinis";
        } else if(tempDegree == 1) {
            return "Bakalauras";
        } else if(tempDegree == 2) {
            return "Magistras"
        } else if(tempDegree == 3) {
            return "Daktaras";
        }
        return false;
    },

    getLanguages: function(result) {
        languages = [
            {label: "Anglų", value: 0},
            {label: "Arabų", value: 1},
            {label: "Armėnų", value: 2},
            {label: "Azerbaidžaniečių", value: 3},
            {label: "Baltarusių", value: 4},
            {label: "Bulgarų", value: 5},
            {label: "Čekų", value: 6},
            {label: "Danų", value: 7},
            {label: "Esperanto", value: 8},
            {label: "Estų", value: 9},
            {label: "Filipinų", value: 10},
            {label: "Graikų", value: 11},
            {label: "Gruzinų", value: 12},
            {label: "Hebrajų", value: 13},
            {label: "Hindi", value: 14},
            {label: "Ispanų", value: 15},
            {label: "Italų", value: 16},
            {label: "Japonų", value: 17},
            {label: "Kinų", value: 18},
            {label: "Korėjiečių", value: 19},
            {label: "Kroatų", value: 20},
            {label: "Latvių", value: 21},
            {label: "Lenkų", value: 22},
            {label: "Lietuvių", value: 23},
            {label: "Lotynų", value: 24},
            {label: "Norvegų", value: 25},
            {label: "Olandų", value: 26},
            {label: "Portugalų", value: 27},
            {label: "Prancūzų", value: 28},
            {label: "Rumunų", value: 29},
            {label: "Rusų", value: 30},
            {label: "Serbų", value: 31},
            {label: "Slovakų", value: 32},
            {label: "Slovėnų", value: 33},
            {label: "Suomių", value: 34},
            {label: "Švedų", value: 35},
            {label: "Turkų", value: 36},
            {label: "Ukrainiečių", value: 37},
            {label: "Vengrų", value: 38},
            {label: "Vokiečių", value: 39}
        ];
        langArray = [];
        for(x = 0; x < result.languages.length; x++) {
            for(y = 0; y < languages.length; y++) {
                if(languages[y].value == result.languages[x].language) {
                    langArray.push(languages[y].label);
                    break;
                }
            }
        }
        return langArray.toString();
    },

    getSkills: function(result) {
        skillArray = [];
        if(result.skills != null) {
            for (x = 0; x < result.skills.length; x++) {
                skillArray.push(result.skills[x].name);
            }
            return skillArray.toString();
        } else {
            return 'None';
        }
    },

    getDesires: function(result) {
        desireArray = [];
        for(x=0; x<result.requirements.length; x++) {
            if (result.requirements[x].desires != null) {
                for (y = 0; y < result.requirements[x].desires.length; y++) {
                    desireArray.push(result.requirements[x].desires[y]);
                }
            }
        }
        if(desireArray.length == 0) {
            return "None"
        } else {
            return desireArray.toString();
        }
    },

    languages: function() {
      return [
            {label: "Anglų", value: 0},
            {label: "Arabų", value: 1},
            {label: "Armėnų", value: 2},
            {label: "Azerbaidžaniečių", value: 3},
            {label: "Baltarusių", value: 4},
            {label: "Bulgarų", value: 5},
            {label: "Čekų", value: 6},
            {label: "Danų", value: 7},
            {label: "Esperanto", value: 8},
            {label: "Estų", value: 9},
            {label: "Filipinų", value: 10},
            {label: "Graikų", value: 11},
            {label: "Gruzinų", value: 12},
            {label: "Hebrajų", value: 13},
            {label: "Hindi", value: 14},
            {label: "Ispanų", value: 15},
            {label: "Italų", value: 16},
            {label: "Japonų", value: 17},
            {label: "Kinų", value: 18},
            {label: "Korėjiečių", value: 19},
            {label: "Kroatų", value: 20},
            {label: "Latvių", value: 21},
            {label: "Lenkų", value: 22},
            {label: "Lietuvių", value: 23},
            {label: "Lotynų", value: 24},
            {label: "Norvegų", value: 25},
            {label: "Olandų", value: 26},
            {label: "Portugalų", value: 27},
            {label: "Prancūzų", value: 28},
            {label: "Rumunų", value: 29},
            {label: "Rusų", value: 30},
            {label: "Serbų", value: 31},
            {label: "Slovakų", value: 32},
            {label: "Slovėnų", value: 33},
            {label: "Suomių", value: 34},
            {label: "Švedų", value: 35},
            {label: "Turkų", value: 36},
            {label: "Ukrainiečių", value: 37},
            {label: "Vengrų", value: 38},
            {label: "Vokiečių", value: 39}
        ];
    },
    skill_search: function() {
        if(Session.get('skillSearch') != '' && Session.get('skillSearch') != null) {
            var search = Session.get('skillSearch');
            var results = Skills.find({'work_area': Number($("#work_area").val()), 'name': {'$regex': '^.*'+Session.get('skillSearch') + ".*\\b", "$options": "i"}});
            if(results.fetch().length == 0) {
                search = {name: search, work_area: Number($("#work_area").val())};
                results = Skills.find({'work_area': Number($("#work_area").val())}).fetch();
                results.unshift(search);
                return results
            }
            return results;
        } else {
            return false;
        }
    },

    isBookmarked: function(id) {
        var bookmark = Bookmarks.findOne({"user_id": id, "company_id": Meteor.userId()});
        if(bookmark != null) {
            return true;
        } else {
            return false;
        }
    },
    isRequested: function(id) {
        var request = Requests.findOne({"user_id": id, "company_id": Meteor.userId()});
        if(request != null) {
            return true;
        } else {
            return false;
        }
    },
    showProfile: function() {
        return Session.get('showProfile');
    }
});

