Template.ticketIndex.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    } else {
        if (Meteor.user()['profile']['type'] != 2) {
            Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
            FlowRouter.go('/');
        }
    }
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('myTickets');
        self.ready.set(handle.ready());
    });
    if(Meteor.user()['profile']['type'] != 2) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }
    Session.set('currentPage', 'tickets');
});

Template.ticketIndex.onRendered(function() {
    $('.tooltipped').tooltip({delay: 5});
});

Template.ticketIndex.helpers({
    tickets: function() {
        var tickets = Tickets.find();
        console.log(tickets.fetch());
        return tickets;
    },
    ticketInUse: function(status) {
        if(status != 0) {
            return true;
        }
        return false;
    },
    ticketUsed: function(status) {
        if(status == 2) {
            return true;
        }
        return false;
    },
    getName: function(user_id) {
        var user = Profiles.findOne({"user_id": user_id});
        return user.first_name+" "+user.last_name;
    },
    subsReady: function() {
        return Template.instance().ready.get();
    }
});

Template.ticketIndex.events({
    "click .buy-button": function(event) {
        Meteor.call("getTickets", event.currentTarget.value, Meteor.userId());
    }
});