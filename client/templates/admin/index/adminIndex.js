Template.adminIndex.onCreated(function() {
    if(Meteor.user()['profile']['type'] != 0) {
        Materialize.toast('Jūms negalima peržiūrėti šio puslapio', 3000, 'rounded');
        FlowRouter.go('/');
    }
});

Template.adminIndex.helpers({
   users: function() {
       console.log(Meteor.users.find().count());
       return Meteor.users.find({profile: {type: "1"}});
   },
    companies: function() {
        return Meteor.users.find({profile: {type: "2"}});
    },
    isCompanyProfile: function(id) {
        profile = CompanyProfiles.findOne({user_id: id});
        if(profile != null) {
            return true;
        } else {
            return false;
        }
    },
    companyProfileEnabled: function(id) {
        profile = CompanyProfiles.findOne({user_id: id});
        if(profile['approved'] == 1) {
            return false;
        } else {
            return true;
        }
    },
    isProfile: function(id) {
        profile = Profiles.findOne({user_id: id});
        if(profile != null) {
            return true;
        } else {
            return false;
        }
    },
    profileEnabled: function(id) {
        profile = Profiles.findOne({user_id: id});
        if(profile['approved'] == 1) {
            return false;
        } else {
            return true;
        }
    }
});