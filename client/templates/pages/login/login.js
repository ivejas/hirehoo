Template.login.events({
    "submit form": function(event) {
        event.preventDefault();
        var email = event.target.email.value;
        var password = event.target.password.value;
        Meteor.loginWithPassword(email, password, function(error) {
            if(error) {
                Materialize.toast('El. paštas arba slaptažodis neteisingi', 2000);
            } else {
                Materialize.toast('Sveiki!', 2000);
                FlowRouter.go('/');
            }
        });
    }
});