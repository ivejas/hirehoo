Template._loginModal.events({
    "click #signIn": function(event) {
        $('#loginModal').openModal();
    },
    "click #signOut": function(event) {
        Meteor.logout();
    },
    "click #signupTrigger": function(event) {
        $('#loginModal').closeModal();
        $('#signUpModal').openModal();
    }
});