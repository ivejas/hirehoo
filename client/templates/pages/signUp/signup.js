Template.signup.onRendered(function() {
    $('select').material_select();
});

Template.signup.events({
    "submit form": function(event) {
        event.preventDefault();
        console.log(event.target.email.value);
        console.log(event.target.password.value);
        if(event.target.accountType.value == '') {
            Materialize.toast('Privalote pasirinkti vartotojo tipą', 4000)
        } else {
            //Accounts.createUser({email: event.target.email.value, password: event.target.password.value, profile: {type: event.target.accountType.value}}, function(data) {
            //    console.log(data);
            //})
            Meteor.call('ATCreateUserServer', {
                email: event.target.email.value,
                password: Accounts._hashPassword(event.target.password.value),
                profile: {type: event.target.accountType.value}  // This is important to prevent 500
            }, function(err) {
                // ATCreateUserServer callback
                if (err) {
                    if(err.error == 403) {
                        Materialize.toast('Šis el.paštas jau užregistruotas', 2000);
                    }
                } else {
                    Materialize.toast('Registracija sėkminga. Pasitikrinkite el.paštą', 4000);
                    FlowRouter.go('/');
                }

                //Meteor.loginWithPassword(email, password, function(err) {
                //    // loginWithPassword callback
                //    if (err) return template.error.set(Helpers.decodeLoginError(err));
                //    Helpers.connectForm.setStep('account', 'next');
                //});
            });
        }
    }
});