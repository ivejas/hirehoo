Template.home.onCreated(function() {
    Session.set('currentPage', 'index');
});

  Template.home.helpers({
      subsReady: function() {
          return FlowRouter.subsReady();
      },
      isLoggedIn: function() {
          if(Meteor.user() == null) {
              return false;
          }
          return true;
      }
  });

  Template.home.events({
  });

  Template.home.onRendered(function() {
      $(document).ready(function(){
          $('.button-collapse').sideNav();
          $('.parallax').parallax();
      });
  });