var getCollection, getIcon, getTemplate, uploadFile;

AutoForm.addInputType('fileUpload', {
    template: 'afFileUpload'
});

getIcon = function(file) {
    var icon;
    if (file) {
        file = file.toLowerCase();
        icon = 'file-o';
        if (file.indexOf('youtube.com') > -1) {
            icon = 'youtube';
        } else if (file.indexOf('vimeo.com') > -1) {
            icon = 'vimeo-square';
        } else if (file.indexOf('.pdf') > -1) {
            icon = 'file-pdf-o';
        } else if (file.indexOf('.doc') > -1 || file.indexOf('.docx') > -1) {
            icon = 'file-word-o';
        } else if (file.indexOf('.ppt') > -1) {
            icon = 'file-powerpoint-o';
        } else if (file.indexOf('.avi') > -1 || file.indexOf('.mov') > -1 || file.indexOf('.mp4') > -1) {
            icon = 'file-movie-o';
        } else if (file.indexOf('.png') > -1 || file.indexOf('.jpg') > -1 || file.indexOf('.gif') > -1 || file.indexOf('.bmp') > -1) {
            icon = 'file-image-o';
        } else if (file.indexOf('http://') > -1 || file.indexOf('https://') > -1) {
            icon = 'link';
        }
        return icon;
    }
};

getTemplate = function(file) {
    var template;
    file = file.toLowerCase();
    template = 'fileThumbIcon';
    if (file.indexOf('.jpg') > -1 || file.indexOf('.png') > -1 || file.indexOf('.gif') > -1) {
        template = 'fileThumbImg';
    }
    return template;
};

getCollection = function(context) {
    if (typeof context.atts.collection === 'string') {
        context.atts.collection = FS._collections[context.atts.collection] || window[context.atts.collection];
    }
    return context.atts.collection;
};

Template.afFileUpload.created = function() {
    this.file = new ReactiveVar(void 0);
    this.fileUpload = new ReactiveVar(void 0);
    this.fileUploaded = new ReactiveVar(void 0);
};

Template.afFileUpload.rendered = function() {
    this.file.set(void 0);
    this.fileUpload.set(void 0);
    this.fileUploaded.set(true);
};

Template.afFileUpload.destroyed = function() {
    var name;
    name = this.data.name;
    this.file.set(void 0);
    this.fileUpload.set(void 0);
    this.fileUploaded.set(void 0);
};

uploadFile = function(event, template) {
    return FS.Utility.eachFile(event, (function(_this) {
        return function(fileObj) {
            var collection, file;
            file = new FS.File(fileObj);
            file.metadata = file.metadata || {};
            console.log(template);
            console.log(_this);
            collection = getCollection(template.data);
            if (_this.atts.metadata) {
                _.extend(file.metadata, _this.atts.metadata);
            }
            return collection.insert(file, function(error, image) {
                var cursor, liveQuery;
                if (error) {
                    return console.log(err);
                } else {
                    $('input[name="' + _this.atts.name + '"]').val(image._id);
                    template.file.set(image);
                    template.fileUpload.set(fileObj.name);
                    template.fileUploaded.set(false);
                    collection = getCollection(_this);
                    cursor = collection.find(image._id);
                    return liveQuery = cursor.observe({
                        changed: function(newImage) {
                            if (newImage.isUploaded()) {
                                template.fileUploaded.set(true);
                                liveQuery.stop();
                            }
                        }
                    });
                }
            });
        };
    })(this));
};

Template.afFileUpload.events({
    "change .file-upload": uploadFile,
    'dropped [data-action="file-dropped"]': uploadFile,
    'click .file-path': function(event, template) {
        template.$('.file-upload').click();
    },
    'click .file-upload-clear': function(event, template) {
        var name;
        name = $(event.currentTarget).attr('file-input');
        $('input[name="' + name + '"]').val('');
        template.fileUploaded.set(true);
        template.fileUpload.set(void 0);
        template.file.set(void 0);
        this.value = void 0;
    }
});

Template.afFileUpload.helpers({
    collection: function() {
        return getCollection(this);
    },
    label: function() {
        return this.atts.label || 'Choose file';
    },
    buttonlabel: function() {
        return this.atts.buttonlabel || 'Choose file';
    },
    dropLabel: function() {
        return this.atts.dropLabel || 'Drop your file here';
    },
    dropEnabled: function() {
        return this.atts.dropEnabled;
    },
    dropClasses: function() {
        return this.atts.dropClasses || 'card-panel grey lighten-4 grey-text text-darken-4';
    },
    removeLabel: function() {
        return this.atts['remove-label'] || 'Remove';
    },
    fileUploadAtts: function() {
        var atts;
        atts = _.clone(this.atts);
        delete atts.collection;
        delete atts.buttonlabel;
        delete atts.metadata;
        return atts;
    },
    fileUpload: function() {
        var cfsFile, collection, file, filename, name, obj, src, template;
        template = Template.instance();
        name = this.atts.name;
        console.log(this);
        collection = getCollection(this);
        if (template.file.get()) {
            file = template.file.get()._id;
        } else if (this.value) {
            file = this.value;
        } else {
            return null;
        }
        if (file !== '' && file) {
            if (file.length === 17) {
                cfsFile = collection.findOne({
                    _id: file
                });
                if (cfsFile) {
                    if (!template.file.get()) {
                        template.file.set(cfsFile);
                        template.fileUploaded.set(true);
                    }
                    filename = cfsFile.name();
                    src = cfsFile.url();
                } else {
                    filename = template.fileUpload.get();
                    if (filename) {
                        obj = {
                            template: 'fileThumbIcon',
                            data: {
                                src: filename,
                                icon: getIcon(filename)
                            }
                        };
                        return obj;
                    }
                }
            } else {
                filename = file;
                src = filename;
            }
        }
        if (filename) {
            obj = {
                template: getTemplate(filename),
                data: {
                    src: src,
                    icon: getIcon(filename)
                }
            };
            return obj;
        }
    },
    fileUploadSelected: function(name) {
        var template;
        template = Template.instance();
        return template.fileUpload.get();
    },
    isUploaded: function(name, collection) {
        var template;
        template = Template.instance();
        return template.fileUploaded.get();
    }
});
