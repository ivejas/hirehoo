Template.companyListIndex.onCreated(function() {
    if(Meteor.user() == null) {
        Materialize.toast('Prašome prisijungti', 3000, 'rounded');
        FlowRouter.go('/')
    }
    Session.set('currentPage', 'companyList');
    var self = this;
    self.ready = new ReactiveVar();
    self.autorun(function() {
        var handle = PostSubs.subscribe('companyProfileList');
        self.ready.set(handle.ready());
    });
});

Template.companyListIndex.helpers({
    subsReady: function() {
        return Template.instance().ready.get();
    },
    categories: function() {
        var categories = [
            {value: 1, label: "Administravimas/sekretoriavimas"},
            {value: 2, label:  "Apskaita/finansai/auditas"},
            {value: 3, label: "Dizainas/architektūra"},
            {value: 4, label: "Draudimas"},
            {value: 5, label: "Eksportas"},
            {value: 6, label: "Elektronika/telekomunikacijos"},
            {value: 7, label: "Energetika"},
            {value: 8, label: "Inžinerija/mechanika"},
            {value: 9, label: "Klientų aptarnavimas/paslaugos"},
            {value: 10, label: "Kompiuteriai/IT/internetas"},
            {value: 11, label: "Kultūra/kūryba"},
            {value: 12, label: "Logistika/transportas"},
            {value: 13, label: "Maisto gamyba"},
            {value: 14, label: "Marketingas/reklama"},
            {value: 15, label: "Medicina/sveikatos apsauga/farmacija"},
            {value: 16, label: "Nekilnojamasis turtas"},
            {value: 17, label: "Pardavimų vadyba"},
            {value: 18, label: "Personalo valdymas"},
            {value: 19, label: "Pirkimai/tiekimas"},
            {value: 20, label: "Pramonė/gamyba"},
            {value: 21, label: "Prekyba - konsultavimas"},
            {value: 22, label: "Sandėliavimas"},
            {value: 23, label: "Statyba"},
            {value: 24, label: "Švietimas/mokymai"},
            {value: 25, label: "Teisė"},
            {value: 26, label: "Turizmas/viešbučiai"},
            {value: 27, label: "Vadovavimas/valdymas"},
            {value: 28, label: "Valstybės tarnyba"},
            {value: 29, label: "Žemės ūkis/žuvininkystė"},
            {value: 30, label: "Žiniasklaida/viešieji ryšiai"}
        ];
        for(var x=0; x<categories.length; x++) {
            var count = CompanyProfiles.find({work_area: categories[x].value}).count();
            categories[x].count = count;
        }
        return categories;
    }
});