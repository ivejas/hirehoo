//AutoForm.setDefaultTemplate('materialize');
PostSubs = new SubsManager();
Template.defaultLayout.onCreated(function() {
    this.subscribe = Meteor.subscribe('myTickets');
    this.subscribe = Meteor.subscribe('myBookmarks');
    this.subscribe = Meteor.subscribe('myRequests');
    this.subscribe = Meteor.subscribe('myProfile');
    this.subscribe = Meteor.subscribe('myCompanyProfile');

});

Template.defaultLayout.helpers({
    ticketCount: function() {
        return Tickets.find().count();
    },
    bookmarkCount: function() {
        return Bookmarks.find().count();
    },
    requestCount: function() {
        return Requests.find().count();
    },
    isInCompanyList: function() {
        if(Session.get("currentPage") == 'companyList') {
            return true;
        }
        return false;
    },
    isInSearch: function() {
        if(Session.get("currentPage") == 'search') {
            return true;
        }
        return false;
    },
    isInIndex: function() {
        if(Session.get("currentPage") == 'index') {
            return true;
        }
        return false;
    },
    isInBookmarks: function() {
        if(Session.get("currentPage") == 'bookmarks') {
            return true;
        }
        return false;
    },
    isInRequests: function() {
        if(Session.get("currentPage") == 'requests') {
            return true;
        }
        return false;
    },
    isInEdit: function() {
        if(Session.get("currentPage") == 'edit') {
            return true;
        }
        return false;
    },
    isInPreview: function() {
        if(Session.get("currentPage") == 'preview') {
            return true;
        }
        return false;
    },
    isInCompanyPreview: function() {
        if(Session.get("currentPage") == 'companyPreview') {
            return true;
        }
        return false;
    },
    isInTickets: function() {
        if(Session.get("currentPage") == 'tickets') {
            return true;
        }
        return false;
    },
    isInDashboard: function() {
        if(Session.get("currentPage") == 'dashboard') {
            return true;
        }
        return false;
    },

    isInCvAdd: function() {
        if(Session.get("currentPage") == 'cvAdd') {
            return true;
        }
        return false;
    },

    isInCompanyAdd: function() {
        if(Session.get("currentPage") == 'companyAdd') {
            return true;
        }
        return false;
    },

    isLoggedIn: function() {
        if(Meteor.user() == null) {
            return false;
        }
        return true;
    },
    cvExists: function() {
        cv = Profiles.findOne({user_id: Meteor.userId()});
        if(cv != null) {
            return true;
        } else {
            return false;
        }
    },

    companyProfileExists: function() {
        var profile = CompanyProfiles.findOne();
        if(profile != null) {
            return true;
        } else {
            return false;
        }
    },

    isCompany: function() {
        if(Meteor.user()['profile']['type'] == 2) {
            $(".dropdown-button").dropdown();
            return true;
        } else {
            return false;
        }
    },
    isAdmin: function() {
        if(Meteor.user()['profile']['type'] == 0) {
            return true;
        } else {
            return false;
        }
    },
    userId: function() {
        return Meteor.userId();
    },

    pageName: function() {
        if(Session.get("currentPage") == 'dashboard') {
            return 'Jūsų paskyros statistika';
        } else if(Session.get("currentPage") == 'tickets') {
            return 'Bilietai';
        } else if(Session.get("currentPage") == 'preview') {
            return 'Profilio peržiūra';
        } else if(Session.get("currentPage") == 'edit') {
            return 'Profilio redagavimas';
        } else if(Session.get("currentPage") == 'requests') {
            return 'Užklausos';
        } else if(Session.get("currentPage") == 'bookmarks') {
            return 'Išsaugoti vartotojai';
        } else if(Session.get("currentPage") == 'search') {
            return 'Vartotojų paieška'
        } else if(Session.get("currentPage") == 'add') {
            return 'Profilio registracija'
        } else if(Session.get("currentPage") == 'companyAdd') {
            return 'Įmonės registracija'
        } else if(Session.get("currentPage") == 'companyList') {
            return 'Įmonių katalogas'
        } else if(Session.get('category') != null) {
            return Session.get('category');
        }
    }
});

Template.defaultLayout.events({
    "click #signOut": function(event) {
        Meteor.logout();
        Materialize.toast('Viso gero!', 2000);
        FlowRouter.go('/');
    }
});

Template.defaultLayout.onRendered(function() {
    $(document).ready(function(){
        $('.slider').slider({full_width: false});
        $('.tooltipped').tooltip({delay: 5});
    });
});


Template.candidatesLink.onRendered(function() {

});

Template.companyProfileLink.onRendered(function() {
});

Transitioner.setTransitions({
    'default': 'fade'
});
//Accounts.onLogout(function(user){
//    Materialize.toast('Logout successfull', 3000, 'rounded')
//});

if (Meteor.isClient) {
    Forms.mixin(Template.cvAdd);
}
AutoForm.setDefaultTemplate('plain');
